package apis.api;

import apis.api.interfaces.Auth;
import apis.api.interfaces.Clients;
import apis.api.responseModel.AuthResponseModel;
import io.qameta.allure.okhttp3.AllureOkHttp3;
import okhttp3.OkHttpClient;
import org.aeonbits.owner.ConfigFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import service.TestsConfig;

import java.util.HashMap;

public class Api {
    // Хранит последние запрошенные токеры для пользователя (ключ: login)
    public final static HashMap<String, AuthResponseModel> tokens;

    public static Auth auth;
    public static Clients clients;

    static {
        TestsConfig testsConfig = ConfigFactory.create(TestsConfig.class);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(testsConfig.apiUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(new OkHttpClient.Builder().addInterceptor(new AllureOkHttp3()).build())
                .build();

        //noinspection rawtypes
        tokens = new HashMap(1);

        auth = retrofit.create(Auth.class);
        clients = retrofit.create(Clients.class);
    }
}

