package apis.api.interfaces;

import apis.api.requestModel.AddClientRequestModel;
import apis.api.requestModel.ClientsRequestModel;
import apis.api.requestModel.ClientsUsersRequestModel;
import apis.api.responseModel.AddClientResponseModel;
import apis.api.responseModel.ClientsResponseModel;
import apis.api.responseModel.ClientsUsersResponseModel;
import retrofit2.Call;
import retrofit2.http.*;

public interface Clients {
    @POST("clients/getPage/")
    @Headers("Content-Type: application/json")
    Call<ClientsResponseModel> clients(@Header("Authorization") String bearerAuthString,
                                       @Body ClientsRequestModel requestModel);

    @POST("clients/users/getPage/")
    @Headers("Content-Type: application/json")
    Call<ClientsUsersResponseModel> clientsUsers(@Header("Authorization") String bearerAuthString,
                                                 @Body ClientsUsersRequestModel requestModel);

    @POST("clients/add")
    @Headers("Content-Type: application/json")
    Call<AddClientResponseModel> addClient(@Header("Authorization") String bearerAuthString,
                                           @Body AddClientRequestModel requestModel);

    @POST("clients/{guid}/locked/false")
    @Headers("Content-Type: application/json")
    Call<Void> lockedFalse(@Header("Authorization") String bearerAuthString,
                           @Path(value = "guid", encoded = true) String guid);

}
