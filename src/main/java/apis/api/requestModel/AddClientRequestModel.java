package apis.api.requestModel;

import lombok.AllArgsConstructor;
import lombok.With;
import testData.enums.PhoneFormat;
import testData.models.Company;

@AllArgsConstructor
@With
public class AddClientRequestModel {
    public String actualAddress;
    public String categoryId;
    public String clientType;
    public Double currentCommissionRate;
    public String federalId;
    public String fullName;
    public String inn;
    public Boolean insuranceAvailable;
    public Double latitude;
    public Double longitude;
    public String name;
    public Double ordersLimit;
    public Boolean ordersUnsecured;
    public String region;
    public String registeredAddress;
    public Boolean registryPaymentsAvailable;
    public String representativeEmail;
    public String representativeName;
    public String representativePhone;

    public AddClientRequestModel(Company company) {
        switch (company.getCompanyForm()) {
            case LEGAL_ENTITY:
                this.clientType = "RUSSIAN_LEGAL_ENTITY";
                this.fullName = company.getOfficialName();
                this.name = company.getShortName();
                this.inn = company.getInn();
                this.registeredAddress = company.getAddressRegistered();
                this.actualAddress = company.getAddressActual();
                break;
            case INDIVIDUAL:
                this.clientType = "INDIVIDUAL_ENTREPRENEUR";
                this.fullName = company.getFioIp();
                this.name = "ИП " + company.getFioIp();
                this.inn = company.getInn();
                this.registeredAddress = company.getAddressRegistered();
                break;
            case FOREIGN:
                this.clientType = "FOREIGN_LEGAL_ENTITY";
                this.fullName = company.getOfficialName();
                this.name = company.getShortName();
                this.inn = company.getEin();
                this.registeredAddress = company.getAddressRegistered();
                this.actualAddress = company.getAddressActual();
                break;
        }

        categoryId = "00000000-0000-0000-0000-000000001019"; // Аренда  ; TODO : get this id from API
        federalId = "d0203111-19c0-44d3-b3f9-8178e3aa141a"; // СПБ ; TODO : get this id from API
        region = "MSK"; // TODO : get region name from API

        latitude = 45.668731; // TODO : get it from API
        longitude = -0.394181;

        try {
            currentCommissionRate = Double.parseDouble(company.getCommissionRate());
        } catch (NumberFormatException e) {
            System.err.println("currentCommissionRate должно быть числом, но в переданной компании: " + currentCommissionRate);
            currentCommissionRate = 0.045;
        }

        insuranceAvailable = company.getInsuranceAvailable();
        registryPaymentsAvailable = company.getRegistryPaymentsAvailable();
        ordersUnsecured = company.getOrdersUnsecured();
        ordersLimit = null;

        representativeName = company.getContactorFio();
        representativeEmail = company.getContactorEmail();
        representativePhone = company.getContactorPhone().getNumber(PhoneFormat.FULL_BUT_WITHOUT_PLUS);
    }
}
