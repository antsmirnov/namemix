package apis.api.requestModel;

import lombok.AllArgsConstructor;
import lombok.With;

/* Компании */
@AllArgsConstructor
@With
public class ClientsRequestModel {
    public Boolean archivedFilter;
    public String nameSubstringFilter;
    public Integer pageNum;
    public Integer pageSize;

    public ClientsRequestModel() {
        this.archivedFilter = false;
        this.nameSubstringFilter = "";
        this.pageSize = 100;
        this.pageNum = 1;
    }
}
