package apis.api.requestModel;

import lombok.AllArgsConstructor;
import lombok.With;

// сотрудники
@AllArgsConstructor
@With
public class ClientsUsersRequestModel {
    public Boolean archiveFilter;
    public String clientId;
    public String fioSort;
    public Integer pageNum;
    public Integer pageSize;

    public ClientsUsersRequestModel() {
        this.archiveFilter = false;
        this.clientId = "";
        this.fioSort = "desc";
        this.pageNum = 1;
        this.pageSize = 100;
    }
}
