package apis.api.responseModel;

import apis.api.responseModel.subResponses.SubResponseClient;

public class ClientsResponseModel {
    public Integer totalCount;
    public SubResponseClient[] clients;
    public String errorCode;
    public String errorMessage;
}
