package apis.api.responseModel;

import apis.api.responseModel.subResponses.SubResponseClientUser;

public class ClientsUsersResponseModel {
    public SubResponseClientUser[] clientUsers;
    public Integer totalCount;
}
