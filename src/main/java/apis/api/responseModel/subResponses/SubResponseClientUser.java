package apis.api.responseModel.subResponses;

public class SubResponseClientUser {
    public Boolean active;
    public Boolean archived;
    public String clientId;
    public String clientInn;
    public String clientName;
    public String clientUserId;
    public Boolean deleted;
    public String email;
    public String firstName;
    public String inn;
    public String lastName;
    public Boolean locked;
    public String login;
    public String password;
    public String passwordHash;
    public String patronymic;
    public String phone;
    public String position;
    public String role;
    public String snils;
}
