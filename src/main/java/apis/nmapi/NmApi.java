package apis.nmapi;

import apis.nmapi.interfaces.*;
import apis.nmapi.responseModel.AuthResponseModel;
import io.qameta.allure.okhttp3.AllureOkHttp3;
import okhttp3.OkHttpClient;
import org.aeonbits.owner.ConfigFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import service.TestsConfig;

import java.util.HashMap;

public class NmApi {
    // Хранит последние запрошенные токены для пользователя (ключ: login)
    public final static HashMap<String, AuthResponseModel> tokens;

    public static Auth auth;
    public static Deposit balance;
    public static Tax tax;
    public static Contract contract;
    public static Orders orders;

    static {
        TestsConfig testsConfig = ConfigFactory.create(TestsConfig.class);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(testsConfig.nmApiUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(new OkHttpClient.Builder().addInterceptor(new AllureOkHttp3()).build())
                .build();

        //noinspection rawtypes
        tokens = new HashMap(1);

        auth = retrofit.create(Auth.class);
        balance = retrofit.create(Deposit.class);
        tax = retrofit.create(Tax.class);
        contract = retrofit.create(Contract.class);
        orders = retrofit.create(Orders.class);
    }

}

