package apis.nmapi.interfaces;

import apis.nmapi.responseModel.AuthResponseModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import testData.models.User;

public interface Auth {
    @POST("auth/login/")
    @Headers("Content-Type: application/json")
    Call<AuthResponseModel> login(@Body User user);

    @POST("auth/refreshToken/")
    @Headers("Content-Type: application/json")
    Call<AuthResponseModel> refreshToken(@Header("Authorization") String bearerAuthString);

}
