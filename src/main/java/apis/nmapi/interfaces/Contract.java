package apis.nmapi.interfaces;

import apis.nmapi.requestModel.ContractInitRequestModel;
import apis.nmapi.requestModel.ContractSignRequestModel;
import apis.nmapi.responseModel.ContractResponesModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Contract {

    @POST("contract/init/")
    @Headers("Content-Type: application/json")
    Call<ContractResponesModel> init(@Header("Authorization") String bearerAuthString,
                                     @Body ContractInitRequestModel contract);

    @POST("contract/sign/")
    @Headers("Content-Type: application/json")
    Call<ContractResponesModel> sign(@Header("Authorization") String bearerAuthString,
                                     @Body ContractSignRequestModel contract);

}
