package apis.nmapi.interfaces;

import apis.nmapi.responseModel.GetInfoDepositBalanceResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface Deposit {
    @GET("deposit/getInfoDepositBalance/")
    Call<GetInfoDepositBalanceResponseModel> getInfoDepositBalance(@Header("Authorization") String bearerAuthString);
}
