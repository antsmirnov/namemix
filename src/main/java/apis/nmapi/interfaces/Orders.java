package apis.nmapi.interfaces;

import apis.nmapi.requestModel.AddWithPaymentRequestModel;
import apis.nmapi.responseModel.AddWithPaymentResponseModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Orders {
    @POST("orders/addWithPayment/")
    Call<AddWithPaymentResponseModel> addWithPayment(@Header("Authorization") String bearerAuthString,
                                                     @Body AddWithPaymentRequestModel requestModel);
}
