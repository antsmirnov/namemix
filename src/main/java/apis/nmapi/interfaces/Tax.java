package apis.nmapi.interfaces;

import apis.nmapi.responseModel.TaxCheckContractorStatusResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Tax {
    @GET("tax/checkContractorStatus/")
    Call<TaxCheckContractorStatusResponseModel> checkContractorStatus(@Header("Authorization") String bearerAuthString,
                                                                      @Query("contractorInn") String contractorInn,
                                                                      @Query("contractorPhone") String contractorPhone);
}
