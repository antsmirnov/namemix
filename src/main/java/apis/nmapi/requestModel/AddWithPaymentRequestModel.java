package apis.nmapi.requestModel;

import db.model.Contractor;
import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.With;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static steps.apis.nmApi.ContractorStatusSteps.dbSteps;

@AllArgsConstructor
@With
@ToString
public class AddWithPaymentRequestModel {
    public String requestId;
    public String contractorPhone;
    public String contractorInn;
    public String contractorFirstName;
    public String contractorLastName;
    public String contractorPatronymic;
    public String contractorPaymentAgreementRequired;
    public String orderName;
    public String orderDescription;
    public String orderAmount;
    public String orderWorkEndDate;
    public String orderWorkStartDate;

    public AddWithPaymentRequestModel() {
        requestId = UUID.randomUUID().toString();
        contractorPhone = "73334004333";
        contractorInn = "518968194007";
        contractorFirstName = "Исполнитель";
        contractorLastName = "Контрактов";
        contractorPatronymic = "";
        contractorPaymentAgreementRequired = "false";
        orderName = "Тестовый заказ";
        orderDescription = "Описание тестового заказа";
        orderAmount = "999.99";
        orderWorkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        orderWorkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public AddWithPaymentRequestModel withPhoneAndInnForFullyRegisteredContractor() {
        Contractor fullyReg = dbSteps.getContractorFullyRegistered();
        return this
                .withContractorPhone(fullyReg.phone)
                .withContractorInn(fullyReg.inn);
    }
}
