package apis.nmapi.requestModel;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ContractInitRequestModel {
    public String contractorInn;
    public String contractorPhone;
}
