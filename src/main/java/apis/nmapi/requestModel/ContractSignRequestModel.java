package apis.nmapi.requestModel;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ContractSignRequestModel {
    public String contractorInn;
    public String contractorPhone;
    public String contractDate;
    public String smsCode;
}
