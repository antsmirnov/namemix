package apis.nmapi.responseModel;

public class AddWithPaymentResponseModel {
    public Boolean success; // Не описано в спеке
    public String errorCode;
    public String errorDetails;
    public String errorMessage;
    public String requestId;
    public String fullName;
    public String contractorInn;
    public String paymentStatus;
    public String paymentAmount;
    public String checkLink;
    public String checkDateTime;
}
