package apis.nmapi.responseModel;

public class AuthResponseModel {
    public String accessToken;
    public String refreshToken;
    public String role;
    public String clientId;
    public String clientUserId;
    public String accessTokenExpirationDateTimeUTC;
    public String refreshTokenExpirationDateTimeUTC;
    public Boolean success;
    public String errorCode;
    public String errorMessage;
}
