package apis.nmapi.responseModel;

public class GetInfoDepositBalanceResponseModel {
    public Double depositAmount;
    public Double orderReserveAmount;
    public Double balanceFree;
    public Double ordersLimitAmount;
    public Double ordersLimitBalance;
    public Double paymentAwaitingContractors;
    public Double paymentAwaitingClient;
    public String errorCode;
    public String errorMessage;
}