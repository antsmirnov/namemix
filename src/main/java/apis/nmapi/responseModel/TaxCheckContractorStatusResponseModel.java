package apis.nmapi.responseModel;

public class TaxCheckContractorStatusResponseModel {
    public String errorCode;
    public String errorMessage;
    public String firstName;
    public String lastName;
    public String patronymic;
    public String phone;
    public String inn;
    public String status;
}