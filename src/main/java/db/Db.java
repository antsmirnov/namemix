package db;

import io.qameta.allure.Allure;
import org.aeonbits.owner.ConfigFactory;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.extension.ExtensionCallback;
import org.jdbi.v3.postgres.PostgresPlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import service.TestsConfig;

import static utils.StringFunctions.convertObjToString;

public class Db {
    private static TestsConfig testsConfig = ConfigFactory.create(TestsConfig.class);

    public static Jdbi postgresDB = Jdbi
            .create(testsConfig.dbConnectionString() + "?" +
                    "user=" + testsConfig.dbLogin() +
                    "&password=" + testsConfig.dbPassword() +
                    "&currentSchema=" + testsConfig.dbSchema()
            )
            .installPlugin(new PostgresPlugin())
            .installPlugin(new SqlObjectPlugin());

    /**
     * Wrapped withExtension method with logging of queries results
     */
    public static <R, E, X extends Exception> R query(Class<E> extensionType, ExtensionCallback<R, E, X> callback)
            throws X {
        R result = postgresDB.withExtension(extensionType, callback);
        Allure.addAttachment("Результат SQL-запроса", convertObjToString(result));
        return result;
    }
}