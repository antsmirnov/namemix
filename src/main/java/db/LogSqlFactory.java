package db;

import io.qameta.allure.Allure;
import org.jdbi.v3.core.statement.SqlLogger;
import org.jdbi.v3.core.statement.StatementContext;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizer;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizerFactory;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizingAnnotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@SqlStatementCustomizingAnnotation(LogSqlFactory.Factory.class)
public @interface LogSqlFactory {

    class Factory implements SqlStatementCustomizerFactory {
        @Override
        public SqlStatementCustomizer createForType(Annotation annotation, Class sqlObjectType) {
            SqlLogger sqlLogger = new SqlLogger() {
                @Override
                public void logBeforeExecution(StatementContext context) {
                    Allure.addAttachment("SQL-запрос", context.getRenderedSql());
                }
            };
            return statement -> statement.setSqlLogger(sqlLogger);
        }
    }
}