package db;

import db.model.Contractor;
import db.model.Phone;
import db.model.mappers.ContractorMapper;
import db.model.mappers.PhoneMapper;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Define;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.stringtemplate4.UseStringTemplateEngine;

import java.util.List;

@LogSqlFactory
public interface Queries {
    @SqlQuery("SELECT inn, phone FROM \"Contractors\" " +
            "WHERE \"phone\" like '700________' " +
            "AND \"fullRegistrationDate\" IS NOT NULL " +
            "AND \"blocked\" = false " +
            "AND \"overallRating\" IS NOT NULL " +
            "LIMIT 50")
    @RegisterRowMapper(ContractorMapper.class)
    List<Contractor> fullRegisteredContractors();

    @SqlQuery("SELECT inn, phone\n" +
            "FROM \"Contractors\" \n" +
            "WHERE \"phone\" like '700________' \n" +
            "AND \"fullRegistrationDate\" IS NULL " +
            "AND \"blocked\" = false " +
            "AND \"inn\" is not NULL " +
            "and phone like '7%'\n" +
            "LIMIT 50")
    @RegisterRowMapper(ContractorMapper.class)
    List<Contractor> notFullRegisteredContractors();

    // TODO: исправить запрос, попадаются неправильные статусы
    @SqlQuery("SELECT inn, phone\n" +
            "FROM \"Contractors\" \n" +
            "WHERE \"phone\" like '700________'\n" +
            "AND fullRegistrationDate\" IS NOT NULL\n" +
            "AND \"overallRating\" is NULL\n" +
            "AND \"taxStatus\" = 'TAXPAYER_UNREGISTERED' \n" +
            "AND \"passportType\" is NULL\n" +
            "AND \"inn\" is not NULL\n" +
            "LIMIT 50;")
    @RegisterRowMapper(ContractorMapper.class)
    List<Contractor> notRegistredInFnsContractors();

    @SqlQuery("SELECT phone FROM \"Contractors\" WHERE phone like '%:phonePart%'")
    @RegisterRowMapper(PhoneMapper.class)
    List<Phone> contractorPhonesLike(@Define("id") String phonePart);

    @SqlUpdate("DELETE " +
            "FROM \"<table>\" " +
            "WHERE \"clientId\" = '<clientId>'")
    @UseStringTemplateEngine
    int deleteObjects(@Define("table") String table, @Define("clientId") String clientId);

}
