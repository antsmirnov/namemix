package db.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Contractor {
    public String inn;
    public String phone;
}
