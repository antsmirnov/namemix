package db.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Phone {
    public String phone;
}
