package db.model.mappers;

import db.model.Contractor;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContractorMapper implements RowMapper<Contractor> {
    public Contractor map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Contractor(rs.getString("inn"), rs.getString("phone"));
    }
}