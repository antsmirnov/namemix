package db.model.mappers;

import db.model.Phone;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PhoneMapper implements RowMapper<Phone> {
    public Phone map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Phone(rs.getString("phone"));
    }
}