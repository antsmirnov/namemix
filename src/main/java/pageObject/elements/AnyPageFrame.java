package pageObject.elements;

import org.openqa.selenium.By;

public class AnyPageFrame {
    public By body = By.cssSelector("body");

    public By notificationsSuccess = By.cssSelector(".Toastify__toast--success");
    public By notificationsError = By.cssSelector(".Toastify__toast--error");
    public By notificationsWarning = By.cssSelector(".Toastify__toast--error");
    public String notificationSuccessPattern = "//*[contains(@class,'Toastify__toast--success') and .//*[text()='%s']]";
    public String notificationWarningPattern = "//*[contains(@class,'Toastify__toast--warning') and .//*[text()='%s']]";
    public String notificationErrorPattern = "//*[contains(@class,'Toastify__toast--error') and .//*[text()='%s']]";

    public By title = By.cssSelector("title");

    public By textVersion = By.className("nmx-menu__version");
    public By btnLogout = By.cssSelector(".nmx-menu__exit");

}
