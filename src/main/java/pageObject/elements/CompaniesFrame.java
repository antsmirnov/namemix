package pageObject.elements;

import org.openqa.selenium.By;

public class CompaniesFrame {
    public By btnAddCompany = By.cssSelector(".icon-plus-app__white");

    public By inputCustomer = By.cssSelector(".deposit-filter__item input");

    public By btnFind = By.cssSelector(".filter-buttons__button-send");
    public By btnClearText = By.cssSelector(".filter-buttons__button-clear-text");

    public By tableCompanyNames = By.xpath("//table/tbody/tr/td[2]/a");
    public By tableCompanyBtnParams = By.cssSelector("i[title='Настройки компании']");
    public By tableCompanyBtnArchive = By.xpath("//div[text()='В архив']");

}
