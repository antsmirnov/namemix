package pageObject.elements;

import org.openqa.selenium.By;

public class CompanyCardFrame {
    public By textHeader = By.xpath("//*[contains(@class, 'client-list-header')]");
    public By inputHeader = By.cssSelector(".client-list-header input");
    public By promptHeader = By.cssSelector(".client-list-header .prompt");
    public By btnHeaderEdit = By.xpath("//*[contains(@class, 'client-list-header')]/i[text()='create']");
    public By btnHeaderCancelEdit = By.xpath("//*[contains(@class, 'client-list-header')]/i[text()='clear']");
    public By btnHeaderApplyEdit = By.xpath("//*[contains(@class, 'client-list-header')]/i[text()='check']");

    public By tabInfo = By.xpath("//a[contains(text(),'Информация')]");
    public By tabNamemixDocs = By.xpath("//a[contains(text(),'Документы Наймикс')]");
    public By tabEmployers = By.xpath("//a[contains(text(),'Сотрудники')]");
    /* add other tabs here */
}
