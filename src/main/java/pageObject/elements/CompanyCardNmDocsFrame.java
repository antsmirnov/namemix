package pageObject.elements;

import org.openqa.selenium.By;

public class CompanyCardNmDocsFrame {

    public By btnAddContract = By.xpath("//div[./*[text()='Добавить договор']]");

    public By attachedDocuments = By.className("document-element"); // not uploaded, just attached
    public By textsAttachedDocument = By.cssSelector(".document-element__text");
    public By btnsDeleteAttachedDocuments = By.cssSelector(".document-element__close-icon");

    public By btnChooseFile = By.xpath("//button[text()='Выбрать файл']");
    public By dropzoneText = By.className("dropzone-app__content");
    public By inputFile = By.cssSelector(".dropzone-app__input-file > input[type='file']");

    public By btnCancel = By.className("apply-buttons__cancel");
    public By btnSave = By.className("apply-buttons__submit");

}
