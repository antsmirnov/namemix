package pageObject.elements;

import org.openqa.selenium.By;

public class CompanyCardNmDocsInfoBlock {
    public By container = By.className("agency-contract-info");

    public By textContractNumber = By.xpath("//*[./*[contains(text(), 'Номер агентского договора')]]/*[@class='label-text__content ']");
    public By textContractDate = By.xpath("//*[./*[contains(text(), 'Дата агентского договора')]]/*[@class='label-text__content ']");

    public By inputContractNumber = By.cssSelector("[name='nmContractNumber']");
    public By inputContractDate = By.cssSelector(".react-datepicker-ignore-onclickoutside");

}
