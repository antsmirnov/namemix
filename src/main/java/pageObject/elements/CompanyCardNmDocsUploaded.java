package pageObject.elements;

import org.openqa.selenium.By;

public class CompanyCardNmDocsUploaded {

    public By containerActual = By.className("agency-contract-actual");
    public By containerHistory = By.className("agency-contract-history");

    public By btnOpenCloseHistory = By.cssSelector(".agency-contract-history-header__icon");

    // Строка соответствующая версии загруженного договора (не важно, в истории или актульная)
    public By oneVersionRow =
            By.xpath("//div[contains(@class, 'flex') and not(contains(@class, 'pagination')) and not(./*[contains(., 'Дата добавления')])]");

    public By linksFileName = By.className("app-link"); // relative locator

    public By btnActualVersionEdit = By.cssSelector(".agency-contract-actual i[title='Редактировать']");

    public By uploadedDocuments = By.cssSelector(".agency-contract-actual .document-element");
    public By textsUploadedDocuments = By.cssSelector(".document-element__text"); // relative locator
    public By btnsDeleteUploadedDocuments = By.cssSelector(".document-element__close-icon"); // relative locator

    public By btnChooseFile = By.xpath("//*[contains(@class, 'agency-contract-actual')]/.//button[text()='Выбрать файл']");
    public By dropzoneText = By.cssSelector(".agency-contract-actual .dropzone-app__content");
    public By inputFile = By.cssSelector(".agency-contract-actual .dropzone-app__input-file > input[type='file']");

    public By btnSave = By.cssSelector(".agency-contract-actual .apply-buttons__submit");
    public By btnCancel = By.cssSelector(".agency-contract-actual .apply-buttons__cancel");

}
