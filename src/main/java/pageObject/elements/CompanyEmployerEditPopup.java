package pageObject.elements;

import org.openqa.selenium.By;

public class CompanyEmployerEditPopup {
    public By inputLastName = By.cssSelector("input[name=lastName]");
    public By inputFirstName = By.cssSelector("input[name=firstName]");
    public By inputPatronymic = By.cssSelector("input[name=patronymic]");
    public By inputSnils = By.cssSelector("input[name=snils]");
    public By inputInn = By.cssSelector("input[name=inn]");

    public By dropDownBtnPosition = By.cssSelector("div[name=position] i");
    public By dropDownOptionsPosition = By.cssSelector("div[name=position] div[role=option]");
    public By dropDownSelectedPosition = By.cssSelector("div[name=position] div.text");

    public By inputPhone = By.cssSelector("input[name=phone]");
    public By inputEmail = By.cssSelector("input[name=email]");

    public By dropDownBtnRole = By.cssSelector("div[name=role] i");
    public By dropDownOptionsRole = By.cssSelector("div[name=role] div[role=option]");
    public By dropDownSelectedRole = By.cssSelector("div[name=role] div.text");

    public By inputPassword = By.cssSelector("input[name=password]");
    public By inputRepeatPassword = By.cssSelector("input[name=repeatPassword]");

    public By btnCancel = By.xpath("//button[contains(text(), 'Отменить')]");
    public By btnAdd = By.cssSelector(".app-button-primary__save");

    public By promptFirstName = By.xpath("//*[contains(@class,'field') and .//*[@name='firstName']]/*[contains(@class, 'prompt')]");
    public By promptLastName = By.xpath("//*[contains(@class,'field') and .//*[@name='lastName']]/*[contains(@class, 'prompt')]");
    public By promptPatronymic = By.xpath("//*[contains(@class,'field') and .//*[@name='patronymic']]/*[contains(@class, 'prompt')]");
    public By promptSnils = By.xpath("//*[contains(@class,'field') and .//*[@name='snils']]/*[contains(@class, 'prompt')]");
    public By promptInn = By.xpath("//*[contains(@class,'field') and .//*[@name='inn']]/*[contains(@class, 'prompt')]");
    public By promptPosition = By.xpath("//*[contains(@class,'field') and .//*[@name='position']]/*[contains(@class, 'prompt')]");
    public By promptPhone = By.xpath("//*[contains(@class,'field') and .//*[@name='phone']]/*[contains(@class, 'prompt')]");
    public By promptEmail = By.xpath("//*[contains(@class,'field') and .//*[@name='email']]/*[contains(@class, 'prompt')]");
    public By promptRole = By.xpath("//*[contains(@class,'field') and .//*[@name='role']]/*[contains(@class, 'prompt')]");
    public By promptPassword = By.xpath("//*[contains(@class,'field') and .//*[@name='password']]/*[contains(@class, 'prompt')]");
    public By promptRepeatPassword = By.xpath("//*[contains(@class,'field') and .//*[@name='repeatPassword']]/*[contains(@class, 'prompt')]");

    public By btnChangePassword = By.cssSelector(".client-member-new-body-btn_password");

}
