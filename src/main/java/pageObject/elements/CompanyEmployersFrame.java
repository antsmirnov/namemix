package pageObject.elements;

import org.openqa.selenium.By;

public class CompanyEmployersFrame {
    public By btnAddEmployer = By.cssSelector(".client-member-list__add");

    public By tableEmployerNames = By.xpath("//div/table/tbody/tr/td[1]");
    public By tableEmployerRows = By.xpath("//table/tbody/tr");
    public By tableEmployerBtnEdit = By.xpath(".//span[text()='create']");        // (!) relative path
    public By tableEmployerBtnArchive = By.xpath(".//span[text()='archive']");    // (!) relative path

}
