package pageObject.elements;

import org.openqa.selenium.By;

public class CompanyParamsEditPopup {

    public By inputCommissionRate = By.cssSelector("input[name=currentCommissionRate]");

    public By btnSave = By.className("apply-buttons__submit");
    public By btnCancel = By.className("apply-buttons__cancel");

    public By promptCommissionRate = By.xpath("//div[./div/input[@name='currentCommissionRate']]/*[contains(@class, 'prompt')]");

}
