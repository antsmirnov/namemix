package pageObject.elements;

import org.openqa.selenium.By;

public class LoginFrame {
    public By inputEmail = By.cssSelector("[name='login']");
    public By inputPassword = By.cssSelector("[name='password']");
    public By btnLogin = By.xpath("//button[contains(text(), 'Войти')]");
}
