package pageObject.elements;

import org.openqa.selenium.By;

public class NewCompanyFrame {

    public By container = By.cssSelector("div.client-new");

    public By labelHeader = By.cssSelector(".client-new__header-title");

    /* labels */
    public By labelFormOfRegistration = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Форма регистрации')]");
    public By labelOfficialName = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Официальное название')]");
    public By labelShortName = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Сокращенное название')]");
    public By labelInn = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'ИНН')]");
    public By labelActualAddress = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Фактический адрес')]");
    public By labelContactPersonName = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'ФИО')]");
    public By labelContactPersonPhone = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Телефон')]");
    public By labelContactPersonEmail = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'E-mail')]");
    public By labelPromoCode = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Промо')]");
    public By labelCategory = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Категория')]");
    public By labelCommission = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Комиссия')]");
    public By labelInsuranceNeeds = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'страхование')]");
    public By labelPaymentByRegistries = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'реестрами')]");
    public By labelOrdersWithoutPledge = By.xpath("//div[contains(@class, 'client-new__row-label') and contains(text(), 'Заказы без обеспеч')]");
    public By labelLimit = By.cssSelector(".client-new__row-label_no-width");

    /* inputs */
    public By dropDownBtnFormOfRegistration = By.xpath("//div[./div[contains(text(), 'Форма регистрации')]]/.//i");
    public By optionFormOfRegistration = By.xpath("//div[@name='clientType']/.//div[@role='option']");
    public By dropDownSelectedFormOfRegistration = By.xpath("//div[./div[contains(text(), 'Форма')]]/.//div[@class='text']");

    public By inputOfficialName = By.cssSelector("input[name=fullName]");
    public By inputShortName = By.cssSelector("input[name=name]");
    public By inputFioIndividual = By.cssSelector("input[name=fullName]");
    public By inputInn = By.cssSelector("input[name=inn]");
    public By inputEin = By.cssSelector("input[name=inn]");

    public By inputActualAddress = By.cssSelector("input.react-dadata__input");
    public By inputActualAddressSuggestions = By.cssSelector("div[name=address].react-dadata__suggestion");

    public By inputContactPersonName = By.cssSelector("input[name=representativeName]");
    public By inputContactPersonPhone = By.cssSelector("input[name=representativePhone]");
    public By inputContactPersonEmail = By.cssSelector("input[name=representativeEmail]");

    public By inputPromoCode = By.cssSelector(".client-new__row-promocode input");
    public By btnCheckPromoCode = By.xpath("//button[contains(text(), 'Проверить')]");

    public By dropDownBtnCategory = By.xpath("//div[./div[contains(text(), 'Категория')]]/.//i");
    public By dropDownSelectedCategory = By.xpath("//div[./div[contains(text(), 'Категория')]]/.//div[@class='text']");
    public By dropDownCategoryOptions = By.xpath("//div[./div[contains(text(), 'Категория')]]/.//div[@role='option']");

    public By inputCommission = By.cssSelector("input[name=currentCommissionRate]");

    public By flagInsuranceNeeds = By.cssSelector("input[name=insuranceAvailable]");
    public By flagPaymentByRegistries = By.cssSelector("input[name=registryPaymentsAvailable]");
    public By flagOrdersWithoutPledge = By.cssSelector("input[name=ordersUnsecured]");

    public By inputLimit = By.cssSelector("input[name=ordersLimit]");
    public By inputLimitParent = By.xpath("//div[contains(@class, 'input') and ./input[@name='ordersLimit']]");

    public By btnCancel = By.cssSelector(".apply-buttons__cancel");
    public By btnAdd = By.cssSelector("button.apply-buttons__submit");

    public By promptOfficialName = By.xpath("//div[./*[contains(text(), 'Официальное название')]]/.//div[contains(@class, 'prompt')]");
    public By promptShortName = By.xpath("//div[./*[contains(text(), 'Сокращенное название')]]/.//div[contains(@class, 'prompt')]");
    public By promptInn = By.xpath("//div[./*[contains(text(), 'ИНН')]]/.//div[contains(@class, 'prompt')]");
    public By promptActualAddress = By.xpath("//div[./*[contains(text(), 'Фактический адрес')]]/.//div[contains(@class, 'prompt')]");
    public By promptContractorPersonName = By.xpath("//div[./*[contains(text(), 'ФИО')]]/.//div[contains(@class, 'prompt')]");
    public By promptContractorPersonPhone = By.xpath("//div[./*[contains(text(), 'Телефон')]]/.//div[contains(@class, 'prompt')]");
    public By promptContractorPersonEmail = By.xpath("//div[./*[contains(text(), 'E-mail')]]/.//div[contains(@class, 'prompt')]");
    public By promptPromo = By.xpath("//div[./*[contains(text(), 'Промо')]]/.//div[contains(@class, 'prompt')]");
    public By promptCategory = By.xpath("//div[./*[contains(text(), 'Категория')]]/.//div[contains(@class, 'prompt')]");
    public By promptCommission = By.xpath("//div[./*[contains(text(), 'Комиссия')]]/.//div[contains(@class, 'prompt')]");

}
