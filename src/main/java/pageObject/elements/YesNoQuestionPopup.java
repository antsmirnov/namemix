package pageObject.elements;

import org.openqa.selenium.By;

public class YesNoQuestionPopup {
    public By btnYes = By.xpath("//div[contains(@class, 'modal')]/.//button[text()='Да']");
    public By btnNo = By.xpath("//div[contains(@class, 'modal')]/.//button[text()='Нет']");
    public By textQuestion = By.cssSelector(".modal .content");

}
