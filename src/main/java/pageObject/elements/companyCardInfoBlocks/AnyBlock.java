package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class AnyBlock {
    public By btnBlockEdit = By.cssSelector("i[title='Редактировать']");
    public By btnBlockCancel = By.cssSelector("i[title='Отмена']");
    public By btnBlockSave = By.cssSelector("i.check");
}
