package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class BankRequisitesBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Банковские реквизиты']]");
    public By headerBankRequisites = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Банковские')]");

    public By labelBank = By.xpath("//*[@class='label-text__name ' and text()='Банк']");
    public By labelBik = By.xpath("//*[@class='label-text__name ' and text()='БИК']");
    public By labelRS = By.xpath("//*[@class='label-text__name ' and text()='р/c']");
    public By labelKS = By.xpath("//*[@class='label-text__name ' and text()='к/с']");
    public By labelSwift = By.xpath("//*[@class='label-text__name ' and text()='SWIFT-код']");
    public By labelAccountNumber = By.xpath("//*[@class='label-text__name ' and contains(text(), 'Номер счета')]");
    public By labelRoutingNumber = By.xpath("//*[@class='label-text__name ' and contains(text(), 'Банковский код')]");

    public By textBank = By.xpath("//div[*[@class='label-text__name ' and text()='Банк']]/*[@class='label-text__content ']");
    public By textAddress = By.cssSelector(".client-card-block__bank-address .label-text__content");
    public By textBik = By.xpath("//div[*[@class='label-text__name ' and text()='БИК']]/*[@class='label-text__content ']");
    public By textRS = By.xpath("//div[*[@class='label-text__name ' and text()='р/c']]/*[@class='label-text__content ']");
    public By textKS = By.xpath("//div[*[@class='label-text__name ' and text()='к/с']]/*[@class='label-text__content ']");
    public By textSwift = By.xpath("//div[*[@class='label-text__name ' and text()='SWIFT-код']]/*[@class='label-text__content ']");
    public By textAccountNumber = By.xpath("//div[*[@class='label-text__name ' and contains(text(), 'Номер счета')]]/*[@class='label-text__content ']");
    public By textRoutingNumber = By.xpath("//div[*[@class='label-text__name ' and contains(text(), 'Банковский код')]]/*[@class='label-text__content ']");

    public By inputBank = By.cssSelector("[name='bankName']");
    public By inputAddress = By.cssSelector("[placeholder='Адрес банка']");
    public By inputBik = By.cssSelector("[name='bic']");
    public By inputRS = By.cssSelector("[name='bankCheckAccount']");
    public By inputKS = By.cssSelector("[name='bankCorrAccount']");
    public By inputSwift = By.cssSelector("[placeholder='SWIFT-код']");
    public By inputAccountNumber = By.cssSelector("[name='bankCheckAccount']");
    public By inputRoutingNumber = By.cssSelector("[placeholder='Банковский код (ACH Routing number)']");

    public By promptBank = By.xpath("//*[./div/input[@name='bankName']]/*[contains(@class, 'prompt')]");
    public By promptAddress = By.xpath("//*[./div/input[@placeholder='Адрес банка']]/*[contains(@class, 'prompt')]");
    public By promptBik = By.xpath("//*[./div/input[@name='bic']]/*[contains(@class, 'prompt')]");
    public By promptRS = By.xpath("//*[./div/input[@name='bankCheckAccount']]/*[contains(@class, 'prompt')]");
    public By promptKS = By.xpath("//*[./div/input[@name='bankCorrAccount']]/*[contains(@class, 'prompt')]");
    public By promptSwift = By.xpath("//*[./div/input[@placeholder='SWIFT-код']]/*[contains(@class, 'prompt')]");
    public By promptAccountNumber = By.xpath("//*[./div/input[@name='bankCheckAccount']]/*[contains(@class, 'prompt')]");
    public By promptRoutingNumber = By.xpath("//*[./div/input[@placeholder='Банковский код (ACH Routing number)']]/*[contains(@class, 'prompt')]");

}
