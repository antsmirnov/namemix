package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class CompanyCategoryBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Категория компании']]");
    public By headerCompanyCategory = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Категория')]");

    public By labelCategory = By.xpath("//*[@class='label-text__name ' and text()='Категория']");
    public By labelOkved = By.xpath("//*[@class='label-text__name ' and text()='ОКВЭД']");
    public By labelBrigadierAccess = By.xpath("//*[@class='label-text__name ' and text()='Доступ к фукционалу бригадира']");

    public By textCategory = By.xpath("//div[*[@class='label-text__name ' and text()='Категория']]/*[@class='label-text__content']");
    public By textOkved = By.xpath("//div[*[@class='label-text__name ' and text()='ОКВЭД']]/*[@class='label-text__content']");
    public By textBrigadierAccess = By.xpath("//div[*[@class='label-text__name ' and text()='Доступ к фукционалу бригадира']]/*[@class='label-text__content ']");

    public By inputDdlCategory = By.cssSelector("[name='categoryId']");
    public By inputOkved = By.cssSelector("[name='okved']");
    public By inputBrigadierAccess = By.xpath("//*[@class='label-text__name ' and text()='Доступ к фукционалу бригадира']");
}
