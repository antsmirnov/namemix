package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class CompanyParametersBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Параметры компании']]");
    public By header = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Параметры')]");

    public By labelNonResidentsNotAllowed = By.cssSelector("input[name=prohibitionContractingNonResidents] + label");

    public By flagNonResidentsNotAllowed = By.cssSelector("input[name=prohibitionContractingNonResidents]");
}
