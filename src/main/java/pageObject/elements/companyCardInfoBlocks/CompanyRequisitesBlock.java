package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class CompanyRequisitesBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Реквизиты компании']]");
    public By headerCompanyRequisites = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Реквизиты компании')]");

    public By labelOfficialName = By.xpath("//*[@class='label-text__name ' and contains(text(), 'Официальное')]");
    public By labelFioIp = By.xpath("//*[@class='label-text__name ' and contains(text(), 'ФИО ИП')]");

    public By labelRegistrationForm = By.xpath("//*[@class='label-text__name ' and contains(text(), 'Форма')]");
    public By labelFioContactPerson = By.xpath("//*[@class='label-text__name ' and text()='ФИО контактного лица']");
    public By labelPhoneContactPerson = By.xpath("//*[@class='label-text__name ' and text()='Номер телефона контактного лица']");
    public By labelEmailContactPerson = By.xpath("//*[@class='label-text__name ' and text()='E-mail контактного лица']");
    public By labelActualAddress = By.xpath("//*[@class='label-text__name ' and contains(text(), 'Фактический адрес')]");
    public By labelRegisteredAddress = By.xpath("//*[@class='label-text__name ' and text()='Адрес регистрации']");
    public By labelCompanyPhone = By.xpath("//*[@class='label-text__name ' and text()='Номер телефона компании']");
    public By labelCompanyEmail = By.xpath("//*[@class='label-text__name ' and text()='E-mail компании']");

    public By labelPhone = By.xpath("//*[@class='label-text__name ' and text()='Номер телефона']");
    public By labelEmail = By.xpath("//*[@class='label-text__name ' and text()='E-mail']");

    public By labelEin = By.xpath("//*[@class='label-text__name ' and contains(text(),'EIN')]");
    public By labelInn = By.xpath("//*[@class='label-text__name ' and contains(text(),'ИНН')]");
    public By labelKpp = By.xpath("//*[@class='label-text__name ' and contains(text(),'КПП')]");
    public By labelOgrn = By.xpath("//*[@class='label-text__name ' and contains(text(),'ОГРН')]");
    public By labelOgrnIp = By.xpath("//*[@class='label-text__name ' and contains(text(),'ОГРНИП')]");

    public By textOfficialName = By.xpath("//div[*[@class='label-text__name ' and contains(text(), 'Официальное')]]/*[@class='label-text__content ']");
    public By textFioIp = By.xpath("//div[*[@class='label-text__name ' and contains(text(), 'ФИО ИП')]]/*[@class='label-text__content ']");

    public By textRegistrationForm = By.xpath("//div[*[@class='label-text__name ' and contains(text(), 'Форма')]]/*[@class='label-text__content ']");
    public By textFioContactPerson = By.xpath("//div[*[@class='label-text__name ' and text()='ФИО контактного лица']]/*[@class='label-text__content ']");
    public By textPhoneContactPerson = By.xpath("//div[*[@class='label-text__name ' and text()='Номер телефона контактного лица']]/*[@class='label-text__content ']");
    public By textEmailContactPerson = By.xpath("//div[*[@class='label-text__name ' and text()='E-mail контактного лица']]/*[@class='label-text__content ']");
    public By textActualAddress = By.xpath("//div[*[@class='label-text__name ' and contains(text(), 'Фактический адрес')]]/*[@class='label-text__content ']");
    public By textRegisteredAddress = By.xpath("//div[*[@class='label-text__name ' and text()='Зарегистрированный адрес']]/*[@class='label-text__content ']");
    public By textRegisteredAddressIp = By.xpath("//div[*[@class='label-text__name ' and text()='Адрес регистрации']]/*[@class='label-text__content ']");
    public By textCompanyPhone = By.xpath("//div[*[@class='label-text__name ' and text()='Номер телефона компании']]/*[@class='label-text__content ']");
    public By textCompanyEmail = By.xpath("//div[*[@class='label-text__name ' and text()='E-mail компании']]/*[@class='label-text__content ']");

    public By textPhone = By.xpath("//div[*[@class='label-text__name ' and text()='Номер телефона']]/*[@class='label-text__content ']");
    public By textEmail = By.xpath("//div[*[@class='label-text__name ' and text()='E-mail']]/*[@class='label-text__content ']");

    public By textEin = By.xpath("//div[*[@class='label-text__name ' and contains(text(),'EIN')]]/*[@class='label-text__content ']");
    public By textInn = By.xpath("//div[*[@class='label-text__name ' and contains(text(),'ИНН')]]/*[@class='label-text__content ']");
    public By textKpp = By.xpath("//div[*[@class='label-text__name ' and contains(text(),'КПП')]]/*[@class='label-text__content ']");
    public By textOgrn = By.xpath("//div[*[@class='label-text__name ' and contains(text(),'ОГРН')]]/*[@class='label-text__content ']");
    public By textOgrnIp = By.xpath("//div[*[@class='label-text__name ' and contains(text(),'ОГРНИП')]]/*[@class='label-text__content ']");

    /* inputs: */
    public By inputOfficialName = By.cssSelector("[name='fullName']");
    public By inputFioIp = By.cssSelector("[name='fullName']");
    public By inputRegistrationForm = By.cssSelector("input[disabled][type=text]"); // TODO: написать нормальный локатор

    public By inputFioContactPerson = By.xpath("//input[@name='representativeName']");
    public By inputPhoneContactPerson = By.xpath("//input[@name='representativePhone']");
    public By inputEmailContactPerson = By.xpath("//input[@name='representativeEmail']");
    public By inputActualAddress = By.cssSelector("[placeholder='Фактический адрес']");
    public By inputRegisteredAddress = By.cssSelector("[placeholder='Зарегистрированный адрес']");

    public By inputCompanyPhone = By.cssSelector("[name='phone']");
    public By inputCompanyEmail = By.cssSelector("[name='email']");
    public By inputPhone = By.cssSelector("[name='phone']"); // Для ИП
    public By inputEmail = By.cssSelector("[name='email']");

    public By inputEin = By.cssSelector("[name='inn']");  // Не ошибка, то-же поле
    public By inputInn = By.cssSelector("[name='inn']");
    public By inputKpp = By.cssSelector("[name='registrationReasonCode']");
    public By inputOgrn = By.cssSelector("[name='ogrn']");
    public By inputOgrnIp = By.cssSelector("[name='ogrn']");

    /* prompts: */
    public By promptOfficialName = By.xpath("//*[./div/input[@name='fullName']]/*[contains(@class, 'prompt')]");
    public By promptFioIp = By.xpath("//*[./div/input[@name='fullName']]/*[contains(@class, 'prompt')]");

    public By promptFioContactPerson = By.xpath("//*[./div/input[@name='representativeName']]/*[contains(@class, 'prompt')]");
    public By promptPhoneContactPerson = By.xpath("//*[./div/input[@name='representativePhone']]/*[contains(@class, 'prompt')]");
    public By promptEmailContactPerson = By.xpath("//*[./div/input[@name='representativeEmail']]/*[contains(@class, 'prompt')]");
    public By promptActualAddress = By.xpath("//*[./div/input[@placeholder='Фактический адрес']]/*[contains(@class, 'prompt')]");
    public By promptRegisteredAddress = By.xpath("//*[./div/input[@placeholder='Зарегистрированный адрес']]/*[contains(@class, 'prompt')]");

    public By promptCompanyPhone = By.xpath("//*[./div/input[@name='phone']]/*[contains(@class, 'prompt')]");
    public By promptCompanyEmail = By.xpath("//*[./div/input[@name='email']]/*[contains(@class, 'prompt')]");
    public By promptPhone = By.xpath("//*[./div/input[@name='phone']]/*[contains(@class, 'prompt')]");    // ИП
    public By promptEmail = By.xpath("//*[./div/input[@name='email']]/*[contains(@class, 'prompt')]");

    public By promptEin = By.xpath("//*[./div/input[@name='inn']]/*[contains(@class, 'prompt')]");        // Не ошибка, то-же поле
    public By promptInn = By.xpath("//*[./div/input[@name='inn']]/*[contains(@class, 'prompt')]");
    public By promptKpp = By.xpath("//*[./div/input[@name='registrationReasonCode']]/*[contains(@class, 'prompt')]");
    public By promptOgrn = By.xpath("//*[./div/input[@name='ogrn']]/*[contains(@class, 'prompt')]");
    public By promptOgrnIp = By.xpath("//*[./div/input[@name='ogrn']]/*[contains(@class, 'prompt')]");
}
