package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class DirectorBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Руководитель']]");
    public By headerDirector = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Руководитель')]");

    public By labelFio = By.xpath("//*[@class='label-text__name ' and text()='ФИО руководителя']");
    public By labelPhone = By.xpath("//*[@class='label-text__name ' and text()='Номер телефона руководителя']");

    public By textFio = By.xpath("//div[*[@class='label-text__name ' and text()='ФИО руководителя']]/*[@class='label-text__content ']");
    public By textPhone = By.xpath("//div[*[@class='label-text__name ' and text()='Номер телефона руководителя']]/*[@class='label-text__content ']");

    public By inputFio = By.cssSelector("[name='managerName']");
    public By inputPhone = By.cssSelector("[name='managerPhone']");

    public By promptFio = By.xpath("//*[./div/input[@name='managerName']]/*[contains(@class, 'prompt')]");
    public By promptPhone = By.xpath("//*[./div/input[@name='managerPhone']]/*[contains(@class, 'prompt')]");
}
