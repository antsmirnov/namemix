package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class LimitsBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Лимиты']]");
    public By headerLimits = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Лимиты')]");

    public By labelLimitOfOne = By.xpath("//*[@class='label-text__name ' and text()='Лимит 1 транзакции, руб']");
    public By labelLimitPerDay = By.xpath("//*[@class='label-text__name ' and text()='Лимит в сутки на 1 исполнителя, руб']");
    public By labelLimitPerMonth = By.xpath("//*[@class='label-text__name ' and text()='Лимит в месяц на 1 исполнителя, руб']");
    public By labelLimitForOrdersWithoutPledge = By.xpath("//*[@class='label-text__name ' and contains(text(),'без обеспечения')]");

    public By textLimitOfOne =
            By.xpath("//div[*[@class='label-text__name ' and text()='Лимит 1 транзакции, руб']]/*[@class='label-text__content ']");
    public By textLimitPerDay =
            By.xpath("//div[*[@class='label-text__name ' and text()='Лимит в месяц на 1 исполнителя, руб']]/*[@class='label-text__content ']");
    public By textLimitPerMonth =
            By.xpath("//div[*[@class='label-text__name ' and text()='Лимит в месяц на 1 исполнителя, руб']]/*[@class='label-text__content ']");
    public By textLimitForOrdersWithoutPledge =
            By.xpath("//div[*[@class='label-text__name ' and contains(text(),'без обеспечения')]]/*[@class='label-text__content ']");
}
