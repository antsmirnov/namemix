package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class MainAccountantBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Главный бухгалтер']]");
    public By headerMainAccountant = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'бухгалтер')]");

    public By labelFio = By.xpath("//*[@class='label-text__name ' and text()='ФИО главного бухгалтера']");
    public By labelPhone = By.xpath("//*[@class='label-text__name ' and text()='Телефон']");

    public By textFio = By.xpath("//div[*[@class='label-text__name ' and text()='ФИО главного бухгалтера']]/*[@class='label-text__content ']");
    public By textPhone = By.xpath("//div[*[@class='label-text__name ' and text()='Телефон']]/*[@class='label-text__content ']");

    public By inputFio = By.cssSelector("[name='accountantName']");
    public By inputPhone = By.cssSelector("[name='accountantPhone']");

    public By promptFio = By.xpath("//*[./div/input[@name='accountantName']]/*[contains(@class, 'prompt')]");
    public By promptPhone = By.xpath("//*[./div/input[@name='accountantPhone']]/*[contains(@class, 'prompt')]");
}
