package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class PodpisantBlock {
    public By container = By.xpath("//div[contains(@class, 'card-app ') and .//div[text()='Подписант']]");
    public By headerPodpisant = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Подписант')]");

    public By labelRole = By.xpath("//*[@class='label-text__name ' and text()='Должность']");
    public By labelTranscript = By.xpath("//*[@class='label-text__name ' and text()='Расшифровка']");

    public By textRole = By.xpath("//div[*[@class='label-text__name ' and text()='Должность']]/*[@class='label-text__content ']");
    public By textTranscript = By.xpath("//div[*[@class='label-text__name ' and text()='Расшифровка']]/*[@class='label-text__content ']");

    public By inputRole = By.cssSelector("[name='signatory']");
    public By inputTranscript = By.cssSelector("[name='signatoryDecoding']");

    public By promptRole = By.xpath("//*[./div/input[@name='signatory']]/*[contains(@class, 'prompt')]");
    public By promptTranscript = By.xpath("//*[./div/input[@name='signatoryDecoding']]/*[contains(@class, 'prompt')]");
}
