package pageObject.elements.companyCardInfoBlocks;

import org.openqa.selenium.By;

public class PredstavitelBlock {
    public By container = By.xpath("//div[@class='card-app ' and .//div[text()='Представитель']]");
    public By headerPredstavitel = By.xpath("//*[@class='card-app__header-title' and contains(text(), 'Представитель')]");

    public By labelFio = By.xpath("//*[@class='label-text__name ' and text()='В лице']");
    public By labelBased = By.xpath("//*[@class='label-text__name ' and text()='На основании']");

    public By textFio = By.xpath("//div[*[@class='label-text__name ' and text()='В лице']]/*[@class='label-text__content ']");
    public By textBased = By.xpath("//div[*[@class='label-text__name ' and text()='На основании']]/*[@class='label-text__content ']");

    public By inputFio = By.cssSelector("[name='representativeNameGenitive']");
    public By inputBased = By.cssSelector("[name='rightToSignDocumentDetails']");

    public By promptFio = By.xpath("//*[./div/input[@name='representativeNameGenitive']]/*[contains(@class, 'prompt')]");
    public By promptBased = By.xpath("//*[./div/*[@name='rightToSignDocumentDetails']]/*/*[contains(@class, 'prompt')]");
}
