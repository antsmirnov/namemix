package pageObject.pages;

import pageObject.elements.AnyPageFrame;
import pageObject.elements.YesNoQuestionPopup;

public class AnyPage {
    public static AnyPageFrame onAnyPage = new AnyPageFrame();
    public static YesNoQuestionPopup onYesNoQuestionPopup = new YesNoQuestionPopup();
}
