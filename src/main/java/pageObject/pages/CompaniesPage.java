package pageObject.pages;

import pageObject.elements.CompaniesFrame;
import pageObject.elements.CompanyParamsEditPopup;

public class CompaniesPage {
    public static CompaniesFrame onCompaniesFrame = new CompaniesFrame();
    public static CompanyParamsEditPopup onCompanyParamsEditPopup = new CompanyParamsEditPopup();
}
