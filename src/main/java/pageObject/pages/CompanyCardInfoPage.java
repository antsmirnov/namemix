package pageObject.pages;

import pageObject.elements.companyCardInfoBlocks.*;

public class CompanyCardInfoPage {
    public static AnyBlock onAnyBlock = new AnyBlock();
    public static BankRequisitesBlock onBankRequisitesBlock = new BankRequisitesBlock();
    public static CompanyCategoryBlock onCompanyCategoryBlock = new CompanyCategoryBlock();
    public static CompanyRequisitesBlock onRequisitesBlock = new CompanyRequisitesBlock();
    public static DirectorBlock onDirectorBlock = new DirectorBlock();
    public static LimitsBlock onLimitsBlock = new LimitsBlock();
    public static MainAccountantBlock onMainAccountantBlock = new MainAccountantBlock();
    public static PodpisantBlock onPodpisantBlock = new PodpisantBlock();
    public static PredstavitelBlock onPredstavitelBlock = new PredstavitelBlock();
    public static CompanyParametersBlock onCompanyParametersBlock = new CompanyParametersBlock();
}
