package pageObject.pages;

import pageObject.elements.CompanyCardNmDocsFrame;
import pageObject.elements.CompanyCardNmDocsInfoBlock;
import pageObject.elements.CompanyCardNmDocsUploaded;

public class CompanyCardNmDocsPage {
    public static CompanyCardNmDocsFrame companyCardNmDocsFrame = new CompanyCardNmDocsFrame();
    public static CompanyCardNmDocsInfoBlock companyCardNamemixDocsInfoBlock = new CompanyCardNmDocsInfoBlock();
    public static CompanyCardNmDocsUploaded companyCardNmDocsUploaded = new CompanyCardNmDocsUploaded();

}
