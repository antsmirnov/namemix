package pageObject.pages;

import pageObject.elements.CompanyEmployerEditPopup;
import pageObject.elements.CompanyEmployersFrame;

public class CompanyEmployersPage {
    public static CompanyEmployersFrame onCompanyEmployersFrame = new CompanyEmployersFrame();
    public static CompanyEmployerEditPopup onCompanyEmployerEditPopup = new CompanyEmployerEditPopup();
}
