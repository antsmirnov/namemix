package service;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.google.common.collect.ImmutableMap;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import service.webDrivers.LocalWebDriverProvider;
import service.webDrivers.RemoteWebDriverProvider;
import steps.*;
import steps.apis.api.ApiAuthSteps;
import steps.apis.api.ApiClientsSteps;
import steps.apis.nmApi.*;
import steps.db.DbSteps;
import steps.elements.CompanyCardHeaderSteps;
import testData.models.Company;

import java.util.ArrayList;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;

/**
 * Links to test steps classes for using in test classes
 */
public abstract class BaseTest {
    protected static volatile TestsConfig testsConfig;
    protected static volatile String uiVersion = "";

    // список компаний, созданных во время теста и подлежащих удалению
    protected ArrayList<String> temporaryCompaniesIds = new ArrayList<>();
    // UI
    protected BaseSteps baseSteps = new BaseSteps();
    protected CompaniesSteps companiesSteps = new CompaniesSteps();
    protected NewCompanySteps newCompanySteps = new NewCompanySteps();
    protected CompanyCardInfoSteps companyCardInfoSteps = new CompanyCardInfoSteps();
    protected CompanyCardNmDocsContractSteps companyCardNmDocsContractSteps = new CompanyCardNmDocsContractSteps();
    protected CompanyCardNmDocsContractUploadedSteps companyCardNmDocsContractUploadedSteps = new CompanyCardNmDocsContractUploadedSteps();
    protected CompanyCardHeaderSteps companyCardHeaderSteps = new CompanyCardHeaderSteps();
    protected CompanyParamsEditPopupSteps companyParamsEditPopupSteps = new CompanyParamsEditPopupSteps();
    protected CompanyCardEmployersSteps companyCardEmployersSteps = new CompanyCardEmployersSteps();
    // NM API
    protected AuthSteps nmApiAuthSteps = new AuthSteps();
    protected ApiDepositSteps nmApiDepositSteps = new ApiDepositSteps();
    protected ContractorStatusSteps nmApiContractorStatusSteps = new ContractorStatusSteps();
    protected ContractInitSteps nmApiContractInitSteps = new ContractInitSteps();
    protected ContractSignSteps nmApiContractSignSteps = new ContractSignSteps();
    protected AddWithPaymentSteps nmApiAddWithPaymentSteps = new AddWithPaymentSteps();
    // API
    protected ApiAuthSteps apiAuthSteps = new ApiAuthSteps();
    protected ApiClientsSteps apiClientsSteps = new ApiClientsSteps();
    // DB
    protected DbSteps dbSteps = new DbSteps();

    @AfterSuite(description = "Закрытие браузеров, сохранение параметров отчета", alwaysRun = true)
    public static void afterSuite() {
        ImmutableMap.Builder<String, String> envProps =
                ImmutableMap.<String, String>builder().put("Тестовый стенд", testsConfig.baseUrl());
        if (!uiVersion.equals("")) {
            envProps.put("Версия UI", uiVersion);
        }
        allureEnvironmentWriter(envProps.build());
    }

    @BeforeSuite(description = "Инициализация (before suite)", alwaysRun = true)
    public static void suitInit() {
        // Для вобора конфига нужного стенда, против которого нужно гонять тесты
        String testEnv = System.getProperty("testenv") == null ? "test" : System.getProperty("testenv").toLowerCase();
        ConfigFactory.setProperty("env", testEnv);
        testsConfig = ConfigFactory.create(TestsConfig.class);
    }

    @BeforeClass(description = "Инициализация", alwaysRun = true)
    public void testInit() {
        if (!SelenideLogger.hasListener("CustomListener"))
            SelenideLogger.addListener("CustomListener", new CustomListener().screenshots(true).savePageSource(false));

        Configuration.browser = testsConfig.driverType().equals("remote") ?
                RemoteWebDriverProvider.class.getCanonicalName() :
                LocalWebDriverProvider.class.getCanonicalName();
        Configuration.baseUrl = testsConfig.baseUrl();

        Selenide.open("404");
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    @AfterClass(description = "Удаление созданных компаний и другое", alwaysRun = true)
    public void cleanUp() {
        synchronized (uiVersion) {
            if (uiVersion.equals("")) {
                uiVersion = baseSteps.getUiVersion();
            }
        }

        for (String clientId : temporaryCompaniesIds) {
            dbSteps.deleteCompany(clientId);
        }
    }

    /**
     * Создает компанию, которая будет удалена в конце выполнения тествого класса.
     * Если компания с таким именем (short name/ФИО ИП) уже есть, старая компания будет предварительно удалена.
     */
    protected void recreateTempCompany(Company company) {
        baseSteps.recreateTempCompany(company, temporaryCompaniesIds);
    }

    /**
     * Добавляет ID компании, в список компаний, которые будут удалены в конце тестового класса
     */
    protected void markCompanyForDeletion(Company company) {
        String guid = apiClientsSteps.getNotArchivedCompanyGuid(company.getNameForSearch());
        if (!temporaryCompaniesIds.contains(guid)) {
            temporaryCompaniesIds.add(guid);
        }
    }

}

