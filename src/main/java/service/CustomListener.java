package service;

import com.codeborne.selenide.logevents.LogEvent;
import com.codeborne.selenide.logevents.SelenideLog;
import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.model.StepResult;
import io.qameta.allure.selenide.AllureSelenide;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains regex patterns for different selenide events.
 */

/**
 * Класс для замены строк в allure репортах
 */
//class Replacer {
//    public final Pattern srcPattern;
//    public final String resultPattern;
//    private Matcher matcher = null;
//
//    /**
//     * Конструктор
//     * @param srcPattern        regexp для анализа входных строк
//     * @param resultPattern     шаблон выходной строки.
//     *                          Может включать любой текст и шаблон вида ${x},
//     *                              где x - номер найденной (группы) в regexp
//     */
//    public Replacer(String srcPattern, String resultPattern) {
//        this.srcPattern = Pattern.compile(srcPattern);
//        this.resultPattern = resultPattern;
//    }
//
//
//    private String replaceToMatcherGroups(String input) {
//
//
//    }
//
//    /**
//     * Преобразует строку согласно паттернам. Возвращает null если фейл
//     * @param input
//     */
//    public String replace(String input) {
//        StringBuffer result = new StringBuffer("");
//        if((matcher = srcPattern.matcher(input)).find()) {
//            int prevPatEnd = 0;
//            int newPatStart = 0;
//            Matcher resPatMatcher = Pattern.compile("\\$\\{\\d\\}").matcher(input); // ${x}
//            while (resPatMatcher.find()) {
//                newPatStart = resPatMatcher.start();
//                result.append(input.substring(prevPatEnd, newPatStart));
//                int groupIndex = input.charAt(newPatStart + 3) - '0';
//                result.append(resPatMatcher.group())
//            }
//
//        }
//
//    }
//}

class Patterns {
    static public final Pattern reOpen = Pattern.compile("^\\$\\(\\\"open\\\"\\) (.*)");
    static public final Pattern reAssertThat = Pattern.compile("^\\$\\(\\\"(assertThat|assert)\\\"\\) (.*)");
    static private final String reLocator = "^\\$\\(\\\"(.*)\\\"\\) ";
    static public final Pattern reShouldHave = Pattern.compile(reLocator + "should have\\((.*)\\)");
    static public final Pattern reShouldBe = Pattern.compile(reLocator + "should be\\((.*)\\)");
    static public final Pattern reShould = Pattern.compile(reLocator + "should\\((.*)\\)");
    static public final Pattern reShouldNotHave = Pattern.compile(reLocator + "should not have\\((.*)\\)");
    static public final Pattern reShouldNotBe = Pattern.compile(reLocator + "should not be\\((.*)\\)");
    static public final Pattern reShouldNot = Pattern.compile(reLocator + "should not\\((.*)\\)");
    static public final Pattern reClick = Pattern.compile(reLocator + "click\\(\\)");
    static public final Pattern reSetValue = Pattern.compile(reLocator + "set value\\((.*)\\)");
    static public final Pattern reSendKeys = Pattern.compile(reLocator + "send keys\\((.*)\\)");
    static public final Pattern reScrollIntoView = Pattern.compile(reLocator + "scroll into view\\((.*)\\)");
    static public final Pattern reInfo = Pattern.compile("^\\$\\(\\\"\\[info\\] (.*)\\\"\\) (.*)");

}

/**
 * Changing Selenide steps descriptions in Allure report
 */
public class CustomListener extends AllureSelenide {

    private final AllureLifecycle lifecycle;
    private final boolean includeSelenideLocatorsSteps = true;

    public CustomListener() {
        this(Allure.getLifecycle());
    }

    public CustomListener(final AllureLifecycle lifecycle) {
        this.lifecycle = lifecycle;
    }

    private boolean stepsShouldBeLogged(final LogEvent event) {
        //  other customer Loggers could be configured, they should be logged
        return includeSelenideLocatorsSteps || !(event instanceof SelenideLog);
    }

    /**
     * Returns description for selenide step event
     */
    private String selenideLocatorEventToNiceString(LogEvent event) {
        String name = event.toString();
        Matcher matcher;
        // TODO: Переписать норм., без if-ов, добавить дополнительные замены
        // Там выше есть некоторые наброски на эту тему
        if ((matcher = Patterns.reShouldHave.matcher(name)).find()) {
            return "Элемент \"" + matcher.group(1) + "\" должен иметь " + matcher.group(2);
        } else if ((matcher = Patterns.reShouldBe.matcher(name)).find()) {
            return "Элемент \"" + matcher.group(1) + "\" должен быть " + matcher.group(2);
        } else if ((matcher = Patterns.reShould.matcher(name)).find()) {
            return "Элемент \"" + matcher.group(1) + "\" должен " + matcher.group(2);
        } else if ((matcher = Patterns.reShouldNotHave.matcher(name)).find()) {
            return "Элемент \"" + matcher.group(1) + "\" не должен иметь " + matcher.group(2);
        } else if ((matcher = Patterns.reShouldNotBe.matcher(name)).find()) {
            return "Элемент \"" + matcher.group(1) + "\" не должен быть " + matcher.group(2);
        } else if ((matcher = Patterns.reShouldNot.matcher(name)).find()) {
            return "Элемент \"" + matcher.group(1) + "\" не должен " + matcher.group(2);
        } else if ((matcher = Patterns.reOpen.matcher(name)).find()) {
            return "Открыть страницу " + matcher.group(1);
        } else if ((matcher = Patterns.reClick.matcher(name)).find()) {
            return "Клик по элементу \"" + matcher.group(1) + "\"";
        } else if ((matcher = Patterns.reSetValue.matcher(name)).find()) {
            return "Установить значение \"" + matcher.group(2) + "\" для элемента \"" + matcher.group(1) + "\"";
        } else if ((matcher = Patterns.reSendKeys.matcher(name)).find()) {
            return "Отправить нажатия \"" + matcher.group(2) + "\" элементу \"" + matcher.group(1) + "\"";
        } else if ((matcher = Patterns.reScrollIntoView.matcher(name)).find()) {
            return "Прокрутить до элемента \"" + matcher.group(1) + "\". Параметры: " + matcher.group(2);
        } else if ((matcher = Patterns.reAssertThat.matcher(name)).find()) {
            return "Проверка: " + matcher.group(2);
        } else if ((matcher = Patterns.reInfo.matcher(name)).find()) {
            return "(info) " + matcher.group(1) + ": " + matcher.group(2);
        } else {
            return name;
        }

    }

    @Override
    public void beforeEvent(final LogEvent event) {
        if (stepsShouldBeLogged(event)) {
            lifecycle.getCurrentTestCaseOrStep().ifPresent(parentUuid -> {
                final String uuid = UUID.randomUUID().toString();
                lifecycle.startStep(parentUuid, uuid, new StepResult().setName(selenideLocatorEventToNiceString(event)));
            });
        }
    }
}
