package service;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import testData.enums.PhoneFormat;
import testData.models.Phone;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static utils.StringFunctions.removeNonDigits;

public class Matchers {

    public static Matcher<String> isThisPhone(final Phone expected) {
        return new FeatureMatcher<String, String>(
                equalTo(expected.getNumber(PhoneFormat.FULL_WITH_PLUS)),
                "ожидаемый номер телефона:",
                "фактический номер телефона:"
        ) {
            @Override
            protected String featureValueOf(String phone) {
                return new Phone(phone).getNumber(PhoneFormat.FULL_WITH_PLUS);
            }
        };
    }

    public static Matcher<String> isPhoneMask10Digits() {
        return new FeatureMatcher<String, String>(
                equalTo("+7 ___ ___ ____"),
                "ожидаемая маска",
                "фактическая маска:"
        ) {
            @Override
            protected String featureValueOf(String phone) {
                return (phone);
            }
        };
    }

    public static Matcher<String> isPhoneMask11Digits() {
        return new FeatureMatcher<String, String>(
                equalTo("+_ ___ ___ ____"),
                "ожидаемая маска",
                "фактическая маска:"
        ) {
            @Override
            protected String featureValueOf(String phone) {
                return (phone);
            }
        };
    }

    public static Matcher<String> isPhoneMask() {
        return new FeatureMatcher<String, String>(
                anyOf(isPhoneMask10Digits(), isPhoneMask11Digits()),
                "ожидаемая маска",
                "фактическая маска:"
        ) {
            @Override
            protected String featureValueOf(String phone) {
                return phone;
            }
        };
    }

    public static Matcher<String> isEmptyPhoneValue() {
        return new FeatureMatcher<String, String>(
                isPhoneMask(),
                "значение должно соответствовать",
                "значение:"
        ) {
            @Override
            protected String featureValueOf(String phone) {
                return phone;
            }
        };
    }

    /**
     * Матчер для карточки компании для проверки отображения незаполненых полей
     */
    public static Matcher<String> notSpecified() {
        return new FeatureMatcher<String, String>(
                equalTo("Не указан"),
                "значение должно соответствовать",
                "значение:"
        ) {
            @Override
            protected String featureValueOf(String actual) { return actual; }
        };
    }

    public static Matcher<String> numericallySameInt(final String expected) {
        return new FeatureMatcher<String, String>(
                equalTo(removeNonDigits(expected)),
                "значение должно соответствовать",
                "значение:"
        ) {
            @Override
            protected String featureValueOf(String str) {
                return (removeNonDigits(expected));
            }
        };
    }
}
