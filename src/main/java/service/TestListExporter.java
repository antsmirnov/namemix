package service;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.*;

public class TestListExporter {

    @SneakyThrows
    private static Object getAnnotationField(Annotation annotation, String fieldName) {
        Method[] methods = annotation.annotationType().getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().equals(fieldName)) {
                return method.invoke(annotation, null);
            }
        }
        return null;
    }

    private static Object getAnnotationValue(Annotation annotation) {
        return getAnnotationField(annotation, "value");
    }

    private static String[] getFeatures(Class c) {
        Set<String> featureSet = new HashSet<>();
        Annotation[] annotations = c.getAnnotationsByType(Feature.class);
        for (Annotation feature : annotations) {
            Object value = getAnnotationValue(feature);
            if (value != null) {
                featureSet.add((String) value);
            }
        }

        annotations = c.getAnnotationsByType(Features.class);
        for (Annotation features : annotations) {
            Feature[] childrenFeatures = (Feature[]) getAnnotationValue(features);
            if (childrenFeatures != null) {
                for (Feature feature : childrenFeatures) {
                    Object value = getAnnotationValue(feature);
                    if (value != null) {
                        featureSet.add((String) value);
                    }
                }
            }
        }
        return featureSet.toArray(new String[featureSet.size()]);
    }

    private static String[] getFeatures(Method method) {
        Set<String> featureSet = new HashSet<>();
        Annotation[] annotations = method.getAnnotationsByType(Feature.class);
        for (Annotation feature : annotations) {
            Object value = getAnnotationValue(feature);
            if (value != null) {
                featureSet.add((String) value);
            }
        }

        annotations = method.getAnnotationsByType(Features.class);
        for (Annotation features : annotations) {
            Feature[] childrenFeatures = (Feature[]) getAnnotationValue(features);
            if (childrenFeatures != null) {
                for (Feature feature : childrenFeatures) {
                    Object value = getAnnotationValue(feature);
                    if (value != null) {
                        featureSet.add((String) value);
                    }
                }
            }
        }
        return featureSet.toArray(new String[featureSet.size()]);
    }

    /**
     * Возвращает true, если класс содержит в себе один JIRA-тесткейс (а не набор тесткейсов)
     * (если у _класса_ есть аннотация @Feature[s], у которой строковое значение начинается с prefix
     */
    private static boolean isWholeTestcaseClass(Class c, String prefix) {
        String[] features = getFeatures(c);
        for (String featureText : features) {
            if (featureText.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isWholeTestcaseMethod(Method method, String prefix) {
        String[] features = getFeatures(method);
        for (String featureText : features) {
            if (featureText.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Возвращает текст @Feature аннотации, если есть аннотация начинающаяся с prefix
     */
    private static String getFeatureTextForWholeTestcaseClass(Class c, String prefix) {
        String[] features = getFeatures(c);
        for (String featureText : features) {
            if (featureText.startsWith(prefix)) {
                return featureText;
            }
        }
        return null;
    }

    private static String getFeatureTextForWholeTestcaseMethod(Method method, String prefix) {
        String[] features = getFeatures(method);
        for (String featureText : features) {
            if (featureText.startsWith(prefix)) {
                return featureText;
            }
        }
        return null;
    }

    @SneakyThrows
    public static void main(String[] args) {
        URL testClassesUrl = Paths.get("target/test-classes").toUri().toURL();
        URLClassLoader classLoader = URLClassLoader.newInstance(new URL[]{testClassesUrl}, ClasspathHelper.staticClassLoader());

        Reflections reflections = new Reflections(
                new ConfigurationBuilder()
                        .addUrls(ClasspathHelper.forPackage("tests", classLoader))
                        .addClassLoader(classLoader)
                        .setScanners(
                                new SubTypesScanner(false),
                                new TypeAnnotationsScanner(),
                                new MethodAnnotationsScanner())
                        .filterInputsBy(new FilterBuilder().includePackage("test"))
        );

        Set<Class<?>> testClasses = reflections.getTypesAnnotatedWith(Features.class);
        testClasses.addAll(reflections.getTypesAnnotatedWith(Feature.class));
        List<String[]> tests = new ArrayList<>();
        for (Class<?> clazz : testClasses) {
            if (isWholeTestcaseClass(clazz, "NAMEMIX-")) {
                tests.add(new String[]{getFeatureTextForWholeTestcaseClass(clazz, "NAMEMIX-"), clazz.getSimpleName(), ""});
            }
        }

        Set<Method> testMethods = reflections.getMethodsAnnotatedWith(Features.class);
        testMethods.addAll(reflections.getMethodsAnnotatedWith(Feature.class));
        for (Method method : testMethods) {
            if (isWholeTestcaseMethod(method, "NAMEMIX-")) {
                tests.add(new String[]{getFeatureTextForWholeTestcaseMethod(method, "NAMEMIX-"), method.getDeclaringClass().getSimpleName(), method.getName()});
            }
        }

        tests.sort(Comparator.comparing(t -> t[0]));

        String lastAddedFeature = "";

        JSONArray jsonTests = new JSONArray();
        JSONObject feature;
        JSONArray featureParts = new JSONArray();
        JSONObject part;
        for (int i = 0; i < tests.size(); i++) {
            part = new JSONObject();
            part.put("class", tests.get(i)[1]);
            if (tests.get(i)[2].length() > 0) {
                part.put("method", tests.get(i)[2]);
            }

            if (lastAddedFeature.equals(tests.get(i)[0]) && i > 0) {
                featureParts.put(part);
            } else {
                featureParts = new JSONArray();
                featureParts.put(part);
                feature = new JSONObject();
                feature.put("name", tests.get(i)[0]);
                feature.put("parts", featureParts);
                jsonTests.put(feature);
                lastAddedFeature = tests.get(i)[0];
            }
        }

        String path = "./src/test/resources/";
        OutputStreamWriter file = new OutputStreamWriter(
                new FileOutputStream(path + "tests.json"),
                StandardCharsets.UTF_8);
        file.write(new JSONObject().put("features", jsonTests).toString(2));
        file.close();
    }
}
