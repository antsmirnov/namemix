package service;

import org.aeonbits.owner.Config;

import static org.aeonbits.owner.Config.*;

@LoadPolicy(LoadType.MERGE)
@Sources({"file:src/main/resources/TestsConfig.${env}.properties",
        "system:properties",
        "system:env"})
public interface TestsConfig extends Config {
    @Key("baseUrl")
    String baseUrl();

    @Key("nmApiUrl")
    String nmApiUrl();

    @Key("apiUrl")
    String apiUrl();

    @Key("users.webAdminSite.login")
    String webAdminSiteLogin();

    @Key("users.webAdminSite.password")
    String webAdminSitePassword();

    @Key("users.webAdminCompany.login")
    String webAdminCompanyLogin();

    @Key("users.webAdminCompany.password")
    String webAdminCompanyPassword();

    String dbConnectionString();

    @Key("users.db.login")
    String dbLogin();

    @Key("users.db.password")
    String dbPassword();

    @Key("dbSchema")
    String dbSchema();

    @Key("driverType")
    @DefaultValue("local")
    String driverType();

    @Key("selenoidUri")
    String selenoidUri();

    @Key("browser")
    @DefaultValue("chrome")
    String browser();

    @Key("users.universalTestPassword")
    String universalTestPassword();

    @DefaultValue("1")
    @Key("threadCount")
    Integer threadCount();

}
