package service.webDrivers;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverProvider;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import service.TestsConfig;

import javax.annotation.Nonnull;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.remote.CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION;
import static org.openqa.selenium.remote.CapabilityType.PAGE_LOAD_STRATEGY;

public class RemoteWebDriverProvider implements WebDriverProvider {

    private static TestsConfig testsConfig = ConfigFactory.create(TestsConfig.class);

    @Nonnull
    @Override
    public WebDriver createDriver(DesiredCapabilities desiredCapabilities) {
        RemoteWebDriver driver;
        Configuration.driverManagerEnabled = false;
        Configuration.remote = testsConfig.selenoidUri();
        Configuration.browser = testsConfig.browser();
        Configuration.browserSize = "1920x1080";

        desiredCapabilities.setBrowserName(testsConfig.browser());
        desiredCapabilities.setCapability("enableVNC", true);

        if ("chrome".equals(testsConfig.browser())) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("disable-logging");
            options.addArguments("disable-infobars");
            options.addArguments("disable-translate");
            options.addArguments("disable-plugins");
            options.addArguments("disable-extensions");
            options.addArguments("disable-web-security");
            options.addArguments("no-default-browser-check");
            options.addArguments("no-sandbox");
            options.addArguments("no-first-run");
            options.addArguments("silent");
            options.addArguments("no-first-run");
            options.addArguments("window-size=1920,1080");
            options.addArguments("lang=ru");
            options.addArguments("disable-gpu");
            options.addArguments("disable-notifications");
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, "ignore");
            options.setCapability(ENSURING_CLEAN_SESSION, true);
            options.setCapability(PAGE_LOAD_STRATEGY, "normal");
            desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        } else {
            System.out.println("Используется неизвестный браузер \"" + testsConfig.browser() +
                    "\". Добавьте параметры браузера, в " + RemoteWebDriverProvider.class.getName() + ", если необходимо");
            assert (false);
        }

        try {
            driver = new RemoteWebDriver(new URL(testsConfig.selenoidUri()), desiredCapabilities);
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
            driver.manage().timeouts().setScriptTimeout(120, TimeUnit.SECONDS);
            driver.setFileDetector(new LocalFileDetector());
            return driver;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            assert false;
            return null;
        }
    }

}
