package steps;

import apis.api.responseModel.AuthResponseModel;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pageObject.pages.AnyPage;
import pageObject.pages.LoginPage;
import steps.apis.api.ApiAuthSteps;
import steps.apis.api.ApiClientsSteps;
import steps.db.DbSteps;
import testData.enums.UserRole;
import testData.models.Company;
import testData.models.User;

import java.time.Duration;
import java.util.ArrayList;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.hamcrest.CoreMatchers.nullValue;
import static service.CustomAssert.assertThat;
import static utils.Common.xPathPatternToBy;

public class BaseSteps {

    static ApiClientsSteps apiClientsSteps = new ApiClientsSteps();
    static DbSteps dbSteps = new DbSteps();
    static ApiAuthSteps apiAuthSteps = new ApiAuthSteps();

    @Step("Войти в систему")
    public void loginAs(UserRole userRole, boolean usingApi) {
        User user = new User(userRole);
        if (!usingApi) {
            open("login");
            $(LoginPage.onLoginFrame.inputEmail).setValue(user.login);
            $(LoginPage.onLoginFrame.inputPassword).setValue(user.password);
            $(LoginPage.onLoginFrame.btnLogin).click();
            $(AnyPage.onAnyPage.btnLogout).should(exist, Duration.ofSeconds(15));
        } else {
            open("404");
            AuthResponseModel auth = apiAuthSteps.getAuthForUser(user);
            Selenide.localStorage().setItem("login", user.login);
            Selenide.localStorage().setItem("role", auth.role);
            Selenide.localStorage().setItem("accessToken", auth.accessToken);
            Selenide.localStorage().setItem("refreshToken", auth.refreshToken);
            Selenide.localStorage().setItem("currentClientId", auth.clientId);
            Selenide.localStorage().setItem("currentClientUserId", auth.clientUserId);
        }
    }

    @Step("Войти в систему")
    public void loginAs(UserRole userRole) {
        loginAs(userRole, true);
    }

    public String getUiVersion() {
        return $(AnyPage.onAnyPage.textVersion).exists() ?
                $(AnyPage.onAnyPage.textVersion).getText().replace("Версия UI ", "") :
                "";
    }

    @Step("Проверить существование успешного уведомления '{notifText}' (ждать {timeout} мс)")
    public void checkSuccessNotification(String notifText, int timeout, boolean hideTheNotification) {
        By notification = xPathPatternToBy(AnyPage.onAnyPage.notificationSuccessPattern, notifText);
        $(notification).shouldBe(visible, Duration.ofMillis(timeout));
        if (hideTheNotification) {
            $(notification).click();
        }
    }

    /** Проверяет существование успешного уведомления и скрывает его (чтобы оно не вводило дальнейшие проверки в заблуждение) */
    @Step("Проверить существование успешного уведомления '{notifText}'")
    public void checkSuccessNotification(String notifText) {
        checkSuccessNotification(notifText, 15000, true);
    }

    @Step("Проверить существование warning уведомления '{notifText}' (ждать {timeout} мс)")
    public void checkWarningNotification(String notifText, int timeout, boolean hideTheNotification) {
        By notification = xPathPatternToBy(AnyPage.onAnyPage.notificationWarningPattern, notifText);
        $(notification).shouldBe(visible, Duration.ofMillis(timeout));
        if (hideTheNotification) {
            $(notification).click();
        }
    }

    /** Проверяет существование warning уведомления и скрывает его (чтобы оно не вводило дальнейшие проверки в заблуждение) */
    @Step("Проверить существование warning уведомления '{notifText}'")
    public void checkWarningNotification(String notifText) {
        checkWarningNotification(notifText, 15000, true);
    }

    @Step("Проверить существование warning уведомления '{notifText}' (ждать {timeout} мс)")
    public void checkErrorNotification(String notifText, int timeout, boolean hideTheNotification) {
        By notification = xPathPatternToBy(AnyPage.onAnyPage.notificationErrorPattern, notifText);
        $(notification).shouldBe(visible, Duration.ofMillis(timeout));
        if (hideTheNotification) {
            $(notification).click();
        }
    }

    /** Проверяет существование warning уведомления и скрывает его (чтобы оно не вводило дальнейшие проверки в заблуждение) */
    @Step("Проверить существование warning уведомления '{notifText}'")
    public void checkErrorNotification(String notifText) {
        checkErrorNotification(notifText, 15000, true);
    }

    /**
     * Функция для создания компании (или пересоздания, если компании еще не существует)
     *
     * @param company         компания для создания, по ее имени будет производиться поиск существующий компании
     * @param listOfClientIds лист, в который будет добавлена созданная компания. Компании из
     *                        этого списка будут удалены после выполнения всех тестов тестового класса!!
     */
    @Step("Создание компании, при необходимости - пересоздание (удаление и повторное создание)")
    public void recreateTempCompany(Company company, ArrayList<String> listOfClientIds) {
        String searchBy = company.getNameForSearch();
        String guid = apiClientsSteps.getCompanyGuid(searchBy, false, false, true);
        if (guid != null) {
            dbSteps.deleteCompany(guid);
            assertThat("Компания должна быть успешно удалена (и не должно быть других компаний в поиске по этому имени)",
                    apiClientsSteps.getCompanyGuid(searchBy, false, false, true), nullValue());
        }
        guid = apiClientsSteps.createCompany(company);
        apiClientsSteps.checkCompanyExists(searchBy); // TODO: заменить на проверку методом, получающим компанию по guid
        if (guid != null) {
            listOfClientIds.add(guid);
            company.setClientId(guid);
        }
    }

}
