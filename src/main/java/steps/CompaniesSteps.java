package steps;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.openqa.selenium.StaleElementReferenceException;
import pageObject.pages.AnyPage;
import pageObject.pages.CompaniesPage;
import steps.apis.api.ApiClientsSteps;
import testData.models.Company;

import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static service.CustomAssert.assertThat;
import static testData.enums.CompanyForm.INDIVIDUAL;

public class CompaniesSteps {

    static ApiClientsSteps apiClientsSteps = new ApiClientsSteps();
    static BaseSteps baseSteps = new BaseSteps();

    @Step("Открытие страницы 'Компании'")
    public void openCompaniesPage() {
        open("client");
    }

    @Step("Поиск компании по имени")
    public void searchCompaniesByName(String name) {
        openCompaniesPage();
        $(CompaniesPage.onCompaniesFrame.inputCustomer).setValue(name);
        $(CompaniesPage.onCompaniesFrame.btnFind).click();
        int timeout = 450;
        while (timeout > 0) { // костыль... но должно работать
            timeout -= 50;
            sleep(50);
            try {
                if ($$(CompaniesPage.onCompaniesFrame.tableCompanyNames).filterBy(Condition.text(name)).size() > 0) {
                    return;
                }
            } catch (StaleElementReferenceException e) {
                // в этом месте это частая ситуация из-за быстрого удаления элементов при поиске компаний
                e.printStackTrace();
            }
        }
    }

    @Step("Поиск компании по имени")
    public void searchCompanies(Company company) {
        openCompaniesPage();
        $(CompaniesPage.onCompaniesFrame.inputCustomer).setValue(
                company.getCompanyForm() == INDIVIDUAL ? company.getFioIp() : company.getShortName());
        $(CompaniesPage.onCompaniesFrame.btnFind).click();
        sleep(200);
    }

    @Step("Проверка отсутствия активной (не архивированной) компании по имени")
    public void checkCompanyDoesNotExist(String name, boolean usingApi) {
        if (usingApi) {
            assertThat("Компания с таким именем найдена в списке активных (неархивированных) компаний",
                    apiClientsSteps.searchCompaniesActive(name).length, equalTo(0));
        } else {
            searchCompaniesByName(name);
            assertThat("Компания не была создана",
                    $$(CompaniesPage.onCompaniesFrame.tableCompanyNames).filterBy(Condition.text(name)).size(),
                    equalTo(0));
        }
    }

    @Step("Проверка отсутствия активной (не архивированной) компании по имени")
    public void checkCompanyDoesNotExist(String name) {
        checkCompanyDoesNotExist(name, true);
    }

    @Step("Проверка отсутствия активной (не архивированной) компании по имени")
    public void companyDoesNotExistUICheck(String name) {
        checkCompanyDoesNotExist(name, false);
    }

    @Step("Проверка наличия успешных уведомлений о создании")
    public void checkNotificationsAboutFirmCreation() {
        baseSteps.checkSuccessNotification("Компания успешно добавлена");
        baseSteps.checkSuccessNotification("Компания включена");
    }

    @Step("Проверка существования компании")
    public void checkCompanyExists(String name, boolean usingApi) {
        if (usingApi) {
            apiClientsSteps.checkCompanyExists(name);
        } else {
            searchCompaniesByName(name);
            sleep(200);
            assertThat("Компания существует в списке компаний",
                    $$(CompaniesPage.onCompaniesFrame.tableCompanyNames).filterBy(Condition.text(name)).size(),
                    greaterThan(0));
        }
    }

    @Step("Проверка существования компании через UI")
    public void checkCompanyExistsUI(String name) {
        checkCompanyExists(name, false);
    }

    @Step("Архивировать компанию {company}")
    public void archiveCompany(Company company) {
        searchCompanies(company);
        if ($$(CompaniesPage.onCompaniesFrame.tableCompanyNames).filterBy(Condition.text(company.getShortName())).size() > 0) {
            $$(CompaniesPage.onCompaniesFrame.tableCompanyBtnArchive).get(0).click();
            $(AnyPage.onYesNoQuestionPopup.btnYes).click();
        }
    }

}
