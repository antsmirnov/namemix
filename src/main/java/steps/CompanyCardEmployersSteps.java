package steps;

import apis.api.responseModel.subResponses.SubResponseClientUser;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import pageObject.pages.AnyPage;
import pageObject.pages.CompanyCardPage;
import pageObject.pages.CompanyEmployersPage;
import steps.apis.api.ApiClientsSteps;
import testData.enums.EmployerPosition;
import testData.enums.EmployerRole;
import testData.models.Company;
import testData.models.Employer;

import static com.codeborne.selenide.Condition.exactValue;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static service.CustomAssert.assertThat;
import static service.Matchers.isThisPhone;
import static service.Matchers.numericallySameInt;
import static steps.elements.InputPhone.inputWithCheckPhone;
import static utils.InputMethods.enterValueIntoMaskedInput;
import static utils.InputMethods.jsClick;

public class CompanyCardEmployersSteps {

    static BaseSteps baseSteps = new BaseSteps();
    static CompanyCardInfoSteps companyCardInfoSteps = new CompanyCardInfoSteps();
    static ApiClientsSteps apiClientsSteps = new ApiClientsSteps();

    @Step("Ввести снилс с проверкой")
    public void inputWithCheckSnils(String snils) {
        enterValueIntoMaskedInput($(CompanyEmployersPage.onCompanyEmployerEditPopup.inputSnils), snils);
        assertThat("Значение действительно было введено в поле",
                $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputSnils).getValue().replaceAll("-", ""),
                equalTo(snils));
    }

    @Step("Ввести ИНН с проверкой")
    public void inputWithCheckInn(String inn) {
        enterValueIntoMaskedInput($(CompanyEmployersPage.onCompanyEmployerEditPopup.inputInn), inn);
        assertThat("Значение действительно было введено в поле",
                $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputInn).getValue().replaceAll(" ", ""),
                equalTo(inn));
    }

    @Step("Ввести должность")
    public void inputPosition(EmployerPosition position) {
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.dropDownBtnPosition).click();
        $$(CompanyEmployersPage.onCompanyEmployerEditPopup.dropDownOptionsPosition)
                .filterBy(Condition.text(position.getValue())).get(0).click();
    }

    @Step("Ввести роль")
    public void inputRole(EmployerRole role) {
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.dropDownBtnRole).click();
        $$(CompanyEmployersPage.onCompanyEmployerEditPopup.dropDownOptionsRole)
                .filterBy(Condition.text(role.getValue())).get(0).click();
    }

    @Step("Открыть страницу Сотрудники компании {company}")
    public void openEmployersPage(Company company) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        $(CompanyCardPage.onCompanyCardFrame.tabEmployers).click();
    }

    @Step("Ввести валидные данные сотрудника")
    public void inputEmployerData(Employer employer) {
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputLastName).setValue(employer.getSecondName());
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputFirstName).setValue(employer.getFirstName());
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputPatronymic).setValue(employer.getPatronymic());
        inputWithCheckSnils(employer.getSnils());
        inputWithCheckInn(employer.getInn());
        inputPosition(employer.getPosition());
        inputWithCheckPhone($(CompanyEmployersPage.onCompanyEmployerEditPopup.inputPhone), employer.getPhone());
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputEmail).setValue(employer.getEmail());
        inputRole(employer.getRole());
        if ($(CompanyEmployersPage.onCompanyEmployerEditPopup.btnChangePassword).isDisplayed()) {
            // в случае редактирования сотрудника
            $(CompanyEmployersPage.onCompanyEmployerEditPopup.btnChangePassword).click();
        }
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputPassword).setValue(employer.getPassword());
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputRepeatPassword).setValue(employer.getPassword());
    }

    @Step("Щелкнуть кнопку Добавить/Сохранить сотрудника")
    public void clickAddOrSave() {
        jsClick(CompanyEmployersPage.onCompanyEmployerEditPopup.btnAdd);
    }

    @Step("Добавить сотрудника для компании")
    public void addEmployer(Company company, Employer employer) {
        openEmployersPage(company);
        $(CompanyEmployersPage.onCompanyEmployersFrame.btnAddEmployer).click();
        inputEmployerData(employer);
        clickAddOrSave();
        baseSteps.checkSuccessNotification("Сотрудник успешно добавлен");
    }

    @Step("Проверить что сотрудник {employer} существует")
    public void checkEmployerExist(Company company, Employer employer) {
        openEmployersPage(company);
        sleep(300);
        $$(CompanyEmployersPage.onCompanyEmployersFrame.tableEmployerNames)
                .filterBy(Condition.text(employer.getFio()))
                .shouldHave(CollectionCondition.sizeGreaterThan(0));
    }

    @Step("Открыть окно редактирования сотрудника {employer}")
    public void openEditPopupForEmployer(Company company, Employer employer) {
        openEmployersPage(company);
        $$(CompanyEmployersPage.onCompanyEmployersFrame.tableEmployerRows)
                .filterBy(Condition.text(employer.getFio()))
                .get(0)
                .$(CompanyEmployersPage.onCompanyEmployersFrame.tableEmployerBtnEdit)
                .click();
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputFirstName).shouldBe(Condition.visible);
    }

    @Step("Отредактировать сотрудника компании")
    public void editEmployer(Company company, Employer employer, Employer editedEmployer) {
        openEditPopupForEmployer(company, employer);
        inputEmployerData(editedEmployer);
        clickAddOrSave();
        baseSteps.checkSuccessNotification("Данные сотрудника успешно отредактированы");
    }

    @Step("Проверить данные сотрудника {employer}")
    public void checkEmployerFields(Company company, Employer employer) {
        openEditPopupForEmployer(company, employer);
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputLastName).shouldHave(exactValue(employer.getSecondName()));
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputFirstName).shouldHave(exactValue(employer.getFirstName()));
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputPatronymic).shouldHave(exactValue(employer.getPatronymic()));

        assertThat("Снилс совпадает",
                $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputSnils).getValue(),
                numericallySameInt(employer.getSnils()));
        assertThat("ИНН совпадает",
                $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputInn).getValue(),
                numericallySameInt(employer.getInn()));

        $(CompanyEmployersPage.onCompanyEmployerEditPopup.dropDownSelectedPosition)
                .shouldHave(text(employer.getPosition().getValue()));

        assertThat("ИНН совпадает",
                $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputPhone).getValue(),
                isThisPhone(employer.getPhone()));

        $(CompanyEmployersPage.onCompanyEmployerEditPopup.inputEmail).shouldHave(exactValue(employer.getEmail()));
        $(CompanyEmployersPage.onCompanyEmployerEditPopup.dropDownSelectedRole).shouldHave(text(employer.getRole().getValue()));
    }

    @Step("Архивировать всех сотрудников компании")
    public void archiveAllEmployersOf(Company company) {
        openEmployersPage(company);
        final int limit = 50;
        int count = 0;
        while (++count < limit && $$(CompanyEmployersPage.onCompanyEmployersFrame.tableEmployerRows).size() > 0) {

            $$(CompanyEmployersPage.onCompanyEmployersFrame.tableEmployerRows)
                    .get(0)
                    .$(CompanyEmployersPage.onCompanyEmployersFrame.tableEmployerBtnArchive)
                    .click();
            $(AnyPage.onYesNoQuestionPopup.btnYes).click();
            sleep(300);
            baseSteps.checkSuccessNotification("Сотрудник успешно перемещен в архив");

        }
    }

    @Step("Проверить, что в компании {company} есть сотрудник с e-mail: {email}")
    public void checkEmployerWithThisEmailExistsForCompany(String company, String email) {
        SubResponseClientUser[] employers = apiClientsSteps.getCompanyEmployers(company);
        assertThat("В компании должен быть хотя-бы один сотрудник", employers.length, greaterThan(0));
        boolean found = false;
        for (SubResponseClientUser employer : employers) {
            if (employer.email.equals(email)) {
                found = true;
                break;
            }
        }
        assertThat("Сотрудник с заданным email должен существовать", found, equalTo(true));
    }

    @Step("Добавить сотрудника для компании, если сотрудник еще не создан")
    public void addEmployerIfDoesNotExist(Company company, Employer employer) {
        try {
            checkEmployerWithThisEmailExistsForCompany(company.getNameForSearch(), employer.getEmail());
        } catch (AssertionError e) {
            addEmployer(company, employer);
        }
    }

}
