package steps;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.testng.Assert;
import pageObject.pages.CompaniesPage;
import pageObject.pages.CompanyCardPage;
import steps.elements.InputDropDownList;
import testData.enums.CompanyForm;
import testData.models.BankRequisites;
import testData.models.Company;
import testData.models.Phone;
import utils.StringGenerator;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.fail;
import static pageObject.pages.CompanyCardInfoPage.*;
import static service.CustomAssert.assertThat;
import static service.Matchers.isThisPhone;
import static steps.elements.InputAddress.enterAddressWithCheck;
import static steps.elements.InputPhone.enterPhoneRaw;
import static steps.elements.InputPhone.inputWithCheckPhone;
import static utils.InputMethods.*;

public class CompanyCardInfoSteps {

    static BaseSteps baseSteps = new BaseSteps();
    static CompaniesSteps companiesSteps = new CompaniesSteps();

    /**
     * @param byUrl true - открыть по прямой ссылке (если возможно), false - открыть через UI
     */
    @Step("Открыть страницу нужной компании, если необходимо")
    public void openCompanyInfoPageIfNeeds(Company company, boolean byUrl) {
        String searchBy = company.getCompanyForm() == CompanyForm.INDIVIDUAL ? company.getFioIp() : company.getShortName();
        if (
                !($(CompanyCardPage.onCompanyCardFrame.textHeader).exists()) ||
                        !($(CompanyCardPage.onCompanyCardFrame.textHeader).getText().contains(searchBy)) ||
                        !(url().contains("/info"))
        ) {
            if (!byUrl || company.getClientId() == null) {
                companiesSteps.searchCompaniesByName(searchBy);
                $$(CompaniesPage.onCompaniesFrame.tableCompanyNames).get(0).click();
                $(CompanyCardPage.onCompanyCardFrame.textHeader).shouldHave(textCaseSensitive(searchBy));
            } else {
                open("client-card/" + company.getClientId() + "/info");
            }
        }
    }

    public void openCompanyInfoPageIfNeeds(Company company) {
        openCompanyInfoPageIfNeeds(company, true);
    }

    @Step("Отображение блока 'Реквизиты компании'")
    public void checkDisplayingCompanyRequisites(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onRequisitesBlock.headerCompanyRequisites).shouldHave(exactTextCaseSensitive("Реквизиты компании"));
        switch (company.getCompanyForm()) {
            case INDIVIDUAL:
                $(onRequisitesBlock.labelFioIp).shouldHave(exactTextCaseSensitive("ФИО ИП"));
                $(onRequisitesBlock.labelOgrnIp).shouldHave(exactTextCaseSensitive("ОГРНИП"));
                $(onRequisitesBlock.labelPhone).shouldHave(exactText("Номер телефона"));
                $(onRequisitesBlock.labelEmail).shouldHave(exactText("E-mail"));

                $(onRequisitesBlock.textFioIp).shouldBe(visible);
                $(onRequisitesBlock.textOgrnIp).shouldBe(visible);
                $(onRequisitesBlock.textPhone).shouldBe(visible);
                $(onRequisitesBlock.textEmail).shouldBe(visible);
                break;
            case LEGAL_ENTITY:
                $(onRequisitesBlock.labelOfficialName).shouldHave(exactTextCaseSensitive("Официальное название"));
                $(onRequisitesBlock.labelCompanyPhone).shouldHave(exactTextCaseSensitive("Номер телефона компании"));
                $(onRequisitesBlock.labelCompanyEmail).shouldHave(exactTextCaseSensitive("E-mail компании"));
                $(onRequisitesBlock.labelOgrn).shouldHave(exactTextCaseSensitive("ОГРН"));
                $(onRequisitesBlock.labelKpp).shouldHave(exactTextCaseSensitive("КПП"));
                $(onRequisitesBlock.labelInn).shouldHave(exactTextCaseSensitive("ИНН"));
                $(onRequisitesBlock.labelActualAddress).shouldHave(exactTextCaseSensitive("Фактический адрес"));

                $(onRequisitesBlock.textOfficialName).shouldBe(visible);
                $(onRequisitesBlock.labelCompanyPhone).shouldBe(visible);
                $(onRequisitesBlock.labelCompanyEmail).shouldBe(visible);
                $(onRequisitesBlock.textOgrn).shouldBe(visible);
                $(onRequisitesBlock.textKpp).shouldBe(visible);
                $(onRequisitesBlock.textInn).shouldBe(visible);
                break;
            case FOREIGN:
                $(onRequisitesBlock.labelOfficialName).shouldHave(exactTextCaseSensitive("Официальное название"));
                $(onRequisitesBlock.labelEin).shouldHave(exactTextCaseSensitive("Идентификационный номер работодателя (EIN)"));
                $(onRequisitesBlock.labelCompanyPhone).shouldHave(exactTextCaseSensitive("Номер телефона компании"));
                $(onRequisitesBlock.labelCompanyEmail).shouldHave(exactTextCaseSensitive("E-mail компании"));
                $(onRequisitesBlock.labelActualAddress).shouldHave(exactTextCaseSensitive("Фактический адрес"));

                $(onRequisitesBlock.textOfficialName).shouldBe(visible);
                $(onRequisitesBlock.textEin).should(visible);
                $(onRequisitesBlock.labelCompanyPhone).shouldBe(visible);
                $(onRequisitesBlock.labelCompanyEmail).shouldBe(visible);
                $(onRequisitesBlock.labelCompanyPhone).shouldBe(visible);
                $(onRequisitesBlock.labelCompanyEmail).shouldBe(visible);
                break;
        }

        $(onRequisitesBlock.labelRegistrationForm).shouldHave(exactTextCaseSensitive("Форма регистрации бизнеса"));
        $(onRequisitesBlock.labelFioContactPerson).shouldHave(exactTextCaseSensitive("ФИО контактного лица"));
        $(onRequisitesBlock.labelPhoneContactPerson).shouldHave(exactTextCaseSensitive("Номер телефона контактного лица"));
        $(onRequisitesBlock.labelEmailContactPerson).shouldHave(exactTextCaseSensitive("E-mail контактного лица"));
    }

    @Step("Отображение блока 'Банковские реквизиты'")
    public void checkDisplayingBankRequisites(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onBankRequisitesBlock.headerBankRequisites).shouldHave(exactTextCaseSensitive("Банковские реквизиты"));
        $(onBankRequisitesBlock.labelBank).shouldHave(exactTextCaseSensitive("Банк"));
        $(onBankRequisitesBlock.textBank).shouldBe(visible);
        $(onBankRequisitesBlock.textAddress).shouldBe(visible);

        switch (company.getCompanyForm()) {
            case FOREIGN:
                $(onBankRequisitesBlock.labelSwift).shouldHave(exactTextCaseSensitive("SWIFT-код"));
                $(onBankRequisitesBlock.labelAccountNumber).shouldHave(exactTextCaseSensitive("Номер счета (Account number)"));
                $(onBankRequisitesBlock.labelRoutingNumber).shouldHave(exactTextCaseSensitive("Банковский код (ACH Routing number)"));
                $(onBankRequisitesBlock.textSwift).shouldBe(visible);
                $(onBankRequisitesBlock.textAccountNumber).shouldBe(visible);
                $(onBankRequisitesBlock.textRoutingNumber).shouldBe(visible);
                break;
            case LEGAL_ENTITY:
            case INDIVIDUAL:
                $(onBankRequisitesBlock.labelBik).shouldHave(exactTextCaseSensitive("БИК"));
                $(onBankRequisitesBlock.labelRS).shouldHave(exactTextCaseSensitive("р/c"));
                $(onBankRequisitesBlock.labelKS).shouldHave(exactTextCaseSensitive("к/с"));
                $(onBankRequisitesBlock.textBik).shouldBe(visible);
                $(onBankRequisitesBlock.textRS).shouldBe(visible);
                $(onBankRequisitesBlock.textKS).shouldBe(visible);
                break;
        }
    }

    @Step("Отображение блока 'Главный бухгалтер'")
    public void checkDisplayingMainAccountant(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onMainAccountantBlock.headerMainAccountant).shouldHave(exactTextCaseSensitive("Главный бухгалтер"));
        $(onMainAccountantBlock.labelFio).shouldHave(exactTextCaseSensitive("ФИО главного бухгалтера"));
        $(onMainAccountantBlock.labelPhone).shouldHave(exactTextCaseSensitive("Телефон"));

        $(onMainAccountantBlock.textFio).shouldBe(visible);
        $(onMainAccountantBlock.textPhone).shouldBe(visible);
    }

    @Step("Отображение блока 'Руководитель'")
    public void checkDisplayingDirector(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onDirectorBlock.headerDirector).shouldHave(exactTextCaseSensitive("Руководитель"));
        $(onDirectorBlock.labelFio).shouldHave(exactTextCaseSensitive("ФИО руководителя"));
        $(onDirectorBlock.labelPhone).shouldHave(exactTextCaseSensitive("Номер телефона руководителя"));

        $(onDirectorBlock.textFio).shouldBe(visible);
        $(onDirectorBlock.textPhone).shouldBe(visible);
    }

    @Step("Отображение блока 'Представитель'")
    public void checkDisplayingPredstavitel(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onPredstavitelBlock.headerPredstavitel).shouldHave(exactTextCaseSensitive("Представитель"));
        $(onPredstavitelBlock.labelFio).shouldHave(exactTextCaseSensitive("В лице"));
        $(onPredstavitelBlock.labelBased).shouldHave(exactTextCaseSensitive("На основании"));

        $(onPredstavitelBlock.textFio).shouldBe(visible);
        $(onPredstavitelBlock.textBased).shouldBe(visible);
    }

    @Step("Отображение блока 'Подписант'")
    public void checkDisplayingPodpisant(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onPodpisantBlock.headerPodpisant).shouldHave(exactTextCaseSensitive("Подписант"));
        $(onPodpisantBlock.labelRole).shouldHave(exactTextCaseSensitive("Должность"));
        $(onPodpisantBlock.labelTranscript).shouldHave(exactTextCaseSensitive("Расшифровка"));

        $(onPodpisantBlock.textRole).shouldBe(visible);
        $(onPodpisantBlock.textTranscript).shouldBe(visible);
    }

    @Step("Отображение блока 'Лимиты'")
    public void checkDisplayingLimits(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onLimitsBlock.headerLimits).shouldHave(exactTextCaseSensitive("Лимиты"));
        $(onLimitsBlock.labelLimitOfOne).shouldHave(exactTextCaseSensitive("Лимит 1 транзакции, руб"));
        $(onLimitsBlock.labelLimitPerDay).shouldHave(exactTextCaseSensitive("Лимит в сутки на 1 исполнителя, руб"));
        $(onLimitsBlock.labelLimitPerMonth).shouldHave(exactTextCaseSensitive("Лимит в месяц на 1 исполнителя, руб"));

        $(onLimitsBlock.textLimitOfOne).shouldBe(visible);
        $(onLimitsBlock.textLimitPerDay).shouldBe(visible);
        $(onLimitsBlock.textLimitPerDay).shouldBe(visible);
        $(onLimitsBlock.textLimitPerMonth).shouldBe(visible);
    }

    @Step("Отображение блока 'Категория компании'")
    public void checkDisplayingCompanyCategory(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onCompanyCategoryBlock.headerCompanyCategory).shouldHave(exactTextCaseSensitive("Категория компании"));
        $(onCompanyCategoryBlock.labelOkved).shouldHave(exactTextCaseSensitive("ОКВЭД"));
        $(onCompanyCategoryBlock.labelCategory).shouldHave(exactTextCaseSensitive("Категория"));

        $(onCompanyCategoryBlock.textCategory).shouldBe(visible);
        $(onCompanyCategoryBlock.textOkved).shouldBe(visible);
    }

    @Step("Отображение блока 'Параметры компании'")
    public void checkDisplayingCompanyParameters(Company company) {
        openCompanyInfoPageIfNeeds(company);
        $(onCompanyParametersBlock.header).shouldHave(exactTextCaseSensitive("Параметры компании"));
        $(onCompanyParametersBlock.labelNonResidentsNotAllowed).shouldHave(exactTextCaseSensitive("Запрет на заключение договоров с нерезидентами"));
    }

    /**
     * Переключает блок в режим редактирования
     */
    public void startEditBlock(By blockContainer, boolean reopenIfOpened) {
        $(blockContainer).shouldBe(visible);
        if (!($(blockContainer).$(onAnyBlock.btnBlockEdit).exists())) {
            if (reopenIfOpened) {
                $(blockContainer).$(onAnyBlock.btnBlockCancel).click();
            } else {
                return;
            }
        }
        $(blockContainer).$(onAnyBlock.btnBlockEdit).click();
    }

    /* Реквизиты компании - редактирование */
    @Step("Открыть раздел Реквизиты компании для редактирования")
    public void startEditRequisitesBlock(boolean reopenIfOpened) {
        startEditBlock(onRequisitesBlock.container, reopenIfOpened);
    }

    public void startEditRequisitesBlock() {
        startEditRequisitesBlock(true);
    }

    @Step("Заполнить валидными данными поля блока 'Реквизиты компании'")
    public void inputRequisitesBlock(Company company) {
        switch (company.getCompanyForm()) {
            case LEGAL_ENTITY:
                enterWithCheck(onRequisitesBlock.inputOfficialName, company.getOfficialName());
                enterWithCheck(onRequisitesBlock.inputFioContactPerson, company.getContactorFio());
                inputWithCheckPhone(onRequisitesBlock.inputPhoneContactPerson, company.getContactorPhone());
                enterWithCheck(onRequisitesBlock.inputEmailContactPerson, company.getContactorEmail());
                enterAddressWithCheck(onRequisitesBlock.inputActualAddress, company.getAddressActual());
                enterAddressWithCheck(onRequisitesBlock.inputRegisteredAddress, company.getAddressRegistered());
                inputWithCheckPhone(onRequisitesBlock.inputCompanyPhone, company.getPhoneOfCompanyOrIp());
                enterWithCheck(onRequisitesBlock.inputCompanyEmail, company.getEmailOfCompanyOrIp());
                enterWithCheck(onRequisitesBlock.inputOgrn, company.getOgrn());
                enterWithCheck(onRequisitesBlock.inputKpp, company.getKpp());
                enterWithCheck(onRequisitesBlock.inputInn, company.getInn());
                break;
            case INDIVIDUAL:
                enterWithCheck(onRequisitesBlock.inputFioIp, company.getFioIp());
                enterWithCheck(onRequisitesBlock.inputFioContactPerson, company.getContactorFio());
                inputWithCheckPhone(onRequisitesBlock.inputPhoneContactPerson, company.getContactorPhone());
                enterWithCheck(onRequisitesBlock.inputEmailContactPerson, company.getContactorEmail());
                enterAddressWithCheck(onRequisitesBlock.inputRegisteredAddress, company.getAddressRegistered());
                inputWithCheckPhone(onRequisitesBlock.inputCompanyPhone, company.getPhoneOfCompanyOrIp());
                enterWithCheck(onRequisitesBlock.inputCompanyEmail, company.getEmailOfCompanyOrIp());
                enterWithCheck(onRequisitesBlock.inputOgrnIp, company.getOgrnIp());
                enterWithCheck(onRequisitesBlock.inputInn, company.getInn());
                break;
            case FOREIGN:
                enterWithCheck(onRequisitesBlock.inputOfficialName, company.getOfficialName());
                enterWithCheck(onRequisitesBlock.inputFioContactPerson, company.getContactorFio());
                inputWithCheckPhone(onRequisitesBlock.inputPhoneContactPerson, company.getContactorPhone());
                enterWithCheck(onRequisitesBlock.inputEmailContactPerson, company.getContactorEmail());
                enterAddressWithCheck(onRequisitesBlock.inputActualAddress, company.getAddressActual());
                enterAddressWithCheck(onRequisitesBlock.inputRegisteredAddress, company.getAddressRegistered());
                inputWithCheckPhone(onRequisitesBlock.inputCompanyPhone, company.getPhoneOfCompanyOrIp());
                enterWithCheck(onRequisitesBlock.inputCompanyEmail, company.getEmailOfCompanyOrIp());
                enterWithCheck(onRequisitesBlock.inputEin, company.getEin());
                break;
            default:
                assertThat("Неизвестная форма регистрации компании, нет реализации", 1, equalTo(2));
                break;
        }

    }
    @Step("Нажать кнопку сохранить и проверить наличие уведомления об успехе")
    public void saveRequisitesAndCheckNotification() {
        $(onRequisitesBlock.container)
                .$(onAnyBlock.btnBlockSave).click();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована", 25000, true);
    }
    @Step("Проверка значений в блоке 'Реквизиты компании'")
    public void checkRequisitesValues(Company company) {
        switch (company.getCompanyForm()) {
            case LEGAL_ENTITY:
                $(onRequisitesBlock.textOfficialName).shouldHave(exactTextCaseSensitive(company.getOfficialName()));
                $(onRequisitesBlock.textRegistrationForm).shouldHave(exactTextCaseSensitive(company.getCompanyForm().getText()));
                $(onRequisitesBlock.textFioContactPerson).shouldHave(exactTextCaseSensitive(company.getContactorFio()));
                assertThat("Телефон контактного лица",
                        $(onRequisitesBlock.textPhoneContactPerson).text(),
                        isThisPhone(company.getContactorPhone()));
                $(onRequisitesBlock.textEmailContactPerson).shouldHave(exactTextCaseSensitive(company.getContactorEmail()));
                $(onRequisitesBlock.textActualAddress).shouldHave(exactTextCaseSensitive(company.getAddressActual()));
                $(onRequisitesBlock.textRegisteredAddress).shouldHave(exactTextCaseSensitive(company.getAddressRegistered()));
                assertThat("Телефон",
                        $(onRequisitesBlock.textCompanyPhone).text(),
                        isThisPhone(company.getPhoneOfCompanyOrIp()));
                $(onRequisitesBlock.textCompanyEmail).shouldHave(exactTextCaseSensitive(company.getEmailOfCompanyOrIp()));
                $(onRequisitesBlock.textOgrn).shouldHave(exactTextCaseSensitive(company.getOgrn()));
                $(onRequisitesBlock.textKpp).shouldHave(exactTextCaseSensitive(company.getKpp()));
                $(onRequisitesBlock.textInn).shouldHave(exactTextCaseSensitive(company.getInn()));
                break;
            case INDIVIDUAL:
                $(onRequisitesBlock.textFioIp).shouldHave(exactTextCaseSensitive(company.getFioIp()));
                $(onRequisitesBlock.textRegistrationForm).shouldHave(exactTextCaseSensitive(company.getCompanyForm().getText()));
                $(onRequisitesBlock.textFioContactPerson).shouldHave(exactTextCaseSensitive(company.getContactorFio()));
                assertThat("Телефон контактного лица",
                        $(onRequisitesBlock.textPhoneContactPerson).text(),
                        isThisPhone(company.getContactorPhone()));
                $(onRequisitesBlock.textEmailContactPerson).shouldHave(exactTextCaseSensitive(company.getContactorEmail()));
                $(onRequisitesBlock.textRegisteredAddressIp).shouldHave(exactTextCaseSensitive(company.getAddressRegistered()));
                assertThat("Телефон",
                        $(onRequisitesBlock.textPhone).text(),
                        isThisPhone(company.getPhoneOfCompanyOrIp()));
                $(onRequisitesBlock.textEmail).shouldHave(exactTextCaseSensitive(company.getEmailOfCompanyOrIp()));
                $(onRequisitesBlock.textOgrnIp).shouldHave(exactTextCaseSensitive(company.getOgrnIp()));
                $(onRequisitesBlock.textInn).shouldHave(exactTextCaseSensitive(company.getInn()));
                break;
            case FOREIGN:
                $(onRequisitesBlock.textOfficialName).shouldHave(exactTextCaseSensitive(company.getOfficialName()));
                $(onRequisitesBlock.textRegistrationForm).shouldHave(exactTextCaseSensitive(company.getCompanyForm().getText()));
                $(onRequisitesBlock.textFioContactPerson).shouldHave(exactTextCaseSensitive(company.getContactorFio()));
                assertThat("Телефон контактного лица",
                        $(onRequisitesBlock.textPhoneContactPerson).text(),
                        isThisPhone(company.getContactorPhone()));
                $(onRequisitesBlock.textEmailContactPerson).shouldHave(exactTextCaseSensitive(company.getContactorEmail()));
                $(onRequisitesBlock.textActualAddress).shouldHave(exactTextCaseSensitive(company.getAddressActual()));
                $(onRequisitesBlock.textRegisteredAddress).shouldHave(exactTextCaseSensitive(company.getAddressRegistered()));
                assertThat("Телефон",
                        $(onRequisitesBlock.textCompanyPhone).text(),
                        isThisPhone(company.getPhoneOfCompanyOrIp()));
                $(onRequisitesBlock.textCompanyEmail).shouldHave(exactTextCaseSensitive(company.getEmailOfCompanyOrIp()));
                $(onRequisitesBlock.textEin).shouldHave(exactTextCaseSensitive(company.getEin()));
                break;
            default:
                assertThat("Неизвестная форма регистрации компании, нет реализации", 1, equalTo(2));
                break;
        }
    }
    @Step("Ввести значения блока, сохранить и проверить значения после сохранения")
    public void checkEditOfRequisitesBlock(Company company) {
        startEditRequisitesBlock();
        inputRequisitesBlock(company);
        saveRequisitesAndCheckNotification();
        checkRequisitesValues(company); // проверка новых значений до и после рефреша страницы
        refresh();
        checkRequisitesValues(company);
    }

    /* Банковские реквизиты - редактирование */
    @Step("Открыть раздел Банковские реквизиты для редактирования")
    public void startEditBankRequisitesBlock() {
        startEditBlock(onBankRequisitesBlock.container, true);
    }
    @Step("Открыть раздел Банковские реквизиты для редактирования")
    public void startEditBankRequisitesBlock(boolean reopenIfOpened) {
        startEditBlock(onBankRequisitesBlock.container, reopenIfOpened);
    }
    @Step("Заполнить валидными данными поля блока 'Банковские реквизиты'")
    public void inputBankRequisitesBlock(BankRequisites bankRequisites) {
        switch (bankRequisites.getCompanyForm()) {
            case LEGAL_ENTITY:
            case INDIVIDUAL:
                enterWithCheck(onBankRequisitesBlock.inputBank, bankRequisites.getBank());
                enterAddressWithCheck(onBankRequisitesBlock.inputAddress, bankRequisites.getBankAddress());
                enterWithCheck(onBankRequisitesBlock.inputBik, bankRequisites.getBik());
                enterWithCheck(onBankRequisitesBlock.inputRS, bankRequisites.getCheckAccount());
                enterWithCheck(onBankRequisitesBlock.inputKS, bankRequisites.getCorrAccount());
                break;
            case FOREIGN:
                enterWithCheck(onBankRequisitesBlock.inputBank, bankRequisites.getBank());
                enterAddressWithCheck(onBankRequisitesBlock.inputAddress, bankRequisites.getBankAddress());
                enterWithCheck(onBankRequisitesBlock.inputSwift, bankRequisites.getSwift());
                enterWithCheck(onBankRequisitesBlock.inputAccountNumber, bankRequisites.getAccountNumber());
                enterWithCheck(onBankRequisitesBlock.inputRoutingNumber, bankRequisites.getAchRoutingNumber());
                break;
        }
    }
    @Step("Нажать кнопку сохранить и проверить наличие уведомления об успехе")
    public void saveBankRequisitesAndCheckNotification() {
        $(onBankRequisitesBlock.container)
                .$(onAnyBlock.btnBlockSave).click();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована", 25000, true);
    }
    @Step("Проверка значений в блоке 'Банковские реквизиты'")
    public void checkBankRequisitesValues(BankRequisites requisites) {
        switch (requisites.getCompanyForm()) {
            case LEGAL_ENTITY:
            case INDIVIDUAL:
                $(onBankRequisitesBlock.textBank).shouldHave(exactTextCaseSensitive(requisites.getBank()));
                $(onBankRequisitesBlock.textAddress).shouldHave(exactTextCaseSensitive(requisites.getBankAddress()));
                $(onBankRequisitesBlock.textBik).shouldHave(exactTextCaseSensitive(requisites.getBik()));
                $(onBankRequisitesBlock.textRS).shouldHave(exactTextCaseSensitive(requisites.getCheckAccount()));
                $(onBankRequisitesBlock.textKS).shouldHave(exactTextCaseSensitive(requisites.getCorrAccount()));
                break;
            case FOREIGN:
                $(onBankRequisitesBlock.textBank).shouldHave(exactTextCaseSensitive(requisites.getBank()));
                $(onBankRequisitesBlock.textAddress).shouldHave(exactTextCaseSensitive(requisites.getBankAddress()));
                $(onBankRequisitesBlock.textSwift).shouldHave(exactTextCaseSensitive(requisites.getSwift()));
                $(onBankRequisitesBlock.textAccountNumber).shouldHave(exactTextCaseSensitive(requisites.getAccountNumber()));
                $(onBankRequisitesBlock.textRoutingNumber).shouldHave(exactTextCaseSensitive(requisites.getAchRoutingNumber()));
                break;
            default:
                Assert.fail("Неизвестный тип компании, не реализовано");
        }

    }
    @Step("Ввести значения блока, сохранить и проверить значения после сохранения")
    public void checkEditOfBankRequisitesBlock(BankRequisites bank) {
        startEditBankRequisitesBlock();
        inputBankRequisitesBlock(bank);
        saveBankRequisitesAndCheckNotification();
        checkBankRequisitesValues(bank); // до и после рефреша
        refresh();
        checkBankRequisitesValues(bank);
    }

    /* Руководитель компании - редактирование */
    @Step("Открыть раздел 'Руководитель компании' для редактирования")
    public void startEditDirectorBlock() {
        startEditBlock(onDirectorBlock.container, true);
    }
    @Step("Открыть раздел 'Руководитель компании' для редактирования")
    public void startEditDirectorBlock(boolean reopenIfOpened) {
        startEditBlock(onDirectorBlock.container, reopenIfOpened);
    }

    @Step("Заполнить валидными данными поля блока 'Руководитель компании'")
    public void inputDirectorBlock(String fio, Phone phone) {
        enterWithCheck(onDirectorBlock.inputFio, fio);
        inputWithCheckPhone(onDirectorBlock.inputPhone, phone);
    }
    @Step("Нажать кнопку сохранить и проверить наличие уведомления об успехе")
    public void saveDirectorAndCheckNotification() {
        $(onDirectorBlock.container)
                .$(onAnyBlock.btnBlockSave).click();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована", 25000, true);
    }
    @Step("Проверка значений в блоке 'Руководитель компании'")
    public void checkDirectorValues(String fio, Phone phone) {
        $(onDirectorBlock.textFio).shouldHave(exactTextCaseSensitive(fio));
        assertThat($(onDirectorBlock.textPhone).getText(), isThisPhone(phone));
    }
    @Step("Ввести значения блока, сохранить и проверить значения после сохранения")
    public void checkEditOfDirectorBlock(String fio, Phone phone) {
        startEditDirectorBlock();
        inputDirectorBlock(fio, phone);
        saveDirectorAndCheckNotification();
        checkDirectorValues(fio, phone); // до и после рефреша
        refresh();
        checkDirectorValues(fio, phone);
    }

    /* Главный бухгалтер - редактирование */
    @Step("Открыть раздел 'Главный бухгалтер' для редактирования")
    public void startEditMainAccountantBlock() {
        startEditBlock(onMainAccountantBlock.container, true);
    }
    @Step("Открыть раздел 'Главный бухгалтер' для редактирования")
    public void startEditMainAccountantBlock(boolean reopenIfOpened) {
        startEditBlock(onMainAccountantBlock.container, reopenIfOpened);
    }
    @Step("Заполнить валидными данными поля блока 'Главный бухгалтер'")
    public void inputMainAccountantBlock(String fio, Phone phone) {
        enterWithCheck(onMainAccountantBlock.inputFio, fio);
        inputWithCheckPhone(onMainAccountantBlock.inputPhone, phone);
    }
    @Step("Нажать кнопку сохранить и проверить наличие уведомления об успехе")
    public void saveMainAccountantAndCheckNotification() {
        $(onMainAccountantBlock.container)
                .$(onAnyBlock.btnBlockSave).click();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована", 25000, true);
    }
    @Step("Проверка значений в блоке 'Главный бухгалтер'")
    public void checkMainAccountantValues(String fio, Phone phone) {
        $(onMainAccountantBlock.textFio).shouldHave(exactTextCaseSensitive(fio));
        assertThat($(onMainAccountantBlock.textPhone).getText(), isThisPhone(phone));
    }
    @Step("Ввести значения блока, сохранить и проверить значения после сохранения")
    public void checkEditOfMainAccountantBlock(String fio, Phone phone) {
        startEditMainAccountantBlock();
        inputMainAccountantBlock(fio, phone);
        saveMainAccountantAndCheckNotification();
        checkMainAccountantValues(fio, phone); // до и после рефреша
        refresh();
        checkMainAccountantValues(fio, phone);
    }

    /* Представитель - редактирование */
    @Step("Открыть раздел 'Представитель' для редактирования")
    public void startEditPredstavitelBlock() {
        startEditBlock(onPredstavitelBlock.container, true);
    }
    @Step("Открыть раздел 'Представитель' для редактирования")
    public void startEditPredstavitelBlock(boolean reopenIfOpened) {
        startEditBlock(onPredstavitelBlock.container, reopenIfOpened);
    }

    @Step("Заполнить валидными данными поля блока 'Представитель'")
    public void inputPredstavitelBlock(String fio, String basedOn) {
        enterWithCheck(onPredstavitelBlock.inputFio, fio);
        enterWithCheck(onPredstavitelBlock.inputBased, basedOn);
    }
    @Step("Нажать кнопку сохранить и проверить наличие уведомления об успехе")
    public void savePredstavitelAndCheckNotification() {
        $(onPredstavitelBlock.container)
                .$(onAnyBlock.btnBlockSave).click();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована", 25000, true);
    }
    @Step("Проверка значений в блоке 'Представитель'")
    public void checkPredstavitelValues(String fio, String basedOn) {
        $(onPredstavitelBlock.textFio).shouldHave(exactTextCaseSensitive(fio));
        $(onPredstavitelBlock.textBased).shouldHave(exactTextCaseSensitive(basedOn));
    }
    @Step("Ввести значения блока, сохранить и проверить значения после сохранения")
    public void checkEditOfPredstavitelBlock(String fio, String basedOn) {
        startEditPredstavitelBlock();
        inputPredstavitelBlock(fio, basedOn);
        savePredstavitelAndCheckNotification();
        checkPredstavitelValues(fio, basedOn); // до и после рефреша
        refresh();
        checkPredstavitelValues(fio, basedOn);
    }

    /* Подписант - редактирование */
    @Step("Открыть раздел 'Подписант' для редактирования")
    public void startEditPodpisantBlock() {
        startEditBlock(onPodpisantBlock.container, true);
    }
    @Step("Открыть раздел 'Подписант' для редактирования")
    public void startEditPodpisantBlock(boolean reopenIfOpened) {
        startEditBlock(onPodpisantBlock.container, reopenIfOpened);
    }

    @Step("Заполнить валидными данными поля блока 'Подписант'")
    public void inputPodpisantBlock(String role, String transcript) {
        enterWithCheck(onPodpisantBlock.inputRole, role);
        enterWithCheck(onPodpisantBlock.inputTranscript, transcript);
    }
    @Step("Нажать кнопку сохранить и проверить наличие уведомления об успехе")
    public void savePodpisantAndCheckNotification() {
        $(onPodpisantBlock.container)
                .$(onAnyBlock.btnBlockSave).click();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована", 25000, true);
    }
    @Step("Проверка значений в блоке 'Подписант'")
    public void checkPodpisantValues(String role, String transcript) {
        $(onPodpisantBlock.textRole).shouldHave(exactTextCaseSensitive(role));
        $(onPodpisantBlock.textTranscript).shouldHave(exactTextCaseSensitive(transcript));
    }
    @Step("Ввести значения блока, сохранить и проверить значения после сохранения")
    public void checkEditOfPodpisantBlock(String role, String transcript) {
        startEditPodpisantBlock();
        inputPodpisantBlock(role, transcript);
        savePodpisantAndCheckNotification();
        checkPodpisantValues(role, transcript); // до и после рефреша
        refresh();
        checkPodpisantValues(role, transcript);
    }

    /* Категория компании - редактирование */
    @Step("Открыть раздел 'Категория компании' для редактирования")
    public void startEditCategoryBlock() {
        startEditBlock(onCompanyCategoryBlock.container, true);
    }
    @Step("Открыть раздел 'Категория компании' для редактирования")
    public void startEditCategoryBlock(boolean reopenIfOpened) {
        startEditBlock(onCompanyCategoryBlock.container, reopenIfOpened);
    }
    @Step("Заполнить валидными данными поля блока 'Категория компании'")
    public void inputCategoryBlock(String category, String okved) {
        InputDropDownList.setOptionWithCheck(onCompanyCategoryBlock.inputDdlCategory, category);
        enterWithCheck(onCompanyCategoryBlock.inputOkved, okved);
    }
    @Step("Нажать кнопку сохранить и проверить наличие уведомления об успехе")
    public void saveCategoryAndCheckNotification() {
        $(onCompanyCategoryBlock.container)
                .$(onAnyBlock.btnBlockSave).click();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована", 25000, true);
    }
    @Step("Проверка значений в блоке 'Категория компании'")
    public void checkCategoryValues(String category, String okved) {
        $(onCompanyCategoryBlock.textCategory).shouldHave(exactTextCaseSensitive(category));
        $(onCompanyCategoryBlock.textOkved).shouldHave(exactTextCaseSensitive(okved));
    }
    @Step("Ввести значения блока, сохранить и проверить значения после сохранения")
    public void checkEditOfCategoryBlock(String category, String okved) {
        startEditCategoryBlock();
        inputCategoryBlock(category, okved);
        saveCategoryAndCheckNotification();
        checkCategoryValues(category, okved); // до и после рефреша
        refresh();
        checkCategoryValues(category, okved);
    }


    /* ================ Валидация ================ */

    @Step("Проверить подсказку - 'Обязательное поле'")
    public void checkPromptMandatoryField(By prompt) {
        scrollIntoViewWithMargin(prompt, false);
        $(prompt).shouldHave(exactTextCaseSensitive("Обязательное поле"));
    }

    @Step("Нажать кнопку save для блока")
    public void clickSaveForBlock(By container) {
        $(container).$(onAnyBlock.btnBlockSave).click();
    }

    /**
     * Вввод в поле значения и попытка сохранения с проверкой подсказки ввода.
     * Локатор контейнера нужен для поиска кнопки сохранения
     */
    @Step("Проверка валидации поля. Значение: '{value}', ожидаемый текст ошибки: '{expectedPromptText}'")
    public void checkInputValidationPrompt(By container, By input, By prompt, Object value, String expectedPromptText) {
        clearAndEnterWithCheck(input, value);
        $(container).$(onAnyBlock.btnBlockSave).click();
        scrollIntoViewWithMargin(input);
        $(prompt).shouldHave(exactTextCaseSensitive(expectedPromptText));
    }

    /**
     * Вввод в поле адреса значения и попытка сохранения с проверкой подсказки ввода.
     * Локатор контейнера нужен для поиска кнопки сохранения
     */
    @Step("Проверка валидации поля адреса. Значение: '{value}', ожидаемый текст ошибки: '{expectedPromptText}'")
    public void checkAddressInputValidationPrompt(By container, By input, By prompt, Object value, String expectedPromptText) {
        enterAddressWithCheck(input, value);
        $(container).$(onAnyBlock.btnBlockSave).click();
        scrollIntoViewWithMargin(input);
        $(prompt).shouldHave(exactTextCaseSensitive(expectedPromptText));
    }

    @Step("Проверка валидации поля. Ввод должен быть обрезан до {limit} символов")
    public void checkInputValidationLength(By input, String text, int limit) {
        $(input).setValue(text);
        $(input).shouldHave(exactTextCaseSensitive(text.substring(0, limit)));
    }

    @Step("Проверка ошибки при пустом значении обязательного поля")
    public void mandatoryFieldPromptCheck(By promptLocator) {
        $(promptLocator).shouldHave(exactTextCaseSensitive("Обязательное поле"));
    }

    /**
     * Проверяет что ввод text в поле locatorInput блока locatorContainer был успешен,
     * при этом при попытке сохранить изменения не появляется ошибка с локатором locatorPrompt
     */
    @Step("В поле разрешен текст: {text}")
    public void isInputOfTheTextAllowed(By locatorContainer, By locatorInput, By locatorPrompt, String text) {
        clearAndEnterWithCheck(locatorInput, text);
        $(locatorContainer).$(onAnyBlock.btnBlockSave).click();
        scrollIntoViewWithMargin(locatorInput);
        $(locatorPrompt).shouldNotBe(visible);
    }

    /**
     * Проверяет что ввод address в поле locatorInput блока locatorContainer был успешен,
     * при этом при попытке сохранить изменения не появляется ошибка с локатором locatorPrompt
     */
    @Step("В поле разрешен адрес: {address}")
    public void isInputOfTheAddressAllowed(By locatorContainer, By locatorInput, By locatorPrompt, String address) {
        $(locatorInput).scrollIntoView(true);
        enterAddressWithCheck(locatorInput, address);
        $(locatorContainer).$(onAnyBlock.btnBlockSave).click();
        $(locatorPrompt).shouldNotBe(visible);
    }

    @Step("Проверить, что максимальная длина строки - {maxLength} символов")
    public void validationMaxLength(By locatorContainer, By locatorInput, By locatorPrompt, int maxLength, boolean describedString, StringGenerator generator) {
        checkInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                describedString ?
                        generator.generateDescribed(maxLength + 1, "Строка длиной " + (maxLength + 1)) :
                        generator.generate(maxLength + 1),
                "Максимальная длина - " + maxLength + " символов");
        checkInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                describedString ?
                        generator.generateDescribed(maxLength + 2, "Строка длиной " + (maxLength + 2)) :
                        generator.generate(maxLength + 2),
                "Максимальная длина - " + maxLength + " символов");
        isInputOfTheTextAllowed(locatorContainer, locatorInput, locatorPrompt,
                describedString ?
                        generator.generateDescribed(maxLength, "Строка длиной " + maxLength) :
                        generator.generate(maxLength));
    }

    @Step("Проверить, что максимальная длина строки адреса - {maxLength} символов")
    public void validationAddressMaxLength(By locatorContainer, By locatorInput, By locatorPrompt, int maxLength) {
        isInputOfTheAddressAllowed(locatorContainer, locatorInput, locatorPrompt,
                new StringGenerator().generateDescribed(maxLength, "Строка длиной " + maxLength));
        checkAddressInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                new StringGenerator().generateDescribed(maxLength + 1, "Строка длиной " + (maxLength + 1)),
                "Максимальная длина - " + maxLength + " символов");
        checkAddressInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                new StringGenerator().generateDescribed(maxLength + 2, "Строка длиной " + (maxLength + 1)),
                "Максимальная длина - " + maxLength + " символов");
    }

    public void validationMaxLength(By locatorContainer, By locatorInput, By locatorPrompt, int maxLength) {
        validationMaxLength(locatorContainer, locatorInput, locatorPrompt, maxLength, true, new StringGenerator());
    }

    @Step("Проверить, что минимальная длина строки - {minLength} символов")
    public void validationMinLength(By locatorContainer, By locatorInput, By locatorPrompt, int minLength, boolean describedString, StringGenerator generator) {
        checkInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                describedString ?
                        generator.generateDescribed(minLength - 1, "Строка длиной " + (minLength - 1)) :
                        generator.generate(minLength - 1),
                "Минимальная длина строки " + minLength + " символов");
        if (minLength > 2) {
            checkInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                    describedString ?
                            generator.generateDescribed(minLength - 2, "Строка длиной " + (minLength - 2)) :
                            generator.generate(minLength - 2),
                    "Минимальная длина строки " + minLength + " символов");
        }
        isInputOfTheTextAllowed(locatorContainer, locatorInput, locatorPrompt,
                describedString ?
                        generator.generateDescribed(minLength, "Строка длиной " + minLength) :
                        generator.generate(minLength));
    }

    @Step("Проверить, что минимальная длина строки адреса - {minLength} символов")
    public void validationAddressMinLength(By locatorContainer, By locatorInput, By locatorPrompt, int minLength) {
        isInputOfTheAddressAllowed(locatorContainer, locatorInput, locatorPrompt,
                new StringGenerator().generateDescribed(minLength, "Строка длиной " + minLength));
        checkAddressInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                new StringGenerator().generateDescribed(minLength - 1, "Строка длиной " + (minLength + 1)),
                "Минимальная длина строки " + minLength + " символов");
        checkAddressInputValidationPrompt(locatorContainer, locatorInput, locatorPrompt,
                new StringGenerator().generateDescribed(minLength - 2, "Строка длиной " + (minLength + 1)),
                "Минимальная длина строки " + minLength + " символов");
    }

    public void validationMinLength(By locatorContainer, By locatorInput, By locatorPrompt, int minLength) {
        validationMinLength(locatorContainer, locatorInput, locatorPrompt, minLength, true, new StringGenerator());
    }

    /* Реквизиты компании */
    @Step("Реквизиты компании - валидация. Step 1. Проверка обязательности полей")
    public void validationCompanyRequisitesBlankInputsCheck(CompanyForm companyForm) {
        switch (companyForm) {
            case LEGAL_ENTITY:
                clearWithCheck(onRequisitesBlock.inputOfficialName);
                clearWithCheck(onRequisitesBlock.inputFioContactPerson);
                enterPhoneRaw(onRequisitesBlock.inputPhoneContactPerson, "");
                enterWithCheck(onRequisitesBlock.inputEmailContactPerson, "");
                enterAddressWithCheck(onRequisitesBlock.inputActualAddress, "");
                enterAddressWithCheck(onRequisitesBlock.inputRegisteredAddress, "");
                enterPhoneRaw(onRequisitesBlock.inputPhone, "");
                clearWithCheck(onRequisitesBlock.inputEmail);
                clearWithCheck(onRequisitesBlock.inputOgrn);
                clearWithCheck(onRequisitesBlock.inputKpp);
                clearWithCheck(onRequisitesBlock.inputInn);
                $(onRequisitesBlock.container).$(onAnyBlock.btnBlockSave).click();

                mandatoryFieldPromptCheck(onRequisitesBlock.promptOfficialName);
                $(onRequisitesBlock.promptFioContactPerson).shouldNotBe(visible);
                $(onRequisitesBlock.promptPhoneContactPerson).shouldNotBe(visible);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptPhone);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptEmail);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptActualAddress);

                $(onRequisitesBlock.promptRegisteredAddress).scrollIntoView(false);

                mandatoryFieldPromptCheck(onRequisitesBlock.promptRegisteredAddress);
                $(onRequisitesBlock.promptEmailContactPerson).shouldNotBe(visible);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptOgrn);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptKpp);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptInn);
                break;
            case INDIVIDUAL:
                clearWithCheck(onRequisitesBlock.inputFioIp);
                clearWithCheck(onRequisitesBlock.inputFioContactPerson);
                enterPhoneRaw(onRequisitesBlock.inputPhoneContactPerson, "");
                enterWithCheck(onRequisitesBlock.inputEmailContactPerson, "");
                enterAddressWithCheck(onRequisitesBlock.inputRegisteredAddress, "");
                enterPhoneRaw(onRequisitesBlock.inputCompanyPhone, "");
                clearWithCheck(onRequisitesBlock.inputCompanyEmail);
                clearWithCheck(onRequisitesBlock.inputOgrnIp);
                clearWithCheck(onRequisitesBlock.inputInn);
                $(onRequisitesBlock.container).$(onAnyBlock.btnBlockSave).click();

                mandatoryFieldPromptCheck(onRequisitesBlock.promptFioIp);
                $(onRequisitesBlock.promptFioContactPerson).shouldNotBe(visible);
                $(onRequisitesBlock.promptPhoneContactPerson).shouldNotBe(visible);
                $(onRequisitesBlock.promptEmailContactPerson).shouldNotBe(visible);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptRegisteredAddress);

                scrollIntoViewWithMargin(onRequisitesBlock.inputCompanyPhone, true);

                mandatoryFieldPromptCheck(onRequisitesBlock.promptCompanyPhone);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptCompanyEmail);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptOgrnIp);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptInn);
                break;
            case FOREIGN:
                clearWithCheck(onRequisitesBlock.inputOfficialName);
                clearWithCheck(onRequisitesBlock.inputFioContactPerson);
                enterPhoneRaw(onRequisitesBlock.inputPhoneContactPerson, "");
                enterWithCheck(onRequisitesBlock.inputEmailContactPerson, "");
                enterAddressWithCheck(onRequisitesBlock.inputActualAddress, "");
                enterAddressWithCheck(onRequisitesBlock.inputRegisteredAddress, "");
                enterPhoneRaw(onRequisitesBlock.inputPhone, "");
                clearWithCheck(onRequisitesBlock.inputEmail);
                clearWithCheck(onRequisitesBlock.inputEin);
                $(onRequisitesBlock.container).$(onAnyBlock.btnBlockSave).click();

                mandatoryFieldPromptCheck(onRequisitesBlock.promptOfficialName);
                $(onRequisitesBlock.promptFioContactPerson).shouldNotBe(visible);
                $(onRequisitesBlock.promptPhoneContactPerson).shouldNotBe(visible);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptPhone);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptEmail);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptActualAddress);

                scrollIntoViewWithMargin(onRequisitesBlock.promptRegisteredAddress);

                mandatoryFieldPromptCheck(onRequisitesBlock.promptRegisteredAddress);
                $(onRequisitesBlock.promptEmailContactPerson).shouldNotBe(visible);
                mandatoryFieldPromptCheck(onRequisitesBlock.promptEin);
                break;
            default:
                fail("Не реализовано");
        }
    }

    @Step("Поле 'Форма регистрации бизнеса' недоступна")
    public void validationCompanyRequisitesFormOfRegDisabled() {
        $(onRequisitesBlock.inputRegistrationForm).shouldNotBe(enabled);
    }

    @Step("Проверка подсказки поля ФИО контактного лица при неверном вводе {prompt}")
    public void validationCompanyRequisitesFioContactorPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputFioContactPerson,
                onRequisitesBlock.promptFioContactPerson,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля Оф. название компании:")
    public void isInputAllowedRequisitesOfficialName(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputOfficialName,
                onRequisitesBlock.promptOfficialName
                , text);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля ФИО контактного лица:")
    public void isInputAllowedRequisitesContactorFio(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputFioContactPerson,
                onRequisitesBlock.promptFioContactPerson
                , text);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля E-mail контактного лица")
    public void isInputAllowedRequisitesContactorEmail(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputEmailContactPerson,
                onRequisitesBlock.promptEmailContactPerson
                , text);
    }
    @Step("Проверка подсказки поля E-mail контактного лица при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesEmailContactorPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputEmailContactPerson,
                onRequisitesBlock.promptEmailContactPerson,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'Фактический адрес'")
    public void isInputAllowedRequisitesActualAddress(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputActualAddress,
                onRequisitesBlock.promptActualAddress
                , text);
    }
    @Step("Проверка подсказки поля 'Фактический адрес' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesActualAddressPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputActualAddress,
                onRequisitesBlock.promptActualAddress,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'Адрес регистрации'")
    public void isInputAllowedRequisitesRegisteredAddress(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputRegisteredAddress,
                onRequisitesBlock.promptRegisteredAddress
                , text);
    }
    @Step("Проверка подсказки поля 'Адрес регистрации' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesRegisteredAddressPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputRegisteredAddress,
                onRequisitesBlock.promptRegisteredAddress,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'E-mail компании'")
    public void isInputAllowedRequisitesCompanyEmail(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputCompanyEmail,
                onRequisitesBlock.promptCompanyEmail
                , text);
    }
    @Step("Проверка подсказки поля 'E-mail компании' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesCompanyEmailPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputCompanyEmail,
                onRequisitesBlock.promptCompanyEmail,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'Фактический адрес'")
    public void isInputAllowedRequisitesOgrn(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputOgrn,
                onRequisitesBlock.promptOgrn
                , text);
    }
    @Step("Проверка подсказки поля 'Фактический адрес' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesOgrnPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputOgrn,
                onRequisitesBlock.promptOgrn,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'ИНН' (ЮЛ)")
    public void isInputAllowedRequisitesInnLe(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputInn,
                onRequisitesBlock.promptInn
                , text);
    }
    @Step("Проверка подсказки поля 'ИНН' (ЮЛ) при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesInnLePrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputInn,
                onRequisitesBlock.promptInn,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'КПП'")
    public void isInputAllowedRequisitesKpp(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputKpp,
                onRequisitesBlock.promptKpp
                , text);
    }
    @Step("Проверка подсказки поля 'КПП' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesKppPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputKpp,
                onRequisitesBlock.promptKpp,
                text, prompt);
    }
    @Step("Проверка, что ввод '{text}' разрешен для поля 'EIN'")
    public void isInputAllowedRequisitesEin(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputEin,
                onRequisitesBlock.promptEin
                , text);
    }
    @Step("Проверка подсказки поля 'EIN' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesEinPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputEin,
                onRequisitesBlock.promptEin,
                text, prompt);
    }
    @Step("Проверка, что ввод '{text}' разрешен для поля 'КПП'")
    public void isInputAllowedFioIp(String text) {
        isInputOfTheTextAllowed(onRequisitesBlock.container,
                onRequisitesBlock.inputFioIp,
                onRequisitesBlock.promptFioIp
                , text);
    }
    @Step("Проверка подсказки поля 'ФИО КПП' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyRequisitesFioIpPrompt(String text, String prompt) {
        checkInputValidationPrompt(onRequisitesBlock.container,
                onRequisitesBlock.inputFioIp,
                onRequisitesBlock.promptFioIp,
                text, prompt);
    }

    /* Реквизиты банка */

    @Step("Банковские реквизиты - валидация. Проверка обязательности полей")
    public void validationCompanyBankRequisitesMandatoryInputsCheck(CompanyForm companyForm) {
        switch (companyForm) {
            case LEGAL_ENTITY:
            case INDIVIDUAL:
                $(onBankRequisitesBlock.container).scrollIntoView(true);
                clearWithCheck(onBankRequisitesBlock.inputBank);
                clearWithCheck(onBankRequisitesBlock.inputAddress);
                clearWithCheck(onBankRequisitesBlock.inputBik);
                clearWithCheck(onBankRequisitesBlock.inputRS);
                clearWithCheck(onBankRequisitesBlock.inputKS);
                $(onBankRequisitesBlock.container).$(onAnyBlock.btnBlockSave).click();

                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptBank);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptAddress);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptBik);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptRS);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptKS);
                break;
            case FOREIGN:
                $(onBankRequisitesBlock.container).scrollIntoView(true);
                clearWithCheck(onBankRequisitesBlock.inputBank);
                clearWithCheck(onBankRequisitesBlock.inputAddress);
                clearWithCheck(onBankRequisitesBlock.inputSwift);
                clearWithCheck(onBankRequisitesBlock.inputAccountNumber);
                clearWithCheck(onBankRequisitesBlock.inputRoutingNumber);
                $(onBankRequisitesBlock.container).$(onAnyBlock.btnBlockSave).click();

                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptBank);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptAddress);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptSwift);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptAccountNumber);
                mandatoryFieldPromptCheck(onBankRequisitesBlock.promptRoutingNumber);
                break;
            default:
                fail("Не реализовано");
        }
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'БИК'")
    public void isInputAllowedBankRequisitesBik(String text) {
        isInputOfTheTextAllowed(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputBik,
                onBankRequisitesBlock.promptBik
                , text);
    }
    @Step("Проверка подсказки поля 'БИК' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyBankRequisitesBikPrompt(String text, String prompt) {
        checkInputValidationPrompt(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputBik,
                onBankRequisitesBlock.promptBik,
                text, prompt);
    }

    @Step("Проверка, что ввод '{text}' разрешен для поля 'Расчетный счёт'")
    public void isInputAllowedBankRequisitesCheckingAccount(String text) {
        isInputOfTheTextAllowed(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputRS,
                onBankRequisitesBlock.promptRS
                , text);
    }
    @Step("Проверка подсказки поля 'Расчетный счёт' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyBankRequisitesCheckingAccountPrompt(String text, String prompt) {
        checkInputValidationPrompt(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputRS,
                onBankRequisitesBlock.promptRS,
                text, prompt);
    }
    @Step("Проверка, что ввод '{text}' разрешен для поля 'Account number'")
    public void isInputAllowedBankRequisitesAccountNumber(String text) {
        isInputOfTheTextAllowed(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputAccountNumber,
                onBankRequisitesBlock.promptAccountNumber
                , text);
    }
    @Step("Проверка подсказки поля 'Account number' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyBankRequisitesAccountNumberPrompt(String text, String prompt) {
        checkInputValidationPrompt(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputAccountNumber,
                onBankRequisitesBlock.promptAccountNumber,
                text, prompt);
    }
    @Step("Проверка, что ввод '{text}' разрешен для поля 'Банковский код (ACH Routing number)'")
    public void isInputAllowedBankRequisitesRoutingNumber(String text) {
        isInputOfTheTextAllowed(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputRoutingNumber,
                onBankRequisitesBlock.promptRoutingNumber
                , text);
    }
    @Step("Проверка подсказки поля 'Банковский код (ACH Routing number)' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyBankRequisitesRoutingNumberPrompt(String text, String prompt) {
        checkInputValidationPrompt(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputRoutingNumber,
                onBankRequisitesBlock.promptRoutingNumber,
                text, prompt);
    }
    @Step("Проверка, что ввод '{text}' разрешен для поля 'Корреспондентский счёт'")
    public void isInputAllowedBankRequisitesCorrespondentAccount(String text) {
        isInputOfTheTextAllowed(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputKS,
                onBankRequisitesBlock.promptKS
                , text);
    }
    @Step("Проверка подсказки поля 'Корреспондентский счёт' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyBankRequisitesCorrespondentAccountPrompt(String text, String prompt) {
        checkInputValidationPrompt(onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputKS,
                onBankRequisitesBlock.promptKS,
                text, prompt);
    }
    @Step("Проверка, что ввод '{text}' разрешен для поля 'ФИО руководителя'")
    public void isInputAllowedDirectorFio(String text) {
        isInputOfTheTextAllowed(onDirectorBlock.container,
                onDirectorBlock.inputFio,
                onDirectorBlock.promptFio
                , text);
    }
    @Step("Проверка подсказки поля 'ФИО руководителя' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyDirectorFioPrompt(String text, String prompt) {
        checkInputValidationPrompt(onDirectorBlock.container,
                onDirectorBlock.inputFio,
                onDirectorBlock.promptFio,
                text, prompt);
    }
    @Step("Проверка, что ввод '{text}' разрешен для поля 'ФИО главного бухгалтера'")
    public void isInputAllowedMainAccountantFio(String text) {
        isInputOfTheTextAllowed(onMainAccountantBlock.container,
                onMainAccountantBlock.inputFio,
                onMainAccountantBlock.promptFio
                , text);
    }
    @Step("Проверка подсказки поля 'ФИО главного бухгалтера' при неверном вводе '{text}'/'{prompt}'")
    public void validationCompanyMainAccountantFioPrompt(String text, String prompt) {
        checkInputValidationPrompt(onMainAccountantBlock.container,
                onMainAccountantBlock.inputFio,
                onMainAccountantBlock.promptFio,
                text, prompt);
    }

}
