package steps;

import com.codeborne.selenide.CollectionCondition;
import io.qameta.allure.Step;
import pageObject.pages.CompaniesPage;
import pageObject.pages.CompanyCardPage;
import testData.enums.CompanyForm;
import testData.models.Company;
import utils.fileCreator.FileCreator;
import utils.fileCreator.FileFormats;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.core.IsEqual.equalTo;
import static pageObject.pages.CompanyCardNmDocsPage.companyCardNmDocsFrame;
import static service.CustomAssert.assertThat;

public class CompanyCardNmDocsContractSteps {

    CompaniesSteps companiesSteps = new CompaniesSteps();
    BaseSteps baseSteps = new BaseSteps();

    /** @param byUrl true - открыть по прямой ссылке (если возможно), false - открыть через UI */
    @Step("Открыть страницу нужной компании, если необходимо")
    public void openCompanyNmDocsIfNeeds(Company company, boolean byUrl, boolean reopenAlways) {
        String searchBy = company.getCompanyForm() == CompanyForm.INDIVIDUAL ? company.getFioIp() : company.getShortName();
        if (
                reopenAlways || !($(CompanyCardPage.onCompanyCardFrame.textHeader).exists()) ||
                        !($(CompanyCardPage.onCompanyCardFrame.textHeader).getText().contains(searchBy)) ||
                        !(url().contains("/agency-contract"))
        ) {
            if (!byUrl || company.getClientId() == null) {
                companiesSteps.searchCompaniesByName(searchBy);
                $$(CompaniesPage.onCompaniesFrame.tableCompanyNames).get(0).click();
                $(CompanyCardPage.onCompanyCardFrame.tabNamemixDocs).click();
                $(CompanyCardPage.onCompanyCardFrame.textHeader).shouldHave(textCaseSensitive(searchBy));
            } else {
                open("client-card/" + company.getClientId() + "/agency-contract");
            }
        }
    }

    public void openCompanyNmDocsIfNeeds(Company company) {
        openCompanyNmDocsIfNeeds(company, true, false);
    }

    public void openCompanyNmDocs(Company company) {
        openCompanyNmDocsIfNeeds(company, true, true);
    }

    @Step("Открыть форму загрузки файлов")
    public void openUploadForm() {
        if (!$(companyCardNmDocsFrame.dropzoneText).isDisplayed())
            $(companyCardNmDocsFrame.btnAddContract).click();
    }

    @Step("Проверить, что drag-n-drop содержит правильный текст")
    public void checkTextOfDragNDrop() {
        $(companyCardNmDocsFrame.dropzoneText)
                .shouldHave(text("Выберите или перетащите до 10 файлов\n" +
                        "В формате *odt, *.docx, *.pdf, *.jpg, *.png\n" +
                        "Максимальный размер 2MB"));
    }

    @Step("Проверить, что drag-n-drop элемент не содержит текста ошибки")
    public void checkNoErrorInDragNDrop() {
        // При отсутствии ошибки последний дочерний div дропзоны содержит такой текст, поэтому такая проверка
        assertThat("Не должно быть ошибки. При отсутствии ошибки здесь отображается текст 'Максимальный размер 2MB'",
                $(companyCardNmDocsFrame.dropzoneText).lastChild().getText(),
                equalTo("Максимальный размер 2MB"));
    }

    @Step("Проверить, что drag-n-drop элемент содержит текст ошибки {error}")
    public void checkErrorInDragNDrop(String error) {
        $(companyCardNmDocsFrame.dropzoneText).lastChild().shouldHave(exactText(error));
    }

    @Step("Проверка отображения drag-n-drop элемента с кнопкой")
    public void checkDisplayingOfUploadFormDragNDrop() {
        $(companyCardNmDocsFrame.dropzoneText).shouldBe(visible);
        checkTextOfDragNDrop();
        $(companyCardNmDocsFrame.btnChooseFile).shouldBe(visible).shouldHave(exactText("Выбрать файл"));
    }

    @Step("Проверка, что форма загрузки файлов не видна")
    public void checkUploadFormNotVisible() {
        $(companyCardNmDocsFrame.btnChooseFile).shouldNotBe(visible);
        $(companyCardNmDocsFrame.dropzoneText).shouldNotBe(visible);
        $(companyCardNmDocsFrame.attachedDocuments).shouldNot(exist);
    }

    @Step("Кнопка 'Сохранить' - 'задизейблена' при отсутствии хотя бы одного прикрепленного файла")
    public void checkDisplayingOfUploadFormBtnSaveDisabled() {
        refresh();
        openUploadForm();
        $(companyCardNmDocsFrame.btnSave).click();
        baseSteps.checkWarningNotification("Файлы не выбраны");
    }

    @Step("Прикрепить файл {file}")
    public void attachFile(File file) {
        $(companyCardNmDocsFrame.btnChooseFile).click();
        $("body").pressEscape();
        $(companyCardNmDocsFrame.inputFile).uploadFile(file);
    }

    @Step("Прикрепить файл и проверить наличие прикрепленного файла {file}")
    public void attachFileAndCheckAttached(File file) {
        attachFile(file);
        assertThat("Файл " + file.getName() + " прикреплён в одном экземпляре",
                $$(companyCardNmDocsFrame.attachedDocuments).filterBy(text(file.getName())).size(),
                equalTo(1));
        checkNoErrorInDragNDrop();
    }

    @Step("Количество прикрепленных файлов должно быть: {number}")
    public void checkAttachedCount(Integer number) {
        $$(companyCardNmDocsFrame.attachedDocuments).shouldHave(CollectionCondition.size(number));
    }

    @Step("Проверить отсутствие прикрепленного файла с именем {name}")
    public void checkFileNotAttached(String name) {
        assertThat("Файл  " + name + " не должен быть прикреплён",
                $$(companyCardNmDocsFrame.attachedDocuments).filterBy(text(name)).size(),
                equalTo(0));
    }

    @Step("Проверить отсутствие прикрепленных файлов")
    public void checkNoFilesAttached() {
        assertThat("Ни одного файла не должно быть прикреплено",
                $$(companyCardNmDocsFrame.attachedDocuments).size(), equalTo(0));
    }

    @Step("Удалить один из прикрепленных файлов и проверить что он исчез с формы")
    public void deleteAttachedFileAndCheck() {
        String fileName = $$(companyCardNmDocsFrame.attachedDocuments).get(0)
                .$(companyCardNmDocsFrame.textsAttachedDocument).getText();

        int attachedDocumentsCount = $$(companyCardNmDocsFrame.attachedDocuments).size();
        $$(companyCardNmDocsFrame.attachedDocuments).get(0)
                .$(companyCardNmDocsFrame.btnsDeleteAttachedDocuments).click();

        assertThat("Прикрепленный файл  " + fileName + " должен быть удален",
                $$(companyCardNmDocsFrame.attachedDocuments).filterBy(text(fileName)).size(),
                equalTo(0));
        assertThat("Удален только один элемент",
                $$(companyCardNmDocsFrame.attachedDocuments).size(),
                equalTo(attachedDocumentsCount - 1));
    }

    @Step("Нажать кнопку Сохранить")
    public void clickSave() {
        $(companyCardNmDocsFrame.btnSave).click();
    }

    @Step("Нажать кнопку Отменить")
    public void clickCancel() {
        $(companyCardNmDocsFrame.btnCancel).click();
    }

    @Step("Проверить сообщение о успешной загрузке")
    public void checkSuccessUploadNotification() {
        baseSteps.checkSuccessNotification("Файлы успешно добавлены");
    }

    // 6 по количеству разрешенных форматов
    @Step("Предусловие: загрузка отчета с несколькими файлами (6) для компании {company}")
    public void uploadContractWithSixFilesForCompany(Company company) {
        openCompanyNmDocs(company);
        openUploadForm();
        attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "1"));
        attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "2"));
        attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "3"));
        attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF, "4"));
        attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.JPG, "5"));
        attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "6"));
        clickSave();
        checkSuccessUploadNotification();
    }

}
