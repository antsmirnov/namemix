package steps;

import com.codeborne.selenide.CollectionCondition;
import io.qameta.allure.Step;
import steps.elements.PopupQuestion;
import utils.InputMethods;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.core.IsEqual.equalTo;
import static pageObject.pages.CompanyCardNmDocsPage.companyCardNmDocsUploaded;
import static service.CustomAssert.assertThat;

public class CompanyCardNmDocsContractUploadedSteps {

    CompaniesSteps companiesSteps = new CompaniesSteps();
    BaseSteps baseSteps = new BaseSteps();

    @Step("Проверить список файлов в актуальной версии")
    public void checkActualFiles(String[] files) {
        $(companyCardNmDocsUploaded.containerActual).$$(companyCardNmDocsUploaded.linksFileName)
                .shouldHave(CollectionCondition.exactTextsCaseSensitiveInAnyOrder(files));
    }

    @Step("Открыть раздел история если он свернут")
    public void openHistoryIfNeeds() {
        if (!$(companyCardNmDocsUploaded.btnOpenCloseHistory).getText().equals("expand_more")) {
            $(companyCardNmDocsUploaded.btnOpenCloseHistory).click();
        }
    }

    @Step("Проверить список файлов последней версии в разделе История")
    public void checkLastHistoryVerFiles(String[] files) {
        openHistoryIfNeeds();
        $(companyCardNmDocsUploaded.containerHistory).$$(companyCardNmDocsUploaded.linksFileName)
                .shouldHave(CollectionCondition.exactTextsCaseSensitiveInAnyOrder(files));
    }

    @Step("Нажать кнопку Редактировать")
    public void clickBtnActualVersionEdit() {
        $(companyCardNmDocsUploaded.btnActualVersionEdit).click();
    }

    @Step("Количество загруженных файлов должно быть: {number}")
    public void checkAttachedCount(Integer number) {
        $$(companyCardNmDocsUploaded.uploadedDocuments).shouldHave(CollectionCondition.size(number));
    }

    @Step("Напротив каждого загруженного файла отображается кнопка Удалить")
    public void checkThereIsDeleteBtnForEachUploadedFile() {
        int numberOfFiles = $$(companyCardNmDocsUploaded.uploadedDocuments).size();
        $(companyCardNmDocsUploaded.containerActual).$$(companyCardNmDocsUploaded.btnsDeleteUploadedDocuments)
                .shouldHave(CollectionCondition.size(numberOfFiles));
    }

    @Step("Проверить, что drag-n-drop содержит правильный текст")
    public void checkTextOfDragNDrop() {
        $(companyCardNmDocsUploaded.dropzoneText)
                .shouldHave(text("Выберите или перетащите до 10 файлов\n" +
                        "В формате *odt, *.docx, *.pdf, *.jpg, *.png\n" +
                        "Максимальный размер 2MB"));
    }

    @Step("Есть кнопки Сохранить и Отменить")
    public void checkExistsBtnSaveCancel() {
        $(companyCardNmDocsUploaded.btnSave).shouldBe(visible);
        $(companyCardNmDocsUploaded.btnCancel).shouldBe(visible);
    }

    @Step("Нажать удалить для первого загруженного файла")
    public void clickDeleteForFirstUploadedFile() {
        $(companyCardNmDocsUploaded.uploadedDocuments).$(companyCardNmDocsUploaded.btnsDeleteUploadedDocuments).click();
    }

    @Step("Редактирование. Удалить все файлы")
    public void deleteAllFiles() {
        int iteration = 0;
        while ($(companyCardNmDocsUploaded.uploadedDocuments).exists()) {
            clickDeleteForFirstUploadedFile();
            PopupQuestion.clickYes();
            iteration++;
            assert iteration < 50;
        }
        checkAttachedCount(0);
    }

    @Step("Проверить отсутствие файла в форме редактирования загруженной версии отчета")
    public void checkFileDoesNotExistInEditMode(String name) {
        $(companyCardNmDocsUploaded.containerActual).$$(companyCardNmDocsUploaded.textsUploadedDocuments)
                .filterBy(exactTextCaseSensitive(name)).shouldHave(CollectionCondition.size(0));
    }

    @Step("Прикрепить файл {file}")
    public void attachFile(File file) {
        $(companyCardNmDocsUploaded.btnChooseFile).click();
        $("body").pressEscape();
        $(companyCardNmDocsUploaded.inputFile).uploadFile(file);
    }

    @Step("Прикрепить файл и проверить наличие прикрепленного файла {file}")
    public void attachFileAndCheckAttached(File file) {
        attachFile(file);
        assertThat("Файл " + file.getName() + " прикреплён в одном экземпляре",
                $$(companyCardNmDocsUploaded.uploadedDocuments).filterBy(text(file.getName())).size(),
                equalTo(1));
//        checkNoErrorInDragNDrop();
    }

    @Step("Нажать кнопку Сохранить")
    public void clickSave() {
        $(companyCardNmDocsUploaded.btnSave).click();
    }

    @Step("Нажать кнопку Отменить")
    public void clickCancel() {
        $(companyCardNmDocsUploaded.btnCancel).click();
    }

    @Step("Отображается сообщение об успешном обновлении файлов")
    public void checkSuccessNotificationFilesUpdated() {
        baseSteps.checkSuccessNotification("Файлы успешно обновлены");
    }

    @Step("Проверить, что drag-n-drop элемент содержит текст ошибки {error}")
    public void checkErrorInDragNDrop(String error) {
        InputMethods.scrollIntoViewWithMargin(companyCardNmDocsUploaded.dropzoneText);
        $(companyCardNmDocsUploaded.dropzoneText).lastChild().shouldHave(exactText(error));
    }

    @Step("Проверить, что drag-n-drop элемент не содержит ошибки")
    public void checkNoErrorInDragNDrop(String error) {
        InputMethods.scrollIntoViewWithMargin(companyCardNmDocsUploaded.dropzoneText);
        $(companyCardNmDocsUploaded.dropzoneText).lastChild().shouldHave(exactText("Максимальный размер 2MB"));
    }

}
