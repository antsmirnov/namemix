package steps;

import io.qameta.allure.Step;
import pageObject.pages.CompaniesPage;
import testData.models.Company;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;
import static testData.Values.cyrillicSmallSet;
import static testData.Values.latinSmallSet;
import static utils.Common.*;
import static utils.InputMethods.clearAndEnterWithCheck;

public class CompanyParamsEditPopupSteps {

    static CompaniesSteps companiesSteps = new CompaniesSteps();

    @Step("Найти компанию и открыть окно настроек")
    public void openParamsForCompany(Company company) {
        companiesSteps.searchCompanies(company);
        $(CompaniesPage.onCompaniesFrame.tableCompanyBtnParams).should(exist).click();
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).shouldBe(visible);
    }

    @Step("Ввод значения поля 'Ставка комиссии'")
    public void inputComissionRate(String comission) {
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).shouldBe(visible).setValue(comission);
    }

    @Step("Ввод значения поля 'Ставка комиссии' с проверкой ввода")
    public void inputWithCheckCommissionRate(String comission) {
        clearAndEnterWithCheck(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate, comission);
    }

    @Step("Ввести ставку компании и нажать отмену ({company})")
    public void testCommissionEditAndCancel(Company company) {
        openParamsForCompany(company);
        String oldCommissionValue = $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).getValue();
        allureLogValue("Комиссия до редактирования", oldCommissionValue);
        inputWithCheckCommissionRate(Integer.toString(randomInt(1, 99)));
        $(CompaniesPage.onCompanyParamsEditPopup.btnCancel).click();
        refresh();
        openParamsForCompany(company);
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).shouldHave(exactValue(oldCommissionValue));
    }

    @Step("Ввести ставку компании и нажать Сохранить ({company})")
    public void testCommissionEditAndSave(Company company) {
        openParamsForCompany(company);

        String oldCommissionValue = $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).getValue();
        String newCommissionValue = String.format("%.2f", randomDouble(0.01, 99.99));
        allureLogValue("Комиссия до редактирования", oldCommissionValue);

        inputWithCheckCommissionRate(newCommissionValue);
        $(CompaniesPage.onCompanyParamsEditPopup.btnSave).click();
        // запрос update может вообще упасть по таймауту, тогда уведомдения не будет
        //$(AnyPage.onAnyPage.notificationsSuccess).waitUntil(exist, 30 * 1000);

        for (int attempts = 0; attempts < 50; attempts++) {
            refresh();
            openParamsForCompany(company);
            if ($(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).getValue().equals(newCommissionValue)) {
                break;
            }
            sleep(2000);
        }
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).shouldHave(exactValue(newCommissionValue));
    }

    @Step("Ввести невалиденое значение комиссии, проверить подсказку {value}")
    public void checkPromptWithInvalidCommission(String value, String prompt, Company company) {
        openParamsForCompany(company);
        inputComissionRate(value);
        $(CompaniesPage.onCompanyParamsEditPopup.btnSave).click();
        $(CompaniesPage.onCompanyParamsEditPopup.promptCommissionRate).should(visible).shouldHave(text(prompt));
    }

    @Step("Ввести значение меньше чем 4.5. Ввод должен быть разрешен")
    public void checkCommissionWithValueLessThen4andHalf(Company company) {
        openParamsForCompany(company);
        inputWithCheckCommissionRate("3");
        $(CompaniesPage.onCompanyParamsEditPopup.btnSave).click();
        $(CompaniesPage.onCompanyParamsEditPopup.promptCommissionRate).shouldNotBe(visible);
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).shouldNotBe(visible);
    }

    @Step("Проверка ввода букв")
    public void checkCommissionLetters(Company company) {
        openParamsForCompany(company);
        String oldValue = $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).getValue();
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).sendKeys(latinSmallSet + cyrillicSmallSet);
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).shouldHave(exactValue(oldValue));
    }

    @Step("Проверка ввода 3 знаков после запятой")
    public void checkCommission3digits(Company company) {
        openParamsForCompany(company);
        inputComissionRate("5.678");
        $(CompaniesPage.onCompanyParamsEditPopup.inputCommissionRate).shouldHave(exactValue("5.67"));
    }

}
