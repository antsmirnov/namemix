package steps;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import pageObject.pages.NewCompanyPage;
import service.Matchers;
import testData.enums.CompanyForm;
import testData.models.Company;
import testData.models.Phone;
import utils.StringGenerator;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static service.CustomAssert.assertThat;
import static steps.elements.InputPhone.enterPhoneRaw;
import static steps.elements.InputPhone.inputWithCheckPhone;
import static testData.enums.CompanyForm.INDIVIDUAL;
import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.StringFunctions.stringWithLengthDescription;

public class NewCompanySteps {

    @Step("Открыть форму создания компании, если необходимо")
    public void openCreationFormIfNeeds(CompanyForm companyForm) {
        if (!(WebDriverRunner.url().contains("client/new"))) {
            open("client/new");
        }
        if (!$(NewCompanyPage.onNewCompanyFrame.dropDownSelectedFormOfRegistration).getText().equals(companyForm.getText())) {
            setCompanyForm(companyForm);
        }
    }

    @Step("Открыть форму создания компании, если необходимо")
    public void openCreationFormIfNeeds() {
        openCreationFormIfNeeds(LEGAL_ENTITY);
    }

    @Step("Проверка хедера формы создания компании")
    public void creationFormDisplayingHeader() {
        $(NewCompanyPage.onNewCompanyFrame.labelHeader).shouldBe(visible).shouldHave(textCaseSensitive("Сведения о заказчике"));
    }

    @Step("Проверка наличия поля выбора формы регистрации")
    public void creationFormDisplayingFormOfRegistration() {
        $(NewCompanyPage.onNewCompanyFrame.labelFormOfRegistration).shouldBe(visible).should(textCaseSensitive("Форма регистрации бизнеса"));
        $(NewCompanyPage.onNewCompanyFrame.dropDownSelectedFormOfRegistration).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода официального имени")
    public void creationFormDisplayingOfficialName() {
        $(NewCompanyPage.onNewCompanyFrame.labelOfficialName).shouldBe(visible).shouldHave(textCaseSensitive("Официальное название компании"));
        $(NewCompanyPage.onNewCompanyFrame.inputOfficialName).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода сокр. имени")
    public void creationFormDisplayingShortName() {
        $(NewCompanyPage.onNewCompanyFrame.labelShortName).shouldBe(visible).shouldHave(textCaseSensitive("Сокращенное название компании"));
        $(NewCompanyPage.onNewCompanyFrame.inputShortName).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода ИНН")
    public void creationFormDisplayingInn() {
        $(NewCompanyPage.onNewCompanyFrame.labelInn).shouldBe(visible).shouldHave(textCaseSensitive("ИНН"));
        $(NewCompanyPage.onNewCompanyFrame.inputInn).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода адреса")
    public void creationformdisplayingAddress() {
        $(NewCompanyPage.onNewCompanyFrame.labelActualAddress).shouldBe(visible).shouldHave(textCaseSensitive("Фактический адрес"));
        $(NewCompanyPage.onNewCompanyFrame.inputActualAddress).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода ФИО представителя")
    public void creationFormDisplayingContactPersonName() {
        $(NewCompanyPage.onNewCompanyFrame.labelContactPersonName).shouldBe(visible).shouldHave(textCaseSensitive("ФИО контактного лица"));
        $(NewCompanyPage.onNewCompanyFrame.inputContactPersonName).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода телефона")
    public void creationFormDisplayingPhone() {
        $(NewCompanyPage.onNewCompanyFrame.labelContactPersonPhone).shouldBe(visible).shouldHave(textCaseSensitive("Телефон контактного лица"));
        $(NewCompanyPage.onNewCompanyFrame.inputContactPersonPhone).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода E-mail")
    public void creationFormDisplayingEmail() {
        $(NewCompanyPage.onNewCompanyFrame.labelContactPersonEmail).shouldBe(visible).shouldHave(textCaseSensitive("E-mail контактного лица"));
        $(NewCompanyPage.onNewCompanyFrame.inputContactPersonEmail).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода промо-кода")
    public void creationFormDisplayingPromo() {
        $(NewCompanyPage.onNewCompanyFrame.labelPromoCode).shouldBe(visible).shouldHave(textCaseSensitive("Промо-код"));
        $(NewCompanyPage.onNewCompanyFrame.inputPromoCode).shouldBe(visible);
        $(NewCompanyPage.onNewCompanyFrame.btnCheckPromoCode).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода категории")
    public void creationFormDisplayingCategory() {
        $(NewCompanyPage.onNewCompanyFrame.labelCategory).shouldBe(visible).shouldHave(textCaseSensitive("Категория"));
        $(NewCompanyPage.onNewCompanyFrame.dropDownBtnCategory).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода комиссии")
    public void creationFormDisplayingComission() {
        $(NewCompanyPage.onNewCompanyFrame.labelCommission).shouldBe(visible).shouldHave(textCaseSensitive("Комиссия"));
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода лимита")
    public void creationFormDisplayingLimit() {
        $(NewCompanyPage.onNewCompanyFrame.labelLimit).shouldBe(visible).shouldHave(textCaseSensitive("Лимит, ₽"));
        $(NewCompanyPage.onNewCompanyFrame.inputLimit).shouldBe(visible).shouldBe(disabled);
    }

    @Step("Проверка наличия флага страхования")
    public void creationFormDisplayingInsurance() {
        $(NewCompanyPage.onNewCompanyFrame.labelInsuranceNeeds).shouldBe(visible).shouldHave(textCaseSensitive("Необходимое страхование"));
        $(NewCompanyPage.onNewCompanyFrame.flagInsuranceNeeds).shouldBe(exist);
    }

    @Step("Проверка наличия флага 'Оплата регистрами'")
    public void creationFormDisplayingPaymentByRegisters() {
        $(NewCompanyPage.onNewCompanyFrame.labelPaymentByRegistries).shouldBe(visible).shouldHave(textCaseSensitive("Оплаты реестрами"));
        $(NewCompanyPage.onNewCompanyFrame.flagPaymentByRegistries).shouldBe(exist);
    }

    @Step("Проверка наличия 'Заказы без обеспечения'")
    public void creationFormDisplayingOrdersWithoutPledge() {
        $(NewCompanyPage.onNewCompanyFrame.flagOrdersWithoutPledge).shouldBe(exist);
        $(NewCompanyPage.onNewCompanyFrame.labelOrdersWithoutPledge).shouldBe(visible).shouldHave(textCaseSensitive("Заказы без обеспечения"));
    }

    @Step("Выбрать тип компании")
    public void setCompanyForm(CompanyForm companyForm) {
        $(NewCompanyPage.onNewCompanyFrame.dropDownBtnFormOfRegistration).click();
        $$(NewCompanyPage.onNewCompanyFrame.optionFormOfRegistration).filterBy(text(companyForm.getText())).get(0).click();
    }

    @Step("Выбрать категорию компании")
    public void setCategory(String category) {
        $(NewCompanyPage.onNewCompanyFrame.dropDownBtnCategory).click();
        SelenideElement option = $$(NewCompanyPage.onNewCompanyFrame.dropDownCategoryOptions)
                .filterBy(text(category)).get(0);
        option.scrollIntoView(false);
        option.click();
    }

    @Step("Ввести официальное название компании: '{name}'")
    public void inputWithCheckOfficialName(String name) {
        $(NewCompanyPage.onNewCompanyFrame.inputOfficialName).setValue(name);
        $(NewCompanyPage.onNewCompanyFrame.inputOfficialName).shouldHave(exactValue(name));
    }

    @Step("Ввести сокращенное название компании: '{name}'")
    public void inputWithCheckShortName(String name) {
        $(NewCompanyPage.onNewCompanyFrame.inputShortName).setValue(name);
        $(NewCompanyPage.onNewCompanyFrame.inputShortName).shouldHave(exactValue(name));
    }

    @Step("Ввести сокращенное название компании: '{inn}'")
    public void inputWithCheckInn(String inn) {
        $(NewCompanyPage.onNewCompanyFrame.inputInn).setValue(inn);
        $(NewCompanyPage.onNewCompanyFrame.inputInn).shouldHave(exactValue(inn));
    }

    @Step("Ввести ФИО ИП: '{name}'")
    public void inputWithCheckFioIp(String name) {
        $(NewCompanyPage.onNewCompanyFrame.inputFioIndividual).setValue(name);
        $(NewCompanyPage.onNewCompanyFrame.inputFioIndividual).shouldHave(exactValue(name));
    }

    @Step("Ввести EIN: '{ein}'")
    public void inputWithCheckEin(String ein) {
        $(NewCompanyPage.onNewCompanyFrame.inputEin).setValue(ein);
        String formattedEin = ein.substring(0,2) + "-" + ein.substring(2);
        $(NewCompanyPage.onNewCompanyFrame.inputEin).shouldHave(exactValue(formattedEin));
    }

    @Step("Ввести адрес: '{address}'")
    public void inputWithCheckActualAddress(String address) {
        $(NewCompanyPage.onNewCompanyFrame.inputActualAddress).setValue(address);
        $(NewCompanyPage.onNewCompanyFrame.inputActualAddress).shouldHave(exactValue(address));
    }

    @Step("Ввести имя контактного лица: '{name}'")
    public void inputWithCheckContactPersonName(String name) {
        $(NewCompanyPage.onNewCompanyFrame.inputContactPersonName).setValue(name);
        $(NewCompanyPage.onNewCompanyFrame.inputContactPersonName).shouldHave(exactValue(name));
    }

    @Step("Ввести e-mail контактного лица: \"{email}\"")
    public void inputWithCheckContactPersonEmail(String email) {
        $(NewCompanyPage.onNewCompanyFrame.inputContactPersonEmail).setValue(email);
        $(NewCompanyPage.onNewCompanyFrame.inputContactPersonEmail).shouldHave(exactValue(email));
    }

    @Step("Ввести телефон: '{phone}'")
    public void inputContactPersonPhoneRaw(String phone) {
        enterPhoneRaw($(NewCompanyPage.onNewCompanyFrame.inputContactPersonPhone), phone);
    }

    @Step("Ввести телефон: '{phone}'")
    public void inputWithCheckContactPersonPhone(Phone phone) {
        inputWithCheckPhone($(NewCompanyPage.onNewCompanyFrame.inputContactPersonPhone), phone);
    }

    @Step("Ввести комиссию: '{commission}'")
    public void inputWithCheckCommission(String commission) {
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).click();
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).sendKeys(commission);
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).sendKeys(Keys.TAB);
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).shouldHave(exactValue(commission));
    }

    @Step("Заполнить поля формы создания компании (Юр. лицо)")
    public void inputCompanyDataLEPart(Company company) {
        inputWithCheckOfficialName(company.getOfficialName());
        inputWithCheckShortName(company.getShortName());
        inputWithCheckInn(company.getInn());
        inputWithCheckActualAddress(company.getAddressActual());
    }

    @Step("Заполнить поля формы создания компании (ИП)")
    public void inputCompanyDataIndividualPart(Company company) {
        inputWithCheckFioIp(company.getFioIp());
        inputWithCheckInn(company.getInn());
        inputWithCheckActualAddress(company.getAddressRegistered());
    }

    @Step("Заполнить поля формы создания компании (Иностранная)")
    public void inputCompanyDataForeignPart(Company company) {
        inputWithCheckOfficialName(company.getOfficialName());
        inputWithCheckShortName(company.getShortName());
        inputWithCheckEin(company.getEin());
        inputWithCheckActualAddress(company.getAddressActual());
    }

    @Step("Заполнить поля формы создания компании данными")
    public void inputCompanyData(Company company) {
        open("client/new");
        setCompanyForm(company.getCompanyForm());

        if (company.getCompanyForm() == LEGAL_ENTITY) inputCompanyDataLEPart(company);
        else if (company.getCompanyForm() == INDIVIDUAL) inputCompanyDataIndividualPart(company);
        else inputCompanyDataForeignPart(company);

        inputWithCheckContactPersonName(company.getContactorFio());
        inputWithCheckContactPersonEmail(company.getContactorEmail());
        setCategory(company.getCategory());
        inputWithCheckCommission(company.getCommissionRate());
    }

    @Step("Нажать кнопку Отмена")
    public void clickCancel() {
        sleep(300);     // "Воркэраунд", чтобы JS успел отработать после ввода значений
        $(NewCompanyPage.onNewCompanyFrame.btnCancel).hover();
        $(NewCompanyPage.onNewCompanyFrame.btnCancel).click();
    }

    @Step("Нажать кнопку Добавить")
    public void clickAdd() {
        sleep(500);     // "Воркэраунд", чтобы JS успел отработать после ввода значений
        $(NewCompanyPage.onNewCompanyFrame.btnAdd).hover();
        sleep(200);
        $(NewCompanyPage.onNewCompanyFrame.btnAdd).click();
    }

    @Step("Официальное название компании. Ввести латиницу")
    public void checkOfficialNameInputLatin() {
        inputWithCheckOfficialName("Latinitsa LTD");
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptOfficialName).shouldNot(exist);
    }
    @Step("Официальное название компании. Ввести менее 10 разрешенных символов")
    public void checkOfficialNameInputShort() {
        inputWithCheckOfficialName("4%#я");
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptOfficialName).shouldHave(text("Минимальная длина строки 5 символов"));
    }
    @Step("Официальное название компании. Ввести более 250 разрешенных символов")
    public void checkOfficialNameInputLong() {
        inputWithCheckOfficialName(stringWithLengthDescription(251, "Официальное имя"));
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptOfficialName).shouldHave(text("Максимальная длина - 250 символов"));
    }

    @Step("Сокращенное название компании. Ввести латиницу")
    public void checkShortNameInputLatin() {
        inputWithCheckShortName("Latinitsa LTD");
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptShortName).shouldNot(exist);
    }
    @Step("Сокращенное название компании. Ввести менее 5 разрешенных символов")
    public void checkShortNameInputShort() {
        inputWithCheckShortName("4\"!я");
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptShortName).shouldHave(text("Минимальная длина строки 5 символов"));
    }
    @Step("Сокращенное название компании. Ввести более 100 разрешенных символов")
    public void checkShortNameInputLong() {
        inputWithCheckShortName(stringWithLengthDescription(251, "Сокращенное имя"));
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptShortName).shouldHave(text("Максимальная длина - 100 символов"));
    }

    @Step("ИНН ввести не цифры")
    public void checkInnInputNotDigits() {
        $(NewCompanyPage.onNewCompanyFrame.inputInn).setValue("Nе Цифры!'");
        $(NewCompanyPage.onNewCompanyFrame.inputInn).shouldHave(exactValue(""));
    }

    @Step("Ввести невалидный ИНН")
    public void checkInnInputInvalidFormat(CompanyForm companyForm) {
        inputWithCheckInn(companyForm == LEGAL_ENTITY ? "1234567890" : "123456789012");
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptInn).shouldHave(text("Неверный формат ИНН"));
    }

    @Step("Ввести ИНН некорректной длины")
    public void checkInnInputBadLength(CompanyForm companyForm) {
        inputWithCheckInn("12345678901");
        clickAdd();
        if (companyForm == LEGAL_ENTITY) {
            $(NewCompanyPage.onNewCompanyFrame.promptInn).shouldHave(text("ИНН может состоять только из 10 цифр"));
        } else {
            $(NewCompanyPage.onNewCompanyFrame.promptInn).shouldHave(text("ИНН может состоять только из 12 цифр"));
        }
    }

    @Step("EIN ввести не цифры")
    public void checkEinInputNonDigits() {
        $(NewCompanyPage.onNewCompanyFrame.inputEin).setValue("Nе Цифры!'");
        $(NewCompanyPage.onNewCompanyFrame.inputEin).shouldHave(exactValue(""));
    }

    @Step("ФИО контактного лица. Ввести более 150 символов")
    public void checkInputContactorFioTooLong() {
        inputWithCheckContactPersonName(stringWithLengthDescription(151, "ФИО"));
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptContractorPersonName).shouldHave(text("Максимальная длина - 150 символов"));
    }

    @Step("Номер телефона контактного лица. Ввести более 10 цифр")
    public void checkInputContactorPhoneShort() {
        inputContactPersonPhoneRaw("+7999123123166");
        assertThat("Лишние цифры номера не были введены",
                $(NewCompanyPage.onNewCompanyFrame.inputContactPersonPhone).getValue(),
                Matchers.isThisPhone(new Phone("+79991231231")));
    }

    @Step("Проверка подсказки ввода для невалидного e-mail {email}")
    public void checkInputEmailInvalid(String email) {
        inputWithCheckContactPersonEmail(email);
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptContractorPersonEmail).shouldHave(text("Введен некорректный email"));
    }

    @Step("Проверка подсказки ввода для слишком длинного e-mail")
    public void checkInputEmailTooLong() {
        StringGenerator generator = new StringGenerator().withLatinLowerCase(true);
        inputWithCheckContactPersonEmail(generator.generate(301) + "@" + generator.generate(16) + ".ru"); // 321 символ
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptContractorPersonEmail).shouldHave(text("Максимальная длина - 320 символов"));
    }
    @Step("E-mail длиной 320 символов должен приниматься")
    public void checkInputEmailLengthValueAlmostTooLong() {
        StringGenerator generator = new StringGenerator().withLatinLowerCase(true);
        inputWithCheckContactPersonEmail(generator.generate(300) + "@" + generator.generate(16) + ".ru"); // 320 символов
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptContractorPersonEmail).shouldNot(exist);
    }

    @Step("Проверка ввода валидной комиссии {value}")
    public void checkInputCommissionValid(String value) {
        inputWithCheckCommission(value);
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptCommission).shouldNot(exist);
    }

    @Step("Вввод ставки комиссии c тремя знаками в дробной части")
    public void checkInputCommissionGT100() {
        inputWithCheckCommission("101");
        clickAdd();
        $(NewCompanyPage.onNewCompanyFrame.promptCommission).shouldHave(exactTextCaseSensitive("Максимальное допустимое значение равно 100"));
    }

    @Step("Вввод ставки комиссии содержащей более чем два знака после запятой")
    public void checkInputCommissionMoreThenTwoFracNumbers() {
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).click();
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).sendKeys("6.1234");
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).sendKeys(Keys.TAB);
        $(NewCompanyPage.onNewCompanyFrame.inputCommission).shouldHave(exactValue("6.12"));
    }
}