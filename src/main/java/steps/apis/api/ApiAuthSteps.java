package steps.apis.api;

import apis.api.Api;
import apis.api.responseModel.AuthResponseModel;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import testData.models.User;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;
import static utils.Api.isExpired;

public class ApiAuthSteps {

    static final int authAttemptsLimit = 10;
    static final int timeIntervalBetweenAttempts = 500; // ms

    @Step("Получение токенов accessToken и refreshToken для пользователя {user}")
    public AuthResponseModel getAuthTokensByLoginPass(User user) {
        int attemptsCount = 0;
        while (true) {
            try {
                return Api.auth.login(user).execute().body();
            } catch (IOException e) {
                System.err.println(e.toString());
                assertThat("Запрос не был выполнен после " + authAttemptsLimit + " попыток",
                        ++attemptsCount,
                        lessThan(authAttemptsLimit));
            }
            Selenide.sleep(timeIntervalBetweenAttempts);
        }
    }

    @Step("Обновление токенов accessToken и refreshToken")
    public AuthResponseModel getAuthTokensByRefreshToken(String refreshToken) {
        int attemptsCount = 0;
        while (true) {
            try {
                return Api.auth.refreshToken("Bearer " + refreshToken).execute().body();
            } catch (IOException e) {
                System.err.println(e.toString());
                assertThat("Запрос не был выполнен после " + authAttemptsLimit + " попыток",
                        ++attemptsCount,
                        lessThan(authAttemptsLimit));
                Selenide.sleep(timeIntervalBetweenAttempts);
            }
        }

    }

    /**
     * Возвращает весь ответ сервера на запрос авторизации для юзера.
     * Кэширует ответ сервера и при необходимости обновляет одним из двух способов
     */
    @Step("Получить access токен (весь AuthResponseModel, обновить если требуется)")
    public AuthResponseModel getAuthForUser(User user) {
        synchronized (Api.tokens) {
            if (Api.tokens.containsKey(user.login)) {
                if (isExpired(Api.tokens.get(user.login).accessTokenExpirationDateTimeUTC)) {
                    if (!isExpired(Api.tokens.get(user.login).refreshTokenExpirationDateTimeUTC)) {
                        Api.tokens.put(user.login, getAuthTokensByRefreshToken(Api.tokens.get(user.login).refreshToken));
                    } else {
                        Api.tokens.put(user.login, getAuthTokensByLoginPass(user));
                    }
                }
            } else {
                Api.tokens.put(user.login, getAuthTokensByLoginPass(user));
            }
            return Api.tokens.get(user.login);
        }
    }

    /**
     * Возвращает accessToken для юзера. Кэширует ответ сервера и при необходимости обновляет одним из двух способов
     */
    @Step("Получить access токен (обновить если требуется)")
    public String getAccessTokenForUser(User user) {
        return getAuthForUser(user).accessToken;
    }

}
