package steps.apis.api;

import apis.api.Api;
import apis.api.requestModel.AddClientRequestModel;
import apis.api.requestModel.ClientsRequestModel;
import apis.api.requestModel.ClientsUsersRequestModel;
import apis.api.responseModel.AddClientResponseModel;
import apis.api.responseModel.ClientsResponseModel;
import apis.api.responseModel.ClientsUsersResponseModel;
import apis.api.responseModel.subResponses.SubResponseClient;
import apis.api.responseModel.subResponses.SubResponseClientUser;
import io.qameta.allure.Step;
import org.testng.Assert;
import retrofit2.Response;
import testData.enums.UserRole;
import testData.models.Company;
import testData.models.User;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.*;
import static service.CustomAssert.assertThat;

/**
 * Класс для работы со списком клиентов (на данный момент - только компаний) через API
 */
public class ApiClientsSteps {
    static ApiAuthSteps apiAuthSteps = new ApiAuthSteps();

    public SubResponseClient[] searchCompanies(String nameSubstringFilter, boolean archivedFilter) {
        try {
            Response<ClientsResponseModel> page = Api.clients.clients(
                    "Bearer " + apiAuthSteps.getAccessTokenForUser(new User(UserRole.WEBADMIN_OF_SITE)),
                    new ClientsRequestModel()
                            .withPageSize(100)
                            .withArchivedFilter(archivedFilter)
                            .withNameSubstringFilter(nameSubstringFilter)
            ).execute();
            assertThat(page.body(), not(nullValue()));
            assertThat(page.raw().code(), equalTo(200));
            return page.body().clients;
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
            return null;
        }
    }

    @Step("Поиск активных компаний '{nameSubstringFilter}'")
    public SubResponseClient[] searchCompaniesActive(String nameSubstringFilter) {
        return searchCompanies(nameSubstringFilter, false);
    }

    @Step("Поиск архивных компаний '{nameSubstringFilter}'")
    public SubResponseClient[] searchCompaniesArchived(String nameSubstringFilter) {
        return searchCompanies(nameSubstringFilter, true);
    }

    @Step("Найти компанию '{nameSubstringFilter}' и получить ее сотрудников")
    public SubResponseClientUser[] getCompanyEmployers(String nameSubstringFilter) {
        SubResponseClient[] companies = searchCompanies(nameSubstringFilter, false);
        assertThat("Чтобы получить сотрудников компании, компания должна существовать", companies.length, greaterThan(0));
        try {
            Response<ClientsUsersResponseModel> response = Api.clients.clientsUsers(
                    "Bearer " + apiAuthSteps.getAccessTokenForUser(new User(UserRole.WEBADMIN_OF_SITE)),
                    new ClientsUsersRequestModel()
                            .withClientId(companies[0].clientId)
                            .withArchiveFilter(false)).execute();
            assertThat(response.body(), not(nullValue()));
            assertThat(response.raw().code(), equalTo(200));
            return response.body().clientUsers;
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
            return null;
        }
    }

    @Step("Создание компании через API")
    public String createCompany(AddClientRequestModel requestModel) {
        try {
            Response<AddClientResponseModel> response = Api.clients.addClient(
                    "Bearer " + apiAuthSteps.getAccessTokenForUser(new User(UserRole.WEBADMIN_OF_SITE)),
                    requestModel).execute();
            assertThat(response.body(), not(nullValue()));
            assertThat(response.raw().code(), equalTo(200));
            try {
                Api.clients.lockedFalse(
                        "Bearer " + apiAuthSteps.getAccessTokenForUser(new User(UserRole.WEBADMIN_OF_SITE)),
                        response.body().guid).execute();
            } catch (Exception e) {
                assertThat(
                        "Запрос к API clients/" + response.body().guid + "/locked/false должен выполниться успешно",
                        e, nullValue());
                return null;
            }
            return response.body().guid;
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
            return null;
        }
    }

    public String createCompany(Company company) {
        return createCompany(new AddClientRequestModel(company));
    }

    /* Может вернуть null если отключены проверки! */
    @Step("Получение clientId компании по имени {name}, поиск среди архивных: {archived}")
    public String getCompanyGuid(String name, boolean archived, boolean failIfDoesNotExist, boolean failIfMoreThenOne) {
        SubResponseClient[] companies = searchCompanies(name, archived);
        if (failIfDoesNotExist) {
            assertThat("Запрос должен вернуть компанию", companies, notNullValue());
            assertThat("Запрос должен вернуть как минимум одну компанию", companies.length, greaterThan(0));
        }
        if (failIfMoreThenOne) {
            // в этом случае возможно стоит удалить ненужные компании или использовать другие имена компаний
            assertThat("Запрос должен вернуть ТОЛЬКО ОДНУ компанию", companies.length, lessThanOrEqualTo(1));
        }

        if (companies != null && companies.length > 0) {
            return companies[0].clientId;
        } else return null;
    }

    public String getNotArchivedCompanyGuid(String name) {
        return getCompanyGuid(name, false, true, true);
    }

    public String getArchivedCompanyGuid(String name) {
        return getCompanyGuid(name, true, true, true);
    }

    @Step("Проверка существования компании")
    public void checkCompanyExists(String name) {
        SubResponseClient[] companies = searchCompaniesActive(name);
        assertThat("Ответ API должен содержать массив клиентов",
                companies, notNullValue());
        assertThat("Компания с таким именем найдена в списке активных (неархивированных) компаний",
                companies.length, greaterThan(0));
    }

}
