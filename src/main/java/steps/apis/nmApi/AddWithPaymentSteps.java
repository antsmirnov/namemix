package steps.apis.nmApi;

import apis.nmapi.requestModel.AddWithPaymentRequestModel;
import io.qameta.allure.Step;
import testData.models.User;

import static org.hamcrest.CoreMatchers.equalTo;
import static service.CustomAssert.assertThat;

public class AddWithPaymentSteps {

    static AuthSteps authSteps = new AuthSteps();

    @Step("order/addWithPayment проверка кода ошибки {errorCode}")
    public void addWithPaymentErrorCheck(User user, AddWithPaymentRequestModel requestModel, String errorCode, String errorMessage) {

        System.err.println("Этот метод временно отключен и не делает ничего");
        assertThat("addWithPayment не должен вызываться! (Временно отключен)", 0, equalTo(1));

//        try {
//            Response<AddWithPaymentResponseModel> response = Api.orders.addWithPayment("Bearer " + apiAuthSteps.getAccessTokenForUser(user),
//                    requestModel).execute();
//            assertThat(response.errorBody(), nullValue());
//            assertThat(response.body(), not(nullValue()));
//            assertThat("Код ошибки должен содержать: " + errorCode, response.body().errorCode, equalTo(errorCode));
//            assertThat("Текст ошибки", response.body().errorMessage, containsString(errorMessage));
//            assertThat("Поле \"success\"", response.body().success, anyOf(equalTo(false), nullValue()));
//            assertThat("Статус ответа: 200", response.raw().code(), (equalTo(200)));
//        } catch (IOException e) {
//        e.printStackTrace();
//        Assert.fail("Запрос к API должен выполниться успешно");
//        }
    }
}
