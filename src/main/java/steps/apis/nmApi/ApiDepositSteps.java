package steps.apis.nmApi;

import apis.nmapi.NmApi;
import apis.nmapi.responseModel.GetInfoDepositBalanceResponseModel;
import io.qameta.allure.Step;
import org.testng.Assert;
import retrofit2.Response;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

public class ApiDepositSteps {

    @Step("API должен вернуть баланс c использованием токена авторизации")
    public void depositBalanceIsAccessible(String token) {
        try {
            Response<GetInfoDepositBalanceResponseModel> response = NmApi.balance.getInfoDepositBalance("Bearer " + token).execute();
            assertThat("Запрос должен выполниться успешно", response.errorBody(), nullValue());
            assertThat("Ответ содержит поле depositAmount", response.body().depositAmount, not(nullValue()));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
        }
    }
}
