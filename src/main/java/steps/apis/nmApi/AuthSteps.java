package steps.apis.nmApi;

import apis.nmapi.NmApi;
import apis.nmapi.responseModel.AuthResponseModel;
import io.qameta.allure.Step;
import org.testng.Assert;
import testData.models.User;

import java.io.IOException;

import static utils.Api.isExpired;

public class AuthSteps {

    @Step("Получение токенов accessToken и refreshToken для пользователя {user}")
    public AuthResponseModel getAuthTokensByLoginPass(User user) {
        try {
            return NmApi.auth.login(user).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Метод получения токена не был выполнен успешно");
            return null;
        }
    }

    @Step("Обновление токенов accessToken и refreshToken")
    public AuthResponseModel getAuthTokensByRefreshToken(String refreshToken) {
        try {
            return NmApi.auth.refreshToken("Bearer " + refreshToken).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Метод получения токена не был выполнен успешно");
            return null;
        }
    }

    /**
     * Возвращает весь ответ сервера на запрос авторизации для юзера.
     * Кэширует ответ сервера и при необходимости обновляет одним из двух способов
     */
    @Step("Получить access токен (весь AuthResponseModel, обновить если требуется)")
    public AuthResponseModel getAuthForUser(User user) {
        synchronized (NmApi.tokens) {
            if (NmApi.tokens.containsKey(user.login)) {
                if (isExpired(NmApi.tokens.get(user.login).accessTokenExpirationDateTimeUTC)) {
                    if (!isExpired(NmApi.tokens.get(user.login).refreshTokenExpirationDateTimeUTC)) {
                        NmApi.tokens.put(user.login, getAuthTokensByRefreshToken(NmApi.tokens.get(user.login).refreshToken));
                    } else {
                        NmApi.tokens.put(user.login, getAuthTokensByLoginPass(user));
                    }
                }
            } else {
                NmApi.tokens.put(user.login, getAuthTokensByLoginPass(user));
            }
            return NmApi.tokens.get(user.login);
        }
    }

    /**
     * Возвращает accessToken для юзера. Кэширует ответ сервера и при необходимости обновляет одним из двух способов
     */
    @Step("Получить access токен (обновить если требуется)")
    public String getAccessTokenForUser(User user) {
        return getAuthForUser(user).accessToken;
    }

}
