package steps.apis.nmApi;

import apis.nmapi.NmApi;
import apis.nmapi.requestModel.ContractInitRequestModel;
import apis.nmapi.responseModel.ContractResponesModel;
import io.qameta.allure.Step;
import org.testng.Assert;
import retrofit2.Response;
import testData.models.User;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.*;
import static service.CustomAssert.assertThat;

public class ContractInitSteps {

    static AuthSteps authSteps = new AuthSteps();

    @Step("Проверка contract/init с кодом ошибки: {expectedErrCode}")
    public void contractInitCheckError(User user, String contractorPhone, String contractorInn,
                                       String expectedErrCode, String expectedErrMessage) {
        try {
            Response<ContractResponesModel> response = NmApi.contract.init("Bearer " + authSteps.getAccessTokenForUser(user),
                    new ContractInitRequestModel(contractorInn, contractorPhone)).execute();
            assertThat(response.errorBody(), nullValue());
            assertThat(response.body(), not(nullValue()));
            assertThat("Код ошибки должен быть: " + expectedErrCode, response.body().errorCode, equalTo(expectedErrCode));
            assertThat("Текст ошибки", response.body().errorMessage, equalTo(expectedErrMessage));
            assertThat("Статус ответа: 200", response.raw().code(), equalTo(200));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
        }
    }

    @Step("Проверка успешного выполнения contract/init")
    public void contractInitSuccess(User user, String contractorPhone, String contractorInn) {
        try {
            Response<ContractResponesModel> response = NmApi.contract.init("Bearer " + authSteps.getAccessTokenForUser(user),
                    new ContractInitRequestModel(contractorInn, contractorPhone)).execute();
            assertThat(response.errorBody(), nullValue());
            assertThat(response.body(), not(nullValue()));
            assertThat("Статус ответа: 200", response.raw().code(), equalTo(200));
            assertThat("Поле 'success' = true", response.body().success, equalTo(true));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
        }
    }
}
