package steps.apis.nmApi;

import apis.nmapi.NmApi;
import apis.nmapi.responseModel.TaxCheckContractorStatusResponseModel;
import db.model.Contractor;
import io.qameta.allure.Step;
import org.testng.Assert;
import retrofit2.Response;
import steps.db.DbSteps;
import testData.models.User;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.comparesEqualTo;
import static service.CustomAssert.assertThat;

public class ContractorStatusSteps {

    public static DbSteps dbSteps = new DbSteps();
    public static AuthSteps authSteps = new AuthSteps();

    @Step("Проверка tax/checkContractorStatus (существующие телефон/инн, все проверки пройдены)")
    public void checkContractorStatusWithValidInnAndPhone(User user) {

        Contractor validContractor = dbSteps.getContractorFullyRegistered();

        try {
            TaxCheckContractorStatusResponseModel response = NmApi.tax.checkContractorStatus(
                    "Bearer " + authSteps.getAccessTokenForUser(user),
                    validContractor.inn,
                    validContractor.phone
            ).execute().body();
            assertThat(response, notNullValue());
            assertThat("Статус должен быть = 'ОК'", response.status, comparesEqualTo("OK"));
            assertThat("Поле 'phone' должно совпадать с параметром запроса 'contractorPhone'", response.phone, equalTo(validContractor.phone));
            assertThat("Поле 'inn' должно совпадать с параметром запроса 'contractorInn'", response.inn, equalTo(validContractor.inn));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
        }
    }

    @Step("Проверка tax/checkContractorStatus с кодом ошибки: {expectedErrCode}")
    public void checkContractorStatusError(User user, String contractorPhone, String contractorInn, String expectedErrCode, String expectedErrMessage) {
        try {
            Response<TaxCheckContractorStatusResponseModel> response = NmApi.tax.checkContractorStatus(
                    "Bearer " + authSteps.getAccessTokenForUser(user),
                    contractorInn,
                    contractorPhone
            ).execute();
            assertThat(response.errorBody(), nullValue());
            assertThat(response.body(), not(nullValue()));
            assertThat("Код ошибки должен быть: " + expectedErrCode, response.body().errorCode, equalTo(expectedErrCode));
            assertThat("Текст ошибки", response.body().errorMessage, equalTo(expectedErrMessage));
            assertThat("Поле inn отсутствует", response.body().inn, nullValue());
            assertThat("Поле phone отсутствует", response.body().phone, nullValue());
            assertThat("Поле patronymic отсутствует", response.body().patronymic, nullValue());
            assertThat("Поле lastName отсутствует", response.body().lastName, nullValue());
            assertThat("Поле firstName отсутствует", response.body().firstName, nullValue());
            assertThat("Поле status != 'OK'", response.body().status, anyOf(nullValue(), equalTo("Error")));
            assertThat("Статус ответа: 200", response.raw().code(), equalTo(200));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Запрос к API должен выполниться успешно");
        }
    }

}
