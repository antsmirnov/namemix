package steps.db;

import db.Queries;
import db.model.Contractor;
import db.model.Phone;
import io.qameta.allure.Step;

import java.util.List;

import static db.Db.query;
import static org.hamcrest.CoreMatchers.equalTo;
import static service.CustomAssert.assertThat;
import static utils.Common.allureLogValue;

public class DbSteps {
    @Step("Проверить, что нет исполнителей с таким номером телефона")
    public void checkContractorsWithPhoneNotExist(String phone) {
        List<Phone> phones = query(Queries.class, query -> query.contractorPhonesLike(phone));
        assertThat("Список найденных телефонов должен быть пустым", phones.size(), equalTo(0));
    }

    @Step("Получить исполнителя прошедшего полную регистрацию")
    public Contractor getContractorFullyRegistered() {
        return query(Queries.class, query -> query.fullRegisteredContractors().get(0));
    }

    @Step("Получить исполнителя не прошедшего полную регистрацию")
    public Contractor getContractorNotFullyRegistered() {
        return query(Queries.class, query -> query.notFullRegisteredContractors().get(0));
    }

    @Step("Получить исполнителя не зарегистрированного в ФНС")
    public Contractor getContractorNotRegisteredInFns() {
        return query(Queries.class, query -> query.notRegistredInFnsContractors().get(0));
    }

    @Step("Удаление записей из таблицы {tableName} для clientId: {clientId}")
    public void deleteRecordsByClientId(String tableName, String clientId) {
        try {
            int count = query(Queries.class, query -> query.deleteObjects(tableName, clientId));
            allureLogValue(tableName + ". Количество уделенных строк", Integer.toString(count));
        } catch (Exception e) {
            System.err.println("clientId: " + clientId);
            System.err.println(e.toString());
            assertThat(
                    "Ошибка удаления компании из базы (таблица: " + tableName + ", clientId: " + clientId + ")\n\n" + e.toString(),
                    2 + 2, equalTo(5));
        }
    }

    @Step("Удаление компании {clientId}")
    public void deleteCompany(String clientId) {
        deleteRecordsByClientId("FavouriteContractors", clientId);
        deleteRecordsByClientId("Documents", clientId);
        deleteRecordsByClientId("ClientDepositHistory", clientId);
        deleteRecordsByClientId("ProjectUser", clientId);
        deleteRecordsByClientId("ProjectObjectUser", clientId);
        deleteRecordsByClientId("Objects", clientId);
        deleteRecordsByClientId("ContractHistory", clientId);
        deleteRecordsByClientId("ClientUsers", clientId);
        deleteRecordsByClientId("Project", clientId);
        deleteRecordsByClientId("Clients", clientId);
        // TODO: добавить проверку, что компания удалена
    }
}
