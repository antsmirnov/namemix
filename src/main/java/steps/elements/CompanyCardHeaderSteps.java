package steps.elements;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import pageObject.pages.CompanyCardPage;
import steps.BaseSteps;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;

public class CompanyCardHeaderSteps {
    BaseSteps baseSteps = new BaseSteps();

    @Step("Кнопки отменить и сохранить присутствуют")
    public void headerButtonsExists() {
        startEdit();
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderCancelEdit).shouldBe(visible);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderApplyEdit).shouldBe(visible);
    }

    @Step("Начать редактирование")
    public void startEdit() {
        if (!$(CompanyCardPage.onCompanyCardFrame.btnHeaderEdit).isDisplayed()) refresh();
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderEdit).click();
    }

    @Step("Проверить подсказку ввода {input}/{promptText}")
    public void checkPromptForInput(String input, String promptText) {
        startEdit();
        if (input.length() == 0) {
            $(CompanyCardPage.onCompanyCardFrame.inputHeader).sendKeys(Keys.CONTROL, "a");
            $(CompanyCardPage.onCompanyCardFrame.inputHeader).sendKeys(Keys.DELETE);
        } else {
            $(CompanyCardPage.onCompanyCardFrame.inputHeader).setValue(input);
        }
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderApplyEdit).click();
        $(CompanyCardPage.onCompanyCardFrame.promptHeader).shouldBe(visible).shouldHave(exactTextCaseSensitive(promptText));
    }

    @Step("Проверка, что поле заголовка обязательное")
    public void headerMandatory() {
        checkPromptForInput("", "Обязательное поле");
    }

    @Step("Не в режиме редактирования")
    public void checkNotInEditMode() {
        $(CompanyCardPage.onCompanyCardFrame.inputHeader).shouldNotBe(visible);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderApplyEdit).shouldNotBe(visible);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderCancelEdit).shouldNotBe(visible);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderEdit).shouldBe(visible);
    }

    @Step("В режиме редактирования")
    public void checkInEditMode() {
        $(CompanyCardPage.onCompanyCardFrame.inputHeader).shouldBe(visible);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderApplyEdit).shouldBe(visible);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderCancelEdit).shouldBe(visible);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderEdit).shouldNotBe(visible);
    }

    @Step("Изменить хедер (имя компании) на {name} с проверкой")
    public void headerSaveAndCheck(String name) {
        startEdit();
        setValueToHeaderInput(name);
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderApplyEdit).click();
        $(CompanyCardPage.onCompanyCardFrame.promptHeader).shouldNotBe(visible, Duration.ofMillis(500));
        checkNotInEditMode();
        baseSteps.checkSuccessNotification("Компания успешно отредактирована");
        checkCompanyNameBeforeAndAfterRefresh(name);
    }

    @Step("Проверка текста хедера до и после рефреша страницы")
    public void checkCompanyNameBeforeAndAfterRefresh(String name) {
        $(CompanyCardPage.onCompanyCardFrame.textHeader).shouldHave(exactOwnText(name));
        Selenide.refresh();
        $(CompanyCardPage.onCompanyCardFrame.textHeader).shouldHave(exactOwnText(name));
    }

    public void setValueToHeaderInput(String name) {
        $(CompanyCardPage.onCompanyCardFrame.inputHeader).setValue(name);
    }

    public void btnCancelClick() {
        $(CompanyCardPage.onCompanyCardFrame.btnHeaderCancelEdit).click();
    }

}
