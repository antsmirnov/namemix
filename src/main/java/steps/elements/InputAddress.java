package steps.elements;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;
import static utils.InputMethods.clearAndEnterWithCheck;

public class InputAddress {

    @Step("Ввести адрес c проверкой ввода '{value}'")
    static public void enterAddressWithCheck(SelenideElement element, Object value) {
        clearAndEnterWithCheck(element, value);
        element.sendKeys(Keys.TAB);
    }
    @Step("Ввести адрес c проверкой ввода '{value}'")
    static public void enterAddressWithCheck(By locator, Object value) {
        clearAndEnterWithCheck(locator, value);
        $(locator).sendKeys(Keys.TAB);
    }
}
