package steps.elements;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import utils.InputMethods;

import static com.codeborne.selenide.Selenide.$;
import static org.hamcrest.Matchers.hasItem;
import static service.CustomAssert.assertThat;

public class InputDropDownList {

    public static void openDropDownListIfNeeds(By dropDownListInput) {
        if (!$(dropDownListInput).$("div[role='option']").isDisplayed()) {
            InputMethods.scrollIntoViewWithMargin(dropDownListInput);
            $(dropDownListInput).$("i").click();
        }
    }

    /**
     * Выбирает из выпадающего списка значение optionText, если нет значения, то падает
     *
     * @param dropDownListInput - некий контейнер, внутри которого функция ожидает найти элементы DDL
     * @param exactText         текст для выбора элемента
     */
    @Step("Выбрать в впадающем списке значение {exactText}")
    public static void setOptionWithCheck(By dropDownListInput, String exactText) {
        openDropDownListIfNeeds(dropDownListInput);
        assertThat("Невозможно выбрать значение 'optionText' из выпадающего списка: оно отсутствует",
                $(dropDownListInput).$$("div[role='option']").texts(), hasItem(exactText));
        $(dropDownListInput).$$("div[role='option']").filterBy(Condition.exactText(exactText)).get(0).scrollIntoView(false).click();
        $(dropDownListInput).$(".text").shouldHave(Condition.exactText(exactText));
    }

    @Step("Проверить наличие значения {exactText} в выпадающем списке")
    public static void checkOptionExists(By dropDownListInput, String exactText) {
        openDropDownListIfNeeds(dropDownListInput);
        assertThat("Значение " + exactText + " отсутствует в списке",
                $(dropDownListInput).$$("div[role='option']").texts(), hasItem(exactText));
    }
}
