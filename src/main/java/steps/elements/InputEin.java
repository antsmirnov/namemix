package steps.elements;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import utils.InputMethods;

import static com.codeborne.selenide.Selenide.$;

public class InputEin {

    public static void inputWithCheck(By inputLocator, String inputText, String expectedText) {
        InputMethods.clearWithCheck(inputLocator);
        $(inputLocator).setValue(inputText);
        $(inputLocator).shouldHave(Condition.exactValue(expectedText));
    }

    public static void inputWithCheck(By inputLocator, String inputText) {
        inputWithCheck(inputLocator, inputText, inputText.substring(0, 2) + "-" + inputText.substring(2));
    }
}
