package steps.elements;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import service.Matchers;
import testData.Values;
import testData.models.Phone;

import static com.codeborne.selenide.Selenide.$;
import static org.hamcrest.CoreMatchers.equalTo;
import static service.CustomAssert.assertThat;
import static service.Matchers.isEmptyPhoneValue;
import static testData.enums.PhoneFormat.FULL_WITH_PLUS;
import static utils.InputMethods.clearMaskedInput;
import static utils.InputMethods.enterValueIntoMaskedInput;

public class InputPhone {

    public static void enterPhoneRaw(SelenideElement input, String value) {
        enterValueIntoMaskedInput(input, value);
    }

    public static void enterPhoneRaw(By locator, String value) {
        enterPhoneRaw($(locator), value);
    }

    @Step("Ввод телефона с проверкой правильности ввода")
    public static void inputWithCheckPhone(SelenideElement input, Phone phone) {
        clearMaskedInput(input);
        enterPhoneRaw(input, phone.getNumber(FULL_WITH_PLUS));
        assertThat("Телефон был введен в поле", input.getValue(), Matchers.isThisPhone(phone));
    }

    @Step("Ввод телефона с проверкой правильности ввода")
    public static void inputWithCheckPhone(By locator, Phone phone) {
        clearMaskedInput($(locator));
        enterPhoneRaw($(locator), phone.getNumber(FULL_WITH_PLUS));
        assertThat("Телефон был введен в поле", $(locator).getValue(), Matchers.isThisPhone(phone));
    }

    @Step("Ввод с проверкой. Введено: '{inputText}'. Поле должно содержать после ввода: '{expectedText}'")
    public static void enterPhoneWithExpectedResultCheck(SelenideElement element, String inputText, String expectedText) {
        element.scrollIntoView(true);
        enterPhoneRaw(element, inputText);
        assertThat("Поле должно было содержать " + expectedText, element.getValue(), equalTo(expectedText));
    }

    @Step("Ввод с проверкой. Введено: '{inputText}'. Поле должно содержать после ввода: '{expectedText}'")
    public static void enterPhoneWithExpectedResultCheck(By locator, String inputText, String expectedText) {
        enterPhoneWithExpectedResultCheck($(locator), inputText, expectedText);
    }

    @Step("Очистка поля и ввод с проверкой")
    public static void clearAndEnterPhoneWithExpectedResultCheck(By locator, String inputText, String expectedText) {
        clearPhoneWithCheck($(locator));
        enterPhoneWithExpectedResultCheck($(locator), inputText, expectedText);
    }

    @Step("Очистка поля ввода телефона с проверкой")
    public static void clearPhoneWithCheck(SelenideElement element) {
        clearMaskedInput(element);
        assertThat("Поле ввода телефона должно быть пустым", element.getValue(), isEmptyPhoneValue());
    }
    @Step("Очистка поля ввода телефона с проверкой")
    public static void clearPhoneWithCheck(By locator) {
        clearPhoneWithCheck($(locator));
    }

    /* Валидация поля телефона */
    @Step("Разрешен ввод только цифр")
    public static void checkOnlyDigitsAllowed(By input) {
        clearMaskedInput(input);
        enterPhoneRaw(input, Values.nonDigitsNonSpacesSmallSet + " ");
        assertThat("Разрешен ввод только цифр", $(input).getValue(), Matchers.isEmptyPhoneValue());
    }

    @Step("Можно ввести только 10 символов")
    public static void checkOnly10digitsAllowed(By input) {
        clearMaskedInput(input);
        enterPhoneRaw(input, "12345678901234567890");
        assertThat("Можно ввести только 10 символов", $(input).getValue(),
                Matchers.isThisPhone(new Phone("+71234567890")));
    }

    @Step("Можно ввести только 11 символов")
    public static void checkOnly11digitsAllowed(By input) {
        clearMaskedInput(input);
        enterPhoneRaw(input, "12345678901234567890");
        assertThat("Можно ввести только 10 символов", $(input).getValue(),
                Matchers.isThisPhone(new Phone("+12345678901")));
    }

    @Step("При установке курсора отображается маска")
    public static void checkThereIsMask(By input, boolean elevenDigits) {
        clearMaskedInput(input);
        if (elevenDigits)
            assertThat("При установке курсора отображается маска", $(input).getValue(), Matchers.isPhoneMask11Digits());
        else
            assertThat("При установке курсора отображается маска", $(input).getValue(), Matchers.isPhoneMask10Digits());
    }

    public static void checkThereIsMask(By input) {
        checkThereIsMask(input, false);
    }

    /**
     * @param input        поле для ввода телефона
     * @param elevenDigits true - валидация поля, которое позволяет вводить 11 цифр, инача 10
     */
    @Step("Валидация поля ввода номера телефона")
    public static void inputPhoneValidate(By input, boolean elevenDigits) {
        checkOnlyDigitsAllowed(input);
        if (elevenDigits) {
            checkOnly11digitsAllowed(input);
            checkThereIsMask(input, true);
        } else {
            checkOnly10digitsAllowed(input);
            checkThereIsMask(input);
        }
    }

    public static void inputPhoneValidate(By input) {
        inputPhoneValidate(input, false);
    }

}
