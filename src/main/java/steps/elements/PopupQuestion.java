package steps.elements;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import pageObject.pages.AnyPage;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class PopupQuestion {
    @Step("Текст должен быть '{question}'")
    static public void checkPopupText(String question) {
        $(AnyPage.onYesNoQuestionPopup.textQuestion).shouldHave(Condition.exactTextCaseSensitive(question));
    }

    @Step("Нажать Да")
    static public void clickYes() { $(AnyPage.onYesNoQuestionPopup.btnYes).click(); }

    @Step("Нажать Нет")
    static public void clickNo() {
        $(AnyPage.onYesNoQuestionPopup.btnYes).click();
    }

    @Step("Отображается модальное окно с кнопками Да и Нет")
    static public void isDisplayed() {
        $(AnyPage.onYesNoQuestionPopup.btnYes).shouldBe(visible);
        $(AnyPage.onYesNoQuestionPopup.btnNo).shouldBe(visible);
        $(AnyPage.onYesNoQuestionPopup.textQuestion).shouldBe(visible);
    }

}
