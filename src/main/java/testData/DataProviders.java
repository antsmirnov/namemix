package testData;

import org.testng.annotations.DataProvider;

import static testData.Values.*;

public class DataProviders {

    @DataProvider(name = "emailsValid")
    public static Object[] emailsValid() {
        return validEmails;
    }

    @DataProvider(name = "emailsInvalid")
    public static Object[] emailsInvalid() {
        return invalidEmails;
    }

    @DataProvider(name = "nonDigitsNonSpacesSmallSet")
    public static Object[] nonDigitsNonSpacesSmallSet() {
        return nonDigitsNonSpacesSmallSet.chars().mapToObj(c -> Character.toString((char) c)).toArray(String[]::new);
    }

    @DataProvider(name = "charsNotAllowedInFioShortSet")
    public static Object[] charsNotAllowedInFioShortSet() {
        return (digitsSmallSet + latinSmallSet + keyboardSpecialSymbolsSmallSet.replaceAll("-", "")).chars().mapToObj(c -> Character.toString((char) c)).toArray(String[]::new);
    }

    @DataProvider(name = "charsNotAllowedInNonRussianFioShortSet")
    public static Object[] charsNotAllowedInNonRussianFioShortSet() {
        return (digitsSmallSet + keyboardSpecialSymbolsSmallSet.replaceAll("-", "")).chars().mapToObj(c -> Character.toString((char) c)).toArray(String[]::new);
    }

    @DataProvider(name = "nonDigitsNonLatinSmallSet")
    public static Object[] nonDigitsNonLatinSmallSet() {
        return (" " + cyrillicSmallSet + keyboardSpecialSymbolsSmallSet.replaceAll("-", "")).chars().mapToObj(c -> Character.toString((char) c)).toArray(String[]::new);
    }

}
