package testData;

public class Values {
    public static String latinAlphabetLC = "abcdefghijklmnopqrstuvwxyz";
    public static String latinAlphabetUC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static String latinAlphabet = latinAlphabetUC + latinAlphabetLC;
    public static String latinSmallSet = "AZaz";

    public static String cyrillicAlphabetLC = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    public static String cyrillicAlphabetUC = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    public static String cyrillicAlphabet = cyrillicAlphabetUC + cyrillicAlphabetLC;
    public static String cyrillicSmallSet = "АЯаяЁё";

    public static String keyboardSpecialSymbols = "~`!@#№£€$¢¥§%°^&*()-_+={}[]|\\/:;\"'<>,.";
    public static String keyboardSpecialSymbolsSmallSet = "~!#№()-\\/\"',.";

    // Значения для валидации в случаях, когда §°[]<>£€¢¥\ не принимаются, но это не баг
    public static String keyboardSpecialSymbolsFiltered = "~`!@#№$%^&*()-_+={}|/:;\"',.";
    public static String keyboardSpecialSymbolsFilteredSmallSet = "~!#№()-/\"',.";

    public static String digits = "0123456789";
    public static String digitsSmallSet = "019";

    public static String nonDigitsBigSet = latinAlphabet + cyrillicAlphabet + keyboardSpecialSymbols + " ";
    public static String nonDigitsSmallSet = latinSmallSet + cyrillicSmallSet + keyboardSpecialSymbolsSmallSet + " ";
    public static String nonDigitsNonSpacesSmallSet = latinSmallSet + cyrillicSmallSet + keyboardSpecialSymbolsSmallSet;

    public static String[] validEmails = new String[]{
            latinAlphabetLC + "@Te5t.com",
            latinAlphabetUC + "@t3s7.rU",
            "1234567890@Te5t.com",
            "!#$%&‘*+—/=?^_`{|}~" + "@Te5t.com",
            "t.e.s.t@Te5t.com"
    };

    public static String[] invalidEmails = new String[]{
            "user@domainWithoutDots",
            "sobakiNetDomain.ru",
            "pro bel@domain.ru",
            "user@pro bel.ru",
            "@domain.ru",
            "user@",
            "user@domain.ru ",
            "user@domain.ru  ",
            " user@domain.ru",
            "  user@domain.ru",
            "us..er@domain.ru"
    };

}
