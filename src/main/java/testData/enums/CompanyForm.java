package testData.enums;

public enum CompanyForm {
    LEGAL_ENTITY("Юридическое лицо"),
    INDIVIDUAL("Индивидуальный предприниматель"),
    FOREIGN("Иностранная организация");

    private final String text;

    CompanyForm(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
