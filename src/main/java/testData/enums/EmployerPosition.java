package testData.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum EmployerPosition {
    EMPTY(""),
    MAIN_ACCOUNTANT("Главный бухгалтер"),
    LEADER("Руководитель"),
    MANAGER("Менеджер");

    @Getter
    private String value;
}
