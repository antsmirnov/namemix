package testData.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum EmployerRole {
    ADMIN_OF_COMPANY("Администратор компании"),
    COORDINATOR_OF_COMPANY("Координатор компании"),
    MANAGER_OF_PROJECT("Менеджер проекта"),
    MANAGER_OF_OBJECT("Менеджер объекта");

    @Getter private String value;
}
