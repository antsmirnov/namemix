package testData.enums;

public enum PhoneFormat {
    NORMAL_FORM,
    FULL_WITH_PLUS,
    FULL_BUT_WITHOUT_PLUS,
    FULL_WITH_8,
    LAST_TEN_DIGITS
}
