package testData.enums;

public enum UserRole {
    WEBADMIN_OF_SITE,
    WEBADMIN_OF_COMPANY
}
