package testData.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.With;
import testData.enums.CompanyForm;
import utils.StringGenerator;

import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.Common.randomInt;

@Getter
@AllArgsConstructor
@With
public class BankRequisites {
    private final CompanyForm companyForm;

    private String bank;
    private String bankAddress;
    private String bik;
    private String checkAccount;
    private String corrAccount;

    /* Для иностранной компании: */
    private String swift;
    private String accountNumber;
    private String achRoutingNumber;

    public BankRequisites(CompanyForm form) {
        this.companyForm = form;
        switch (form) {
            case LEGAL_ENTITY:
            case INDIVIDUAL:
                this.bank = "РусКэшБанкъ";
                this.bankAddress = "633266, Новосибирская обл, Ордынский р-н, поселок Петровский, ул Матрасовых";
                this.bik = "044525225";
                this.checkAccount = "40702810638050013199";
                this.corrAccount = "30101810400000000225";
                break;
            case FOREIGN:
                this.bank = "Swiss National Bank";
                this.bankAddress = "Börsenstrasse 15, Zurich, 8022, Switzerland";
                this.swift = new StringGenerator().withLatin(false).withDigits(true).generate(randomInt(8, 11));
                this.accountNumber = new StringGenerator().withLatin(false).withDigits(true).generate(randomInt(8, 34));
                this.achRoutingNumber = new StringGenerator().withLatin(false).withDigits(true).generate(9);
                break;
        }
    }

    public BankRequisites() {
        this(LEGAL_ENTITY);
    }
}
