package testData.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.With;
import testData.enums.CompanyForm;
import utils.StringFunctions;

import java.util.Random;

import static testData.enums.CompanyForm.INDIVIDUAL;
import static testData.enums.CompanyForm.LEGAL_ENTITY;

@SuppressWarnings("FieldMayBeFinal")
@Getter
@AllArgsConstructor
@With
public class Company {
    /* clientId - утилитарное поле для ускорения теста. при создании компании в данное поле сохраняется guid компании,
     который в дальнейшем может быть использован для ускорения теста
     (например: открытие страницы компании по прямой ссылке) */
    private String clientId;

    private String shortName;                   // Юр. лицо или Иностранная компания

    private String officialName;                // ЮЛ, ИнК
    private String fioIp;                       // ИП
    private CompanyForm companyForm;

    private String contactorFio;
    private Phone contactorPhone;
    private String contactorEmail;

    private String addressActual;               // ЮЛ, ИнК
    private String addressRegistered;           // ЮЛ, ИнК, ИП (адрес регистрации)

    private Phone phoneOfCompanyOrIp;
    private String emailOfCompanyOrIp;

    private String ogrn;                        // ЮЛ
    private String ogrnIp;                      // ИП
    private String kpp;                         // ЮЛ
    private String inn;                         // ЮЛ, ИП
    private String ein;                         // ИнК

    private String commissionRate;
    private String category;

    private Boolean insuranceAvailable;
    private Boolean registryPaymentsAvailable;
    private Boolean ordersUnsecured;
    private Double ordersLimit;

    public Company(CompanyForm companyForm) {
        String randomIndex = String.format("%06d", new Random().nextInt(1000_000)); // 000000 .. 999999
        this.clientId = null;
        this.companyForm = companyForm;

        switch (companyForm) {
            case LEGAL_ENTITY:
                this.officialName = "Валидное имя компании " + randomIndex;
                this.shortName = "Валидное сокращенное имя " + randomIndex;
                this.ogrn = "";
                this.kpp = "";
                this.inn = "3799918422";
                this.ein = null;
                this.fioIp = null;
                this.addressActual = "127006, г Москва, поселок Толстопальцево, ул Пушкина, д 10 стр 1";
                this.addressRegistered = "";
                break;
            case INDIVIDUAL:
                this.officialName = null;
                this.shortName = null;
                this.inn = "666352475530";
                this.ein = null;
                this.fioIp = StringFunctions.randomFioWithDash();
                this.addressRegistered = "127006, г Москва, поселок Толстопальцево, ул Пушкина, д 10 стр 1";
                break;
            case FOREIGN:
                this.officialName = "Valid Company Name LTD " + randomIndex;
                this.shortName = "Valid short name " + randomIndex;
                this.inn = null;
                this.ein = "123456789";
                this.fioIp = null;
                this.addressActual = "127006, г Москва, поселок Толстопальцево, ул Пушкина, д 10 стр 1";
                this.addressRegistered = "";
                break;
        }

        this.contactorFio = "Валиднов Валид Валидович";
        this.contactorPhone = new Phone("+70051013510");
        this.contactorEmail = "valid@email.ru";
        this.commissionRate = "4.67";
        this.category = "Аренда";

        this.insuranceAvailable = false;
        this.registryPaymentsAvailable = false;
        this.ordersUnsecured = false;
        this.ordersLimit = null;
    }

    public Company() {
        this(LEGAL_ENTITY); // Юридическое лицо по-дефолту
    }

    public String toString() {
        switch (companyForm) {
            case INDIVIDUAL:
                return "ИП " + fioIp;
            case FOREIGN:
            case LEGAL_ENTITY:
            default:
                return companyForm.getText() + "<" + shortName + ">";
        }
    }

    public String getNameForSearch() {
        return (companyForm == INDIVIDUAL) ? fioIp : shortName;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
