package testData.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.With;
import testData.enums.EmployerPosition;
import testData.enums.EmployerRole;
import utils.Common;
import utils.StringGenerator;

import static testData.enums.EmployerPosition.MANAGER;
import static testData.enums.EmployerRole.MANAGER_OF_OBJECT;
import static utils.StringFunctions.randomEmail;
import static utils.StringFunctions.randomFirstName;

@Getter
@AllArgsConstructor
@With
public class Employer {
    private String firstName;
    private String secondName;
    private String patronymic;
    private String snils;
    private String inn;
    private EmployerPosition position;
    private Phone phone;
    private String email;
    private EmployerRole role;
    private String password;

    public Employer() {
        StringGenerator enStrGen = new StringGenerator();
        this.secondName = "Автоматов";
        this.firstName = randomFirstName(false);
        this.patronymic = "Де-фи-со-вич";
        this.inn = "566500431248";
        this.snils = "55629146717";
        this.position = MANAGER;
        this.phone = new Phone("+71234567890");
        this.email = randomEmail(Common.randomInt(10, 30));
        this.role = MANAGER_OF_OBJECT;
        this.password = "Qwerty123!";
    }

    public String getFio() {
        return secondName + " " + firstName + " " + patronymic;
    }

    @Override
    public String toString() {
        return getFio();
    }
}
