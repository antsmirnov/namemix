package testData.models;

import testData.enums.PhoneFormat;

public class Phone {

    private final String fullNumber;

    public Phone(String number) {
        this.fullNumber = number.trim();
    }

    /*  +7xxxxxxxxxx
        8xxxxxxxxxxx
         xxxxxxxxxxx
     */
    public static String normalise(String phone) {
        phone = phone.replaceAll("[^0-9\\+]", "");
        if (phone.length() < 10 || phone.length() > 12) {
            return phone;
        } else if (phone.startsWith("+")) {
            return phone;
        } else if (phone.length() == 10) {
            return "+7" + phone;
        } else {
            return "+7" + phone.substring(1);
        }
    }

    public static String convertTo(String phone, PhoneFormat format) {
        String normalForm = normalise(phone);
        switch (format) {
            case FULL_WITH_8:
                return "8" + normalForm.substring(2);
            case NORMAL_FORM:
            case FULL_WITH_PLUS:
                return normalForm;
            case FULL_BUT_WITHOUT_PLUS:
                return normalForm.substring(1);
            case LAST_TEN_DIGITS:
                return normalForm.substring(2);
            default:
                return "Unknown tel. format";
        }
    }

    public String getFullNumberRaw() {
        return fullNumber;
    }

    public String getNumber(PhoneFormat format) {
        return convertTo(fullNumber, format);
    }

}
