package testData.models;

import org.aeonbits.owner.ConfigFactory;
import service.TestsConfig;
import testData.enums.UserRole;

public class User {
    public static TestsConfig testsConfig = ConfigFactory.create(TestsConfig.class);

    public final String login;
    public final String password;

    public User(UserRole userRole) {
        switch (userRole) {
            case WEBADMIN_OF_SITE:
                this.login = testsConfig.webAdminSiteLogin();
                this.password = testsConfig.webAdminSitePassword();
                break;
            case WEBADMIN_OF_COMPANY:
                this.login = testsConfig.webAdminCompanyLogin();
                this.password = testsConfig.webAdminCompanyPassword();
                break;
            default:
                this.login = "";
                this.password = "";
        }
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + login + '\'' + '}';
    }
}
