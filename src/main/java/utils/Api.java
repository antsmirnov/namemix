package utils;

import lombok.SneakyThrows;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class Api {

    /**
     * Проверяет, истекла ли (или близка к этому) дата, переданная в строке
     *
     * @param expirationDateStrUtc - строка вида 2020-12-21T11:20:28.453806101
     */
    @SneakyThrows
    public static boolean isExpired(String expirationDateStrUtc) {
        expirationDateStrUtc = expirationDateStrUtc.substring(0, expirationDateStrUtc.lastIndexOf('.'));
        Date expirationDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expirationDateStrUtc);
        Date now = Date.from(Instant.now());
        int delta = 1000 * 60 * 10; // 10 min
        return !now.after(expirationDate) && (expirationDate.getTime() - now.getTime()) >= delta;
    }

}
