package utils;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import pageObject.pages.AnyPage;

import java.util.Objects;

import static com.codeborne.selenide.Condition.exactValue;
import static com.codeborne.selenide.Selenide.$;

public class InputMethods {

    static public void jsClick(By by) {
        Selenide.executeJavaScript("arguments[0].click();", $(by));
    }

    static public void jsSetValue(By by, String val) {
        Selenide.executeJavaScript(String.format("arguments[0].value = '%s';", val), $(by));
    }

    static public void enterReliable(SelenideElement input, String value) {
        int retries = 0;
        input.clear();
        while (!(Objects.equals(input.getValue(), value)) && retries < 20) {
            retries++;
            input.clear();
            enterCharByCharWithEnd(input, value);
        }
    }

    static public void enterCharByCharWithEnd(SelenideElement input, String value) {
        for (char c : value.toCharArray()) {
            input.sendKeys(String.valueOf(c));
            input.sendKeys(Keys.END);
        }
    }

    static public void enterValueIntoMaskedInput(SelenideElement input, String value) {
        input.click();
        input.sendKeys(Keys.HOME);
        input.sendKeys(value);
    }

    static public void clearMaskedInput(SelenideElement input) {
        input.click();
        input.sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.DELETE, Keys.HOME);
    }

    static public void clearMaskedInput(By input) {
        clearMaskedInput($(input));
    }

    /**
     * Вводит значение в элемент и сразу проверяет правильность ввода
     */
    @Step("Ввод '{value}' с проверкой правильности ввода")
    static public void enterWithCheck(By locator, Object value) {
        String strVal = value.toString();
        $(locator).setValue(strVal);
        $(locator).shouldHave(exactValue(strVal));
    }

    /**
     * Вводит значение в элемент и сразу проверяет правильность ввода
     */
    @Step("Ввод '{value}' с проверкой")
    static public void enterWithCheck(SelenideElement element, Object value) {
        String strVal = value.toString();
        element.setValue(strVal);
        element.shouldHave(exactValue(strVal));
    }

    /**
     * Очищает текстовое поле (нужна в случаях, когда селенидовская функция не работает)
     */
    static public void clearWithCheck(SelenideElement element) {
        element.sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.DELETE);
        element.shouldHave(exactValue(""));
    }

    /**
     * Очищает текстовое поле (нужна в случаях, когда селенидовская функция не работает)
     */
    static public void clearWithCheck(By locator) {
        clearWithCheck($(locator));
    }

    /**
     * Ввод значения с проверкой и предварительной очисткой поля
     */
    static public void clearAndEnterWithCheck(SelenideElement element, Object value) {
        clearWithCheck(element);
        enterWithCheck(element, value);
    }

    /**
     * Ввод значения с проверкой и предварительной очисткой поля
     */
    @Step("Ввод значения {value} с проверкой и предварительной очисткой поля")
    static public void clearAndEnterWithCheck(By locator, Object value) {
        clearWithCheck(locator);
        enterWithCheck(locator, value);
    }

    /**
     * Прокрутить до элемента так, чтобы он располагался не вплотную к экрану (добавить поля сверху или снизу)
     */
    static public void scrollIntoViewWithMargin(SelenideElement element, boolean onTopOfScreen) {
        if (element.exists() && element.isDisplayed()) {
            element.scrollIntoView(onTopOfScreen);
            if (onTopOfScreen)
                $(AnyPage.onAnyPage.body).sendKeys(Keys.ARROW_UP, Keys.ARROW_UP);
            else
                $(AnyPage.onAnyPage.body).sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN);
        }
    }

    static public void scrollIntoViewWithMargin(By locator, boolean onTopOfScreen) {
        scrollIntoViewWithMargin($(locator), onTopOfScreen);
    }

    static public void scrollIntoViewWithMargin(SelenideElement element) {
        scrollIntoViewWithMargin(element, true);
    }

    static public void scrollIntoViewWithMargin(By locator) {
        scrollIntoViewWithMargin($(locator), true);
    }
}
