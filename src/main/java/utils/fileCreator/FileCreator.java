package utils.fileCreator;

import utils.StringGenerator;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static java.lang.Math.max;

public class FileCreator {

    public final File baseDirectory;
    public final File samplesDirectory;

    public FileCreator() {
        samplesDirectory = new File("src/main/resources/uploadFileSamples");
        baseDirectory = new File("generatedFiles");
        if (!baseDirectory.exists()) {
            if (!baseDirectory.mkdir()) {
                System.err.printf("Не получилось создать директорию для хранения файлов %s%n", baseDirectory.toString());
                assert false;
            }
        }
    }

    private String sizeToString(long sizeInBytes) {
        sizeInBytes = max(0, sizeInBytes);
        long GiB = sizeInBytes / (1024 * 1024 * 1024);
        long MiB = sizeInBytes % (1024 * 1024 * 1024) / (1024 * 1024);
        long KiB = sizeInBytes % (1024 * 1024) / 1024;
        long B = sizeInBytes % 1024;
        return "Size_" + (GiB > 0 ? GiB + "GiB_" : "") + (MiB > 0 ? MiB + "MiB_" : "") + (KiB > 0 ? KiB + "KiB_" : "") + B + "B";
    }

    private void copySampleFileToPath(FileFormats format, Path destination) {
        final Path copyingFilePath = Paths.get(samplesDirectory.getAbsolutePath(), String.format("file.%s", format));
        try {
            Files.copy(copyingFilePath, destination, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Возвращает уникальный путь для нового файла nameWithExt, сам файл не создается,
     * но могут создаваться поддиректории в baseDirectory, для обеспечения уникальности путей
     * <p>
     * Данный метод нужен для возможности генерировать нескольких файлов с одинаковыми именами (но разными путями)
     */
    private File createPathForNewFile(String nameWithExt) {
        File resultPath = new File(baseDirectory, nameWithExt);
        if (resultPath.exists()) {
            File newSubDir = new File(baseDirectory, new StringGenerator().withDigits(true).generate(6));
            if (!newSubDir.mkdir()) {
                System.err.printf("Не получилось создать директорию %s%n", newSubDir.toString());
                assert false;
            }
            resultPath = new File(newSubDir, nameWithExt);
        }
        return resultPath;
    }

    /** Создает файл с валидным содержимым, с заданным именем, с заданным расширением */
    public File createFileWithValidContent(FileFormats format, String name, String extension) {
        final String nameWithExt = String.format("%s.%s", name, extension);
        final File resultFile = createPathForNewFile(nameWithExt);
        copySampleFileToPath(format, Paths.get(resultFile.getAbsolutePath()));
        return resultFile;
    }

    /** Создает файл с валидным содержимым, с заданным именем, соответствующим формату расширением */
    public File createFileWithValidContent(FileFormats format, String name) {
        return createFileWithValidContent(format, name, format.getExt());
    }

    /** Создает файл с валидным содержимым, со случайным именем, соответствующим формату расширением */
    public File createFileWithValidContent(FileFormats format) {
        return createFileWithValidContent(format, new StringGenerator().withDigits(true).generate(6), format.getExt());
    }

    /**
     * Создает файл с заданным размером в байтах и заданным именем. При последовательном создании нескольких файлов
     * с одинаковым именем будут созданы файлы с одинаковыми именами, но разными путями
     */
    public File createFile(long size, String nameWithExt) {
        if (nameWithExt == null || nameWithExt.length() == 0) {
            System.err.println("Имя файла не должно быть пустым");
            assert false;
        }
        final File file = createPathForNewFile(nameWithExt);
        RandomAccessFile raf;
        try {
            raf = new RandomAccessFile(file, "rw");
            raf.setLength(max(0, size));
            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
            assert false;
            return null;
        }
        return file;
    }

    /** То-же, что createFile, но имя файла будет содержать описание размера файла в формате Size_xGiB_5MiB_13KiB_6B.ext */
    public File createFileWithDescribedSize(long size, String fileExtension) {
        return createFile(size, sizeToString(size) + "." + fileExtension);
    }

    /** То-же, что createFileWithDescribedSize, но имя файла не будет содержать расширения */
    public File createFileWithDescribedSize(long size) {
        return createFile(size, sizeToString(size));
    }

}