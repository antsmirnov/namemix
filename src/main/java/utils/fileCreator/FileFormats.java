package utils.fileCreator;

public enum FileFormats {
    ODT("odt"),
    DOCX("docx"),
    DOC("doc"),
    PDF("pdf"),
    JPG("jpg"),
    PNG("png"),
    GIF("gif"),
    TXT("txt");

    public final String label;

    FileFormats(String label) { this.label = label;}

    public String getExt() { return label; }

    @Override
    public String toString() { return getExt(); }
}
