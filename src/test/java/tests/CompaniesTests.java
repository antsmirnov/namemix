package tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringFunctions;

import static testData.enums.CompanyForm.*;
import static utils.StringFunctions.randomFioWithDash;

@Features({@Feature("WebADMСайта"), @Feature("Компании")})
public class CompaniesTests extends BaseTest {

    @BeforeClass(description = "Вход в систему", alwaysRun = true)
    public void setup() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - заголовок", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingHeader() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingHeader();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - форма регистрации компании", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingFormOfRegistration() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingFormOfRegistration();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - офиц. имя", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingOfficialName() {
        newCompanySteps.creationFormDisplayingOfficialName();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - сокр. имя", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingShortName() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingShortName();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - ИНН", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingInn() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingInn();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - адрес", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingAddress() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationformdisplayingAddress();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - ФИО контактного лица", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingontactorName() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingContactPersonName();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - телефон", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingPhone() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingPhone();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - Email", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingEmail() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingEmail();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - промо-код", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayinPgromo() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingPromo();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - категория", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingCategory() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingCategory();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - комиссия", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingCommission() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingComission();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - лимит", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingLimit() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingLimit();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - страхование", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingInsurance() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingInsurance();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - оплата регистрами", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingPaymentsRegisters() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingPaymentByRegisters();
    }

    @TmsLink("NAMEMIX-T585")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - отображение - заказы без обеспечения", priority = 1,
            groups = {"UI"})
    public void addCompanyDisplayingOrdersWOPledge() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.creationFormDisplayingOrdersWithoutPledge();
    }

    @TmsLink("NAMEMIX-T587")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - добавление (отмена)", priority = 1,
            groups = {"UI"})
    public void addCompanyCancel() {
        Company validCompany = new Company();
        newCompanySteps.inputCompanyData(validCompany);
        newCompanySteps.clickCancel();
        companiesSteps.checkCompanyDoesNotExist(validCompany.getOfficialName());
    }

    @TmsLink("NAMEMIX-T587")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - добавление (юридическое лицо)", priority = 1,
            groups = {"UI"})
    public void addCompanyAddingLE() {

        Company validCompany = new Company(LEGAL_ENTITY)
                .withOfficialName(StringFunctions.stringWithLengthDescription(30, "Оф. имя"))
                .withShortName(StringFunctions.stringWithLengthDescription(30, "Сокр. имя"))
                .withContactorFio(randomFioWithDash());   // кирилица и знаки "-"

        newCompanySteps.inputCompanyData(validCompany);
        newCompanySteps.clickAdd();
        companiesSteps.checkNotificationsAboutFirmCreation();
        companiesSteps.checkCompanyExists(validCompany.getShortName(), false);
        markCompanyForDeletion(validCompany);
    }

    @TmsLink("NAMEMIX-T587")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - добавление (ИП)", priority = 1,
            groups = {"UI"})
    public void addCompanyAddingIndividual() {

        Company validCompany = new Company(INDIVIDUAL)
                .withFioIp(randomFioWithDash());

        newCompanySteps.inputCompanyData(validCompany);
        newCompanySteps.clickAdd();
        companiesSteps.checkNotificationsAboutFirmCreation();
        companiesSteps.checkCompanyExists(validCompany.getFioIp(), false);
        markCompanyForDeletion(validCompany);
    }

    @TmsLink("NAMEMIX-T587")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - добавление (Иностранная)", priority = 1,
            groups = {"UI"})
    public void addCompanyAddingForeign() {

        Company validCompany = new Company(FOREIGN)
                .withOfficialName(StringFunctions.stringWithLengthDescription(30, "Оф. имя"))
                .withShortName(StringFunctions.stringWithLengthDescription(30, "Сокр. имя"))
                .withContactorFio(randomFioWithDash());   // кирилица и знаки "-"

        newCompanySteps.inputCompanyData(validCompany);
        newCompanySteps.clickAdd();
        companiesSteps.checkNotificationsAboutFirmCreation();
        companiesSteps.checkCompanyExists(validCompany.getShortName(), false);
        markCompanyForDeletion(validCompany);
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Латиница в оф. названии)",
            priority = 2, groups = {"UI"})
    public void addCompanValidationOfNameLatin() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkOfficialNameInputLatin();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком короткое оф. имя)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationOfNameTooShort() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkOfficialNameInputShort();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком длинное оф. имя)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationOfNameTooLong() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkOfficialNameInputLong();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Латиница в оф. названии) [Иностранная]",
            priority = 3, groups = {"UI"})
    public void addCompanValidationOfNameLatinForeign() {
        newCompanySteps.openCreationFormIfNeeds(FOREIGN);
        newCompanySteps.checkOfficialNameInputLatin();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком короткое оф. имя) [Иностранная]",
            priority = 3, groups = {"UI"})
    public void addCompanyValidationOfNameTooShortForeign() {
        newCompanySteps.openCreationFormIfNeeds(FOREIGN);
        newCompanySteps.checkOfficialNameInputShort();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком длинное оф. имя) [Иностранная]",
            priority = 3, groups = {"UI"})
    public void addCompanyValidationOfNameTooLongForeign() {
        newCompanySteps.openCreationFormIfNeeds(FOREIGN);
        newCompanySteps.checkOfficialNameInputLong();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Латиница в сокр. названии)",
            priority = 2, groups = {"UI"})
    public void addCompanValidationShortNameLatin() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkShortNameInputLatin();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком короткое сокр. имя)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationShortNameTooShort() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkShortNameInputShort();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком длинное сокр. имя)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationShortNameTooLong() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkShortNameInputLong();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Латиница в сокр. названии) [Иностранная]",
            priority = 3, groups = {"UI"})
    public void addCompanValidationShortNameLatinForeign() {
        newCompanySteps.openCreationFormIfNeeds(FOREIGN);
        newCompanySteps.checkShortNameInputLatin();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком короткое сокр. имя) [Иностранная]",
            priority = 3, groups = {"UI"})
    public void addCompanyValidationShortNameTooShortForeign() {
        newCompanySteps.openCreationFormIfNeeds(FOREIGN);
        newCompanySteps.checkShortNameInputShort();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Слишком длинное сокр. имя) [Иностранная]",
            priority = 3, groups = {"UI"})
    public void addCompanyValidationShortNameTooLongForeign() {
        newCompanySteps.openCreationFormIfNeeds(FOREIGN);
        newCompanySteps.checkShortNameInputLong();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (ИНН не цифры)", priority = 2,
            groups = {"UI"})
    public void addCompanyValidationInnNotDigits() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInnInputNotDigits();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (ИНН невалидный)", priority = 2,
            groups = {"UI"})
    public void addCompanyValidationInnInvalid() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInnInputInvalidFormat(LEGAL_ENTITY);
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (ИНН - неверная длина)", priority = 2,
            groups = {"UI"})
    public void addCompanyValidationInnInvalidLength() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInnInputBadLength(LEGAL_ENTITY);
    }

    @TmsLink("NAMEMIX-T591")                                              // priority = 4, для ИП тестов валидации ввода
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (ИНН не цифры) [ИП]", priority = 4,
            groups = {"UI"})
    public void addCompanyValidationInnNotDigitsIp() {
        newCompanySteps.openCreationFormIfNeeds(INDIVIDUAL);
        newCompanySteps.checkInnInputNotDigits();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (ИНН невалидный) [ИП]", priority = 4,
            groups = {"UI"})
    public void addCompanyValidationInnInvalidIp() {
        newCompanySteps.openCreationFormIfNeeds(INDIVIDUAL);
        newCompanySteps.checkInnInputInvalidFormat(INDIVIDUAL);
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (ИНН - неверная длина) [ИП]",
            priority = 4, groups = {"UI"})
    public void addCompanyValidationInnInvalidLengthIp() {
        newCompanySteps.openCreationFormIfNeeds(INDIVIDUAL);
        newCompanySteps.checkInnInputBadLength(INDIVIDUAL);
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (EIN - ввод не цифр) [Иностранная]",
            priority = 3,           // priority 3 for foreign company input validation
            groups = {"UI"})

    public void addCompanyValidationEinInputNonDigits() {
        newCompanySteps.openCreationFormIfNeeds(FOREIGN);
        newCompanySteps.checkEinInputNonDigits();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (ФИО контактного лица больше 150 символов)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationInputContactorFioTooLong() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputContactorFioTooLong();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Телефон контактного лица менее 10 цифр)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationInputContactorPhoneShort() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputContactorPhoneShort();
    }

    @TmsLink("NAMEMIX-T591")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Телефон контактного лица более 10 цифр)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationInputContactorPhoneLong() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputContactorFioTooLong();
    }

    @DataProvider(name = "InvalidEmails")
    public Object[] invalidEmails() {
        return new String[]{
                "name@domainio",
                "namedomain.io",
                "@domain.io",
                "name@",
                "    name@domain.io",
                "name@domain.io    "
        };
    }

    @TmsLink("NAMEMIX-T325")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (e-mail)",
            dataProvider = "InvalidEmails", priority = 2,
            groups = {"UI"})
    public void addCompanyValidationInputEmailInvalid(String email) {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputEmailInvalid(email);
    }

    @TmsLink("NAMEMIX-T325")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (e-mail (слишком длинный))",
            priority = 2,
            groups = {"UI"})
    public void addCompanyValidationInputEmailTooLong() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputEmailTooLong();
    }

    @TmsLink("NAMEMIX-T325")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (e-mail граничное значение длины)",
            priority = 2,
            groups = {"UI"})
    public void addCompanyValidationInputEmailLengthValueAlmostTooLong() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputEmailLengthValueAlmostTooLong();
    }

    @TmsLink("NAMEMIX-T325")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Ставка комиссии меньше 4.5)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationInputCommissionLess4dot5() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputCommissionValid("4.36");
    }

    @TmsLink("NAMEMIX-T325")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Ставка комиссии больше 100)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationInputCommissionGT100() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputCommissionGT100();
    }

    @TmsLink("NAMEMIX-T325")
    @Test(description = "WebADMСайта/Компании/Активные/Добавить компанию - Валидация (Ставка комиссии имеет более 2 знаков после запятой)",
            priority = 2, groups = {"UI"})
    public void addCompanyValidationInputCommission2DigFrac() {
        newCompanySteps.openCreationFormIfNeeds();
        newCompanySteps.checkInputCommissionMoreThenTwoFracNumbers();
    }
}
