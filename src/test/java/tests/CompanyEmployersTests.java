package tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.EmployerPosition;
import testData.enums.EmployerRole;
import testData.enums.UserRole;
import testData.models.Company;
import testData.models.Employer;
import testData.models.Phone;

import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.Common.randomInt;
import static utils.StringFunctions.*;

@Features({@Feature("WebADMСайта"), @Feature("Компании"), @Feature("Сотрудники")})
public class CompanyEmployersTests extends BaseTest {
    static Company companyLe;
    static Employer employer, editedEmployer;

    @BeforeClass(description = "Логин, создание компаний для теста (если еще не созданы)", alwaysRun = true)
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testSuffix = "T646-T652";
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testSuffix)
                .withOfficialName("Юридическое лицо для теста NAMEMIX-" + testSuffix);
        employer = new Employer();
        editedEmployer = employer
                .withFirstName(randomFirstName())
                .withSecondName(randomLastName())
                .withPatronymic(randomPatronymic())
                .withEmail(randomEmail(randomInt(10, 15)))
                .withInn("155898408759")
                .withSnils("17823282285")
                .withPhone(new Phone("+73434343410"))
                .withPosition(EmployerPosition.MAIN_ACCOUNTANT)
                .withRole(EmployerRole.COORDINATOR_OF_COMPANY);

        recreateTempCompany(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T646")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - добавление",
            groups = {"UI", "Employers"})
    public void checkEmployerAdding() {
        companyCardEmployersSteps.addEmployer(companyLe, employer);
        companyCardEmployersSteps.checkEmployerExist(companyLe, employer);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T652")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Сотрудники/Редактирование сотрудника - Редактирование",
            dependsOnMethods = "checkEmployerAdding", groups = {"UI", "Employers"})
    public void checkEmployerEditing() {
        companyCardEmployersSteps.editEmployer(companyLe, employer, editedEmployer);
        companyCardEmployersSteps.checkEmployerFields(companyLe, editedEmployer);
    }
}
