package tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;

import static testData.enums.CompanyForm.*;

//@Features({@Feature("WebADMСайта"), @Feature("Компании")})
public class CompanyParamsEditPopupTests extends BaseTest {

    static Company companyLe, companyIndividual, companyForeign;

    @BeforeClass(description = "Логин, создание компаний для теста (если еще не созданы)", alwaysRun = true)
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testSuffix = "T1101-1109";
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testSuffix)
                .withOfficialName("Юридическое лицо для теста NAMEMIX-" + testSuffix);
        companyIndividual = new Company(INDIVIDUAL)
                .withFioIp("AUTO Test " + testSuffix);
        companyForeign = new Company(FOREIGN)
                .withShortName("AUTO Tests ForeignCompany " + testSuffix)
                .withOfficialName("Иностранная компания для теста NAMEMIX-" + testSuffix);

        recreateTempCompany(companyLe);
        recreateTempCompany(companyIndividual);
        recreateTempCompany(companyForeign);
    }

    @TmsLink("NAMEMIX-T1107")
    @Features({@Feature("Юр. лицо")})
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - кнопка \"отмена\" (Юр. Лицо)",
            groups = {"UI"})
    public void commissionRateCancelBtn() {
        companyParamsEditPopupSteps.testCommissionEditAndCancel(companyLe);
    }

    @TmsLink("NAMEMIX-T1107")
    @Features({@Feature("ИП")})
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - кнопка \"отмена\" (ИП)",
            groups = {"UI"})
    public void commissionRateCancelBtnIp() {
        companyParamsEditPopupSteps.testCommissionEditAndCancel(companyIndividual);
    }

    @TmsLink("NAMEMIX-T1107")
    @Features({@Feature("Иностранная")})
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - кнопка \"отмена\" (Иностранная)",
            groups = {"UI"})
    public void commissionRateCancelBtnForeign() {
        companyParamsEditPopupSteps.testCommissionEditAndCancel(companyForeign);
    }

    @TmsLink("NAMEMIX-T1109")
    @Features({@Feature("Юр. лицо")})
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - кнопка \"сохранить\" (Юр. Лицо)",
            groups = {"UI"})
    public void commissionRateSaveBtn() {
        companyParamsEditPopupSteps.testCommissionEditAndSave(companyLe);
    }

    @TmsLink("NAMEMIX-T1109")
    @Features({@Feature("ИП")})
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - кнопка \"сохранить\" (ИП)",
            groups = {"UI"}, dependsOnMethods = {"commissionRateSaveBtn"})
    public void commissionRateSaveBtnIp() {
        companyParamsEditPopupSteps.testCommissionEditAndSave(companyIndividual);
    }

    @TmsLink("NAMEMIX-T1109")
    @Features({@Feature("Иностранная")})
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - кнопка \"сохранить\" (Иностранная)",
            groups = {"UI"})
    public void commissionRateSaveBtnForeign() {
        companyParamsEditPopupSteps.testCommissionEditAndSave(companyForeign);
    }

    @TmsLink("NAMEMIX-T1104")
//    @Features({@Feature("Юр. лицо")})
    @Feature("NAMEMIX-T1104. WebADMНаймикса/Компании/Процентная ставка - валидация")
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - валидация - меньше чем 4.5 (Юр. Лицо)",
            groups = {"UI", "Checklist"})
    public void commissionRateValidationLessThen4andHalf() {
        companyParamsEditPopupSteps.checkCommissionWithValueLessThen4andHalf(companyLe);
    }

    @TmsLink("NAMEMIX-T1104")
//    @Features({@Feature("Юр. лицо")})
    @Feature("NAMEMIX-T1104. WebADMНаймикса/Компании/Процентная ставка - валидация")
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - валидация - ввод букв (Юр. Лицо)",
            groups = {"UI", "Checklist"})
    public void commissionRateValidationLetters() {
        companyParamsEditPopupSteps.checkCommissionLetters(companyLe);
    }

    @TmsLink("NAMEMIX-T1104")
//    @Features({@Feature("Юр. лицо")})
    @Feature("NAMEMIX-T1104. WebADMНаймикса/Компании/Процентная ставка - валидация")
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - валидация - ввод > 100 (Юр. Лицо)",
            groups = {"UI", "Checklist"})
    public void commissionRateValidationTooBig() {
        companyParamsEditPopupSteps.checkPromptWithInvalidCommission("111", "Максимальное допустимое значение равно 100", companyLe);
    }

    @TmsLink("NAMEMIX-T1104")
//    @Features({@Feature("Юр. лицо")})
    @Feature("NAMEMIX-T1104. WebADMНаймикса/Компании/Процентная ставка - валидация")
    @Test(description = "WebADMНаймикса/Компании/Процентная ставка - валидация - ввод 3 цифр после запятой (Юр. Лицо)",
            groups = {"UI", "Checklist"})
    public void commissionRateValidation3Digits() {
        companyParamsEditPopupSteps.checkCommission3digits(companyLe);
    }

    // TODO NAMEMIX-T1104 - проверка округления дробного числа

}
