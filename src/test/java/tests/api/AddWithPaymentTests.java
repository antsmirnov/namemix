package tests.api;

import apis.nmapi.requestModel.AddWithPaymentRequestModel;
import db.model.Contractor;
import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.models.User;
import utils.StringGenerator;

import static testData.enums.UserRole.WEBADMIN_OF_COMPANY;

@Features({@Feature("API")})
public class AddWithPaymentTests extends BaseTest {
    public User user;
    public String unknownPhone;
    public String knownPhone;
    public String validInn;
    public String strWithSpecialSymbols;
    public Contractor fullyRegistered;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        user = new User(WEBADMIN_OF_COMPANY);
        unknownPhone = "72224422366";
        knownPhone = "70001122334";
        validInn = "236844264735";
        strWithSpecialSymbols = ";.!$%^~`/\\\t\r\n";
        fullyRegistered = dbSteps.getContractorFullyRegistered();
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки CONTRACTOR_PHONE_NOT_FOUND (unknown phone)",
            groups = {"API"})
    public void addWithPaymentErrorCheckPhoneUnknown() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withContractorPhone(unknownPhone),
                "CONTRACTOR_PHONE_NOT_FOUND", "На платформе Наймикс не найден самозанятый с указанным номером телефона");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки CONTRACTOR_NOT_FULLY_REGISTERED", groups = {"API"})
    public void addWithPaymentErrorCheckNotFullyReg() {
        Contractor contractor = dbSteps.getContractorNotFullyRegistered();
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withContractorPhone(contractor.phone).withContractorInn(contractor.inn),
                "CONTRACTOR_NOT_FULLY_REGISTERED", "Самозанятый не прошел полную регистрацию");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки CONTRACTOR_INN_DIFFERS", groups = {"API"})
    public void addWithPaymentErrorCheckInnDiffers() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withContractorPhone(fullyRegistered.phone).withContractorInn(validInn),
                "CONTRACTOR_INN_DIFFERS", "На платформе Наймикс ИНН самозанятого отличается от указанного");
    }

    //TODO: INCORRECT_ORDER_WORK_START_DATE
    //TODO: TAXPAYER_UNREGISTERED
    //TODO: TAXPAYER_UNBOUND
    //TODO: CONFIRM_IN_MY_TAX
    //TODO: TAX_CALL_TIMEOUT
    //TODO: TAX_CALL_ERROR
    //TODO: BANK_SERVICE_ACCESS_ERROR
    //TODO: CONTRACTOR_CONCLUSION_ERROR
    //TODO: CONTRACTOR_NOT_FOUND_IN_BANK
    //TODO: и прочие статусы...

    // INVALID_ORDER_AMOUNT:

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INVALID_ORDER_AMOUNT (отрицательное orderAmount)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderAmountNeg() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderAmount("-1"),
                "INVALID_ORDER_AMOUNT", "Сумма заказа вне допустимого диапазона (60 - 300000 руб)");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INVALID_ORDER_AMOUNT (нулевой orderAmount)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderAmountZero() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderAmount("0.0"),
                "INVALID_ORDER_AMOUNT", "Сумма заказа вне допустимого диапазона (60 - 300000 руб)");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INVALID_ORDER_AMOUNT (граничное значение orderAmount (59.99))",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderAmountB1() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderAmount("59.99"),
                "INVALID_ORDER_AMOUNT", "Сумма заказа вне допустимого диапазона (60 - 300000 руб)");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INVALID_ORDER_AMOUNT (граничное значение orderAmount (300000.01))",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderAmountB2() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderAmount("300000.01"),
                "INVALID_ORDER_AMOUNT", "Сумма заказа вне допустимого диапазона (60 - 300000 руб)");
    }

    // INCORRECT_INPUT_ERROR:

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (requestId)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectReqId() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withRequestId("Невалидный UUID!"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: requestId");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (contractorPhone)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectInputPhone() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withContractorPhone("Невалидный телефон"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: номер телефона исполнителя");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (contractorPhone не с 7)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectInputPhoneNot7() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withContractorPhone("61234567890"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: номер телефона исполнителя");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (contractorInn)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorInn() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorInn("Невалидный ИНН"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: ИНН исполнителя");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (contractorInn 10 цифр)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorInn10() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withContractorPhone(knownPhone).withContractorInn("1234567890"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: ИНН исполнителя");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (слишком длинное contractorFirstName)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorFirstNameLong() {
        StringGenerator generator = new StringGenerator().withLatinLowerCase(false).withCyrillicLowerCase(true);
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorFirstName(generator.generate(51)
                        ), "INCORRECT_INPUT_ERROR", "Некорректный формат поля: Имя самозанятого");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (слишком длинное contractorLastName)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorLastNameLong() {
        StringGenerator generator = new StringGenerator().withLatinLowerCase(false).withCyrillicLowerCase(true);
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorLastName(generator.generate(51)
                        ), "INCORRECT_INPUT_ERROR", "Некорректный формат поля: Фамилия самозанятого");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (слишком длинное contractorPatronymic)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorPatronymicLong() {
        StringGenerator generator = new StringGenerator().withLatinLowerCase(false).withCyrillicLowerCase(true);
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorPatronymic(generator.generate(51)
                        ), "INCORRECT_INPUT_ERROR", "Некорректный формат поля: Отчество самозанятого");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (contractorPaymentAgreementRequired)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectPayAgreementReq() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorPaymentAgreementRequired(strWithSpecialSymbols),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: Требуется подтверждение платежа от самозанятого в мобильном приложении");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (слишком короткое orderName)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderNameShort() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderName("AA"),
                "INCORRECT_INPUT_ERROR", "Некорректная длина поля: наименование заказа");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (слишком длинное orderName (103c))",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderNameLong() {
        StringGenerator generator = new StringGenerator().withExtra(" ");
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderName(generator.generate(103)),
                "INCORRECT_INPUT_ERROR", "Некорректная длина поля: наименование заказа");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (слишком короткое orderDescription)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderDescriptionShort() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderDescription("AAAA"),
                "INCORRECT_INPUT_ERROR", "Некорректная длина поля: описание выполненых работ");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (слишком длинное orderDescription (256c))",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderDescriptionLong() {
        StringGenerator generator = new StringGenerator();
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel().withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderDescription(generator.generate(256)),
                "INCORRECT_INPUT_ERROR", "Некорректная длина поля: описание выполненых работ");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (некорректный формат orderAmount)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderAmountBadFormat() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderAmount("Letters!"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: сумма заказа");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (некорректный формат OrderWorkStartDate)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderWorkStartDateBadFormat() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderWorkStartDate("2020--02"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: дата начала работ");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (некорректный формат OrderWorkEndDate)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderWorkEndDateBadFormat() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderWorkEndDate("2020--02"),
                "INCORRECT_INPUT_ERROR", "Некорректный формат поля: дата завершения работ");
    }

    /*  Пустые поля:  */

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое RequestId)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectRequestIdEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withRequestId(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: идентификатор запроса");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое ContractorPhone)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorPhoneEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorPhone(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: номер телефона исполнителя");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое ContractorInn)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorInnEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorInn(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: ИНН исполнителя");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое ContractorFirstName)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorFirstNameEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorFirstName(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: Имя самозанятого");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое ContractorLastName)",
            groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorLastNameEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorLastName(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: Фамилия самозанятого");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR " +
            "(пустое ContractorPaymentAgreementRequired)", groups = {"API"})
    public void addWithPaymentErrorIncorrectContractorPaymentAgreementRequiredEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withContractorPaymentAgreementRequired(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: требуется подписание акта");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое OrderName)", groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderNameEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderName(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: наименование заказа");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое OrderDescription)", groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderDescriptionEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderDescription(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: описание выполненых работ");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое OrderAmount)", groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderAmountEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderAmount(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: сумма на карту");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое OrderWorkStartDate)", groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderWorkStartDateEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderWorkStartDate(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: дата начала работ по заказу");
    }

    @Test(description = "Api. orders/addWithPayment/ проверка ошибки INCORRECT_INPUT_ERROR (пустое OrderWorkEndDate)", groups = {"API"})
    public void addWithPaymentErrorIncorrectOrderWorkEndDateEmpty() {
        nmApiAddWithPaymentSteps.addWithPaymentErrorCheck(user,
                new AddWithPaymentRequestModel()
                        .withPhoneAndInnForFullyRegisteredContractor()
                        .withOrderWorkEndDate(""),
                "INCORRECT_INPUT_ERROR", "Не заполнено обязательное поле: дата окончания работ по заказу");
    }
}
