package tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.User;

public class AuthTests extends BaseTest {

    @Features({@Feature("API")})
    @Test(description = "API. Проверка получения рабочего AccessToken-а", groups = {"API"})
    public void gettingGoodAccessTokens() {
        String accessToken = nmApiAuthSteps.getAuthTokensByLoginPass(new User(UserRole.WEBADMIN_OF_COMPANY)).accessToken;
        nmApiDepositSteps.depositBalanceIsAccessible(accessToken);
    }

    @Features({@Feature("API")})
    @Test(description = "API. Проверка получения AccessToken-а методом refreshToken", groups = {"API"})
    public void gettingGoodAccessTokensViaRefreshToken() {
        String refreshToken = nmApiAuthSteps.getAuthTokensByLoginPass(new User(UserRole.WEBADMIN_OF_COMPANY)).refreshToken;
        String accessToken = nmApiAuthSteps.getAuthTokensByRefreshToken(refreshToken).accessToken;
        nmApiDepositSteps.depositBalanceIsAccessible(accessToken);
    }
}
