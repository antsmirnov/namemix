package tests.api;

import db.model.Contractor;
import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import service.TestsConfig;
import testData.enums.EmployerPosition;
import testData.enums.EmployerRole;
import testData.enums.UserRole;
import testData.models.Company;
import testData.models.Employer;
import testData.models.User;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static testData.enums.CompanyForm.LEGAL_ENTITY;

@Features({@Feature("API")})
public class ContractInitTests extends BaseTest {

    public static TestsConfig testsConfig = ConfigFactory.create(TestsConfig.class);
    public static User webAdmOfCompany = new User(UserRole.WEBADMIN_OF_COMPANY);
    public static String validInn = "236844264735";

    public static User webAdmOfCompanyForSign = new User("leemp004@xyz.yz", testsConfig.universalTestPassword());
    public static Contractor contractorForSign = new Contractor("441501415325", "70001122334");

    //TODO: для init добавить проверки для статусов CONTRACT_ALREADY_SIGNED, TAX_CALL_TIMEOUT, TAX_CALL_ERROR, TAXPAYER_UNBOUND

    @BeforeClass(description = "Создание компании с сотрудником", groups = {"API"}, alwaysRun = true)
    public void setup() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        Company companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany UNKNOWN-001")
                .withOfficialName("Юридическое лицо для теста UNKNOWN-001");

        recreateTempCompany(companyLe);

        Employer leEmployer = new Employer()
                .withFirstName("Фёдор")
                .withSecondName("Автов")
                .withPatronymic("Алексеевич")
                .withRole(EmployerRole.ADMIN_OF_COMPANY)
                .withPosition(EmployerPosition.LEADER)
                .withEmail(webAdmOfCompanyForSign.login)
                .withPassword(webAdmOfCompanyForSign.password);

        companyCardEmployersSteps.addEmployerIfDoesNotExist(companyLe, leEmployer);

        closeWebDriver();
    }

    @Test(description = "API. /contract/init проверка ошибки NO_CONTRACTOR_FOUND", groups = {"API"})
    public void checkContractInitNoSuchContractor() {
        String nonExistentPhone = "73334004333";
        dbSteps.checkContractorsWithPhoneNotExist(nonExistentPhone);
        nmApiContractInitSteps.contractInitCheckError(webAdmOfCompany,
                nonExistentPhone,
                validInn,
                "CONTRACTOR_PHONE_NOT_FOUND",   // По спеке NO_CONTRACTOR_FOUND
                "На платформе Наймикс не найден самозанятый с указанным номером телефона");
    }

    @Test(description = "API. /contract/init проверка ошибки CONTRACTOR_NOT_FULLY_REGISTERED", groups = {"API"})
    public void checkContractInitNotFully() {
        Contractor contractor = dbSteps.getContractorNotFullyRegistered();
        nmApiContractInitSteps.contractInitCheckError(webAdmOfCompany,
                contractor.phone,
                contractor.inn,
                "CONTRACTOR_NOT_FULLY_REGISTERED",
                "Самозанятый не прошел полную регистрацию");
    }

    @Test(description = "API. /contract/init проверка ошибки CONTRACTOR_INN_DIFFERS", groups = {"API"})
    public void checkContractInitInnDiffers() {
        Contractor contractor = dbSteps.getContractorFullyRegistered();
        nmApiContractInitSteps.contractInitCheckError(webAdmOfCompany,
                contractor.phone,
                validInn,
                "CONTRACTOR_INN_DIFFERS",
                "На платформе Наймикс ИНН самозанятого отличается от указанного");
    }

    @Test(description = "API. /contract/init проверка ошибки INCORRECT_INPUT_ERROR (пустой телефон)", groups = {"API"})
    public void checkContractInitIncorrectInputNoPhone() {
        nmApiContractInitSteps.contractInitCheckError(webAdmOfCompany,
                "",
                validInn,
                "INCORRECT_INPUT_ERROR",
                "Не заполнено обязательное поле: номер телефона исполнителя");
    }

    @Test(description = "API. /contract/init проверка ошибки INCORRECT_INPUT_ERROR (пустой ИНН)", groups = {"API"})
    public void checkContractInitIncorrectInputNoInn() {
        Contractor contractor = dbSteps.getContractorFullyRegistered();
        nmApiContractInitSteps.contractInitCheckError(webAdmOfCompany,
                contractor.phone,
                "",
                "INCORRECT_INPUT_ERROR",
                "Не заполнено обязательное поле: ИНН исполнителя");
    }

    @Test(description = "API. /contract/init - успешный init", groups = {"API"})
    public void checkContractInitSuccess() {
        nmApiContractInitSteps.contractInitSuccess(webAdmOfCompanyForSign, contractorForSign.phone, contractorForSign.inn);
    }

}
