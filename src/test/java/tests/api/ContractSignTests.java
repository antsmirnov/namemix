package tests.api;

import db.model.Contractor;
import io.qameta.allure.Feature;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import service.TestsConfig;
import testData.enums.EmployerPosition;
import testData.enums.EmployerRole;
import testData.enums.UserRole;
import testData.models.Company;
import testData.models.Employer;
import testData.models.User;

import java.text.SimpleDateFormat;
import java.util.Date;

import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.StringFunctions.randomEmail;

@Feature("API")
public class ContractSignTests extends BaseTest {
    public static TestsConfig testsConfig = ConfigFactory.create(TestsConfig.class);
    public static User webAdmOfCompany = new User(UserRole.WEBADMIN_OF_COMPANY);
    public static String validInn = "236844264735";
    public static String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    public static String invalidSmsCode = "111";
    public static String validSmsCode = "222";
    public static User webAdmOfCompanyForSign = new User(randomEmail(30), testsConfig.universalTestPassword());
    public static Contractor contractorForSign = new Contractor("441501415325", "70001122334");
    public static Company companyLe;
    public static Employer leEmployer;

    @BeforeClass(description = "Создание компании с сотрудником", groups = {"API"}, alwaysRun = true)
    public void setup() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany Signing")
                .withOfficialName("Юридическое лицо для теста Signing");
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany Signing")
                .withOfficialName("Юридическое лицо для теста Signing");

        recreateTempCompany(companyLe);

        leEmployer = new Employer()
                .withFirstName("Фёдор")
                .withSecondName("Автов")
                .withPatronymic("Алексеевич")
                .withRole(EmployerRole.ADMIN_OF_COMPANY)
                .withPosition(EmployerPosition.LEADER)
                .withEmail(webAdmOfCompanyForSign.login)
                .withPassword(webAdmOfCompanyForSign.password);

        companyCardEmployersSteps.addEmployerIfDoesNotExist(companyLe, leEmployer);
    }

    @Test(description = "API. /contract/sign проверка ошибки NO_CONTRACTOR_FOUND", groups = {"API"})
    public void checkSignErrorContactorNotFoundByPhone() {
        String thisPhoneDoesNotExist = "73334004333";
        nmApiContractSignSteps.contractSignCheckError(
                webAdmOfCompany,
                thisPhoneDoesNotExist,
                validInn,
                todayDate,
                validSmsCode,
                "NO_CONTRACTOR_FOUND",
                "На платформе Наймикс не найден самозанятый с указанным номером телефона");
    }

    @Test(description = "API. /contract/sign проверка ошибки CONTRACTOR_NOT_FULLY_REGISTERED", groups = {"API"})
    public void checkSignErrorContractorNotFullyRegistered() {
        Contractor contractor = dbSteps.getContractorNotFullyRegistered();
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompany,
                contractor.phone,
                contractor.inn,
                todayDate,
                validSmsCode,
                "CONTRACTOR_NOT_FULLY_REGISTERED",
                "Самозанятый не прошел полную регистрацию");
    }

    @Test(description = "API. /contract/sign проверка ошибки CONTRACTOR_INN_DIFFERS", groups = {"API"})
    public void checkSignErrorInnDiffers() {
        Contractor contractor = dbSteps.getContractorFullyRegistered();
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompany,
                contractor.phone,
                validInn,
                todayDate,
                validSmsCode,
                "CONTRACTOR_INN_DIFFERS",
                "На платформе Наймикс ИНН самозанятого отличается от указанного");
    }

    @Test(description = "API. /contract/sign проверка ошибки CONTRACT_ALREADY_SIGNED", groups = {"API"},
            dependsOnMethods = {"checkSignSuccess"})
    public void checkSignErrorAlreadySigned() {
        Contractor contractor = dbSteps.getContractorFullyRegistered();
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompanyForSign,
                contractorForSign.phone,
                contractorForSign.inn,
                todayDate, validSmsCode,
                "CONTRACT_ALREADY_SIGNED",
                "Договор уже заключен");
    }

    // TODO: TAX_CALL_TIMEOUT
    // TODO: TAX_CALL_ERROR

    @Test(description = "API. /contract/sign проверка ошибки INCORRECT_INPUT_ERROR (телефон)", groups = {"API"})
    public void checkSignErrorNoPhone() {
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompany,
                "",
                validInn,
                todayDate,
                validSmsCode,
                "INCORRECT_INPUT_ERROR",
                "Не заполнено обязательное поле: номер телефона исполнителя");
    }

    @Test(description = "API. /contract/sign проверка ошибки INCORRECT_INPUT_ERROR (инн)", groups = {"API"})
    public void checkSignErrorNoInn() {
        Contractor contactor = dbSteps.getContractorFullyRegistered();
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompany,
                contactor.phone,
                "",
                todayDate,
                validSmsCode,
                "INCORRECT_INPUT_ERROR",
                "Не заполнено обязательное поле: ИНН исполнителя");
    }

    @Test(description = "API. /contract/sign проверка ошибки INCORRECT_INPUT_ERROR (дата)", groups = {"API"})
    public void checkSignErrorNoDate() {
        Contractor contactor = dbSteps.getContractorFullyRegistered();
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompany,
                contactor.phone,
                validInn,
                "",
                validSmsCode,
                "INCORRECT_INPUT_ERROR",
                "Не заполнено обязательное поле: дата подписания договора");
    }

    @Test(description = "API. /contract/sign проверка ошибки INCORRECT_INPUT_ERROR (смс)", groups = {"API"})
    public void checkSignErrorNoSms() {
        Contractor contactor = dbSteps.getContractorFullyRegistered();
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompany,
                contactor.phone,
                validInn,
                todayDate,
                "",
                "INCORRECT_INPUT_ERROR",
                "Не заполнено обязательное поле: СМС код");
    }

    @Test(description = "API. /contract/sign - SMS_CODE_ERROR", groups = {"API"})
    public void checkSignErrorSmsCodeError() {
        nmApiContractInitSteps.contractInitSuccess(webAdmOfCompanyForSign, contractorForSign.phone, contractorForSign.inn);
        nmApiContractSignSteps.contractSignCheckError(webAdmOfCompanyForSign, contractorForSign.phone, contractorForSign.inn,
                todayDate, invalidSmsCode, "SMS_CODE_ERROR", "СМС код не совпал");
    }

    @Test(description = "API. /contract/sign - успешное подписание", groups = {"API"},
            dependsOnMethods = {"checkSignErrorSmsCodeError"})
    public void checkSignSuccess() {
        nmApiContractSignSteps.contractSignSuccess(webAdmOfCompanyForSign, contractorForSign.phone, contractorForSign.inn,
                todayDate, validSmsCode);
    }

}
