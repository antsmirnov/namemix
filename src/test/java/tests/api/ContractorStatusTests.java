package tests.api;

import db.model.Contractor;
import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.User;

public class ContractorStatusTests extends BaseTest {

    public static User webAdmOfCompany = new User(UserRole.WEBADMIN_OF_COMPANY);
    public static String validInn = "236844264735";

    @Features({@Feature("API")})
    @Test(description = "API. checkContractorStatus - проверка ответа для исполнителя, прошедшего полную регистрацию",
            groups = {"API"})
    public void checkContractorStatusFullyRegistered() {
        nmApiContractorStatusSteps.checkContractorStatusWithValidInnAndPhone(webAdmOfCompany);
    }

    @Features({@Feature("API")})
    @Test(description = "API. checkContractorStatus. Проверка ошибки CONTRACTOR_PHONE_NOT_FOUND", groups = {"API"})
    public void checkContractorStatusErrorPhone() {
        String nonExistentPhone = "73334004333";
        dbSteps.checkContractorsWithPhoneNotExist(nonExistentPhone);
        nmApiContractorStatusSteps.checkContractorStatusError(webAdmOfCompany,
                nonExistentPhone,
                validInn,
                "CONTRACTOR_PHONE_NOT_FOUND",
                "На платформе Наймикс не найден самозанятый с указанным номером телефона");
    }

    @Features({@Feature("API")})
    @Test(description = "API. checkContractorStatus. Проверка ошибки CONTRACTOR_NOT_FULLY_REGISTERED", groups = {"API"})
    public void checkContractorStatusErrorNotFullyRegistered() {
        Contractor contractorNotFullRegistered = dbSteps.getContractorNotFullyRegistered();
        nmApiContractorStatusSteps.checkContractorStatusError(webAdmOfCompany,
                contractorNotFullRegistered.phone,
                contractorNotFullRegistered.inn,
                "CONTRACTOR_NOT_FULLY_REGISTERED",
                "Самозанятый не прошел полную регистрацию");
    }

    @Features({@Feature("API")})
    @Test(description = "API. checkContractorStatus. Проверка ошибки CONTRACTOR_INN_DIFFERS", groups = {"API"})
    public void checkContractorStatusErrorInnDiffers() {
        Contractor contractor = dbSteps.getContractorFullyRegistered();
        nmApiContractorStatusSteps.checkContractorStatusError(webAdmOfCompany,
                contractor.phone,
                validInn,
                "CONTRACTOR_INN_DIFFERS",
                "На платформе Наймикс ИНН самозанятого отличается от указанного");
    }

    @Features({@Feature("API")})
    @Test(description = "API. checkContractorStatus. Проверка ошибки TAXPAYER_UNREGISTERED", groups = {"API"})
    public void checkContractorStatusErrorFns() {
        Contractor contractor = dbSteps.getContractorNotRegisteredInFns();
        nmApiContractorStatusSteps.checkContractorStatusError(webAdmOfCompany,
                contractor.phone,
                contractor.inn,
                "TAXPAYER_UNREGISTERED",
                "Cамозанятый не зарегистрирован в ФНС"); // латинская 'C' (!)
    }

    // TODO: добавить проверку прочих статусов
}
