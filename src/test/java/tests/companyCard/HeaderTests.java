package tests.companyCard;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.Values;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static testData.enums.CompanyForm.LEGAL_ENTITY;

public class HeaderTests extends BaseTest {
    Company company;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1740-T1741";
        company = new Company(LEGAL_ENTITY)
                .withShortName("AUTO tests LeCompany " + testCode)
                .withOfficialName("AUTO tests LeCompany " + testCode);
        recreateTempCompany(company);
    }

    @TmsLink("NAMEMIX-T1740")
    @TmsLink("NAMEMIX-T1741")
    @Feature("NAMEMIX-T1740. Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    @Feature("NAMEMIX-T1741. Админ Наймикса/Компании/Карточка компании ИП/Информация - Редактирование заголовка")
    @Test(description = "Step 1. Проверка наличия кнопок", groups = {"Checklist", "UI"})
    public void companyHeaderButtons() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.headerButtonsExists();
    }

    @TmsLink("NAMEMIX-T1740")
    @Feature("NAMEMIX-T1740. Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    @Test(description = "Валидация заголовка. Step 2. Обязательное поле", groups = {"Checklist", "UI"})
    public void companyHeaderMandatory() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.headerMandatory();
    }

    @TmsLink("NAMEMIX-T1740")
    @Feature("NAMEMIX-T1740. Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    @Test(description = "Валидация заголовка. Step 3.1. Разрешенные символы ", groups = {"Checklist", "UI"})
    public void companyHeaderAllowed() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.headerSaveAndCheck(Values.cyrillicAlphabet);
        companyCardHeaderSteps.headerSaveAndCheck(Values.latinAlphabet);
        companyCardHeaderSteps.headerSaveAndCheck(Values.digits);
        // Не полностью соответствует тесткейсу "- Допускается ввод цифр, спецсимволов, кириллицы и латиницы."
        // `@#£€$¢¥§%°^&*_+={}[|\/'<> запрещены
        companyCardHeaderSteps.headerSaveAndCheck("?!№()-:;\",.");
    }

    @DataProvider(name = "theseSymbolsShouldBeAllowedMaybe")
    public Object[] theseSymbolsShouldBeAllowedMaybe() {
        return ("+&@*").chars().mapToObj(c -> Character.toString((char) c)).toArray(String[]::new);
    }

    @TmsLink("NAMEMIX-T1740")
    @Feature("NAMEMIX-T1740. Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    @Test(description = "Валидация заголовка. Step 3.1.5. Разрешенные символы - символы которые не разрешены, но возможно должны быть разрешены",
            groups = {"Checklist", "UI"}, dataProvider = "theseSymbolsShouldBeAllowedMaybe")
    public void companyHeaderAllowedAtomicTests(String character) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.headerSaveAndCheck(character + character + character + character + character); // min length = 5
    }

    @TmsLink("NAMEMIX-T1740")
    @Feature("NAMEMIX-T1740. Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    @Test(description = "Валидация заголовка. Step 3.2. Минимальная длина", groups = {"Checklist", "UI"})
    public void companyHeaderMinLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.checkPromptForInput("123", "Минимальная длина строки 5 символов");
        companyCardHeaderSteps.checkPromptForInput("1234", "Минимальная длина строки 5 символов");
        companyCardHeaderSteps.headerSaveAndCheck("12345");
    }

    @TmsLink("NAMEMIX-T1740")
    @Feature("NAMEMIX-T1740. Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    @Test(description = "Валидация заголовка. Step 3.3. Максимальная длина", groups = {"Checklist", "UI"})
    public void companyHeaderMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.headerSaveAndCheck(new StringGenerator().generate(99));
        companyCardHeaderSteps.headerSaveAndCheck(new StringGenerator().generate(100));
        companyCardHeaderSteps.checkPromptForInput(new StringGenerator().generate(101), "Максимальная длина - 100 символов");
        companyCardHeaderSteps.checkPromptForInput(new StringGenerator().generate(102), "Максимальная длина - 100 символов");
    }
    // Step 4 проверяется во всех тестах, где выполняется успешное редактирование

    @TmsLink("NAMEMIX-T1740")
    @Feature("NAMEMIX-T1740. Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    @Test(description = "Валидация заголовка. Step 5. Отмена редактирования", groups = {"Checklist", "UI"})
    public void companyHeaderCancel() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.headerSaveAndCheck("Изначальное имя");
        companyCardHeaderSteps.startEdit();
        companyCardHeaderSteps.setValueToHeaderInput("Новое имя для отмены");
        companyCardHeaderSteps.btnCancelClick();
        companyCardHeaderSteps.checkCompanyNameBeforeAndAfterRefresh("Изначальное имя");
    }

    @TmsLink("NAMEMIX-T1741")
    @Feature("NAMEMIX-T1741. Админ Наймикса/Компании/Карточка компании ИП/Информация - Редактирование заголовка")
    @Test(description = "Step 2. Сохранение заголовка", groups = {"Checklist", "UI"})
    public void companyHeaderSaveAndCheck() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(company);
        companyCardHeaderSteps.headerSaveAndCheck("Редактирование заголовка");
    }

}
