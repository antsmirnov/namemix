package tests.companyCard.info;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;

import static testData.enums.CompanyForm.*;

@Features({@Feature("WebADMСайта"), @Feature("Карточка компании - Инфо")})
public class CompanyCardInfoTests extends BaseTest {

    static public Company companyLe, companyIndividual, companyForeign;

    @BeforeClass(description = "Создание компаний для теста (если еще не созданы)", alwaysRun = true)
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany T598")
                .withOfficialName("Юридическое лицо для теста NAMEMIX-T598");
        companyIndividual = new Company(INDIVIDUAL)
                .withFioIp("AUTO Test T598");
        companyForeign = new Company(FOREIGN)
                .withShortName("AUTO Tests ForeignCompany T598")
                .withOfficialName("Иностранная компания для теста NAMEMIX-T598");

        recreateTempCompany(companyLe);
        recreateTempCompany(companyIndividual);
        recreateTempCompany(companyForeign);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока банковских реквизиторв (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkCompanyDisplayingBankLe() {
        companyCardInfoSteps.checkDisplayingBankRequisites(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Главный бухгалтер' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkCompanyDisplayingMainAccountantLe() {
        companyCardInfoSteps.checkDisplayingMainAccountant(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Руководитель' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkCompanyDisplayingDirectorLe() {
        companyCardInfoSteps.checkDisplayingDirector(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Представитель' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkCompanyDisplayingPredstavitelLe() {
        companyCardInfoSteps.checkDisplayingPredstavitel(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Подписант' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkCompanyDisplayingPodpisantLe() {
        companyCardInfoSteps.checkDisplayingPodpisant(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Лимиты' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkCompanyDisplayingLimitsLe() {
        companyCardInfoSteps.checkDisplayingLimits(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Категория компании' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkDisplayingCompanyCategoryLe() {
        companyCardInfoSteps.checkDisplayingCompanyCategory(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Реквизиты компании' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkDisplayingCompanyRequisitesLe() {
        companyCardInfoSteps.checkDisplayingCompanyRequisites(companyLe);
    }

    @Features({@Feature("Юр. лицо")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Реквизиты компании' (Юридическое лицо)",
            priority = 1, groups = {"UI"})
    public void checkDisplayingCompanyParametersLe() {
        companyCardInfoSteps.checkDisplayingCompanyParameters(companyLe);
    }

    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока банковских реквизиторв (ИП)",
            priority = 2, groups = {"UI"})
    public void checkCompanyDisplayingBankIp() {
        companyCardInfoSteps.checkDisplayingBankRequisites(companyIndividual);
    }

    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Главный бухгалтер' (ИП)",
            priority = 2, groups = {"UI"})
    public void checkCompanyDisplayingMainAccountantIp() {
        companyCardInfoSteps.checkDisplayingMainAccountant(companyIndividual);
    }

    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Представитель' (ИП)",
            priority = 2, groups = {"UI"})
    public void checkCompanyDisplayingPredstavitelIp() {
        companyCardInfoSteps.checkDisplayingPredstavitel(companyIndividual);
    }
    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Подписант' (ИП)",
            priority = 2, groups = {"UI"})
    public void checkCompanyDisplayingPodpisantIp() {
        companyCardInfoSteps.checkDisplayingPodpisant(companyIndividual);
    }

    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Лимиты' (ИП)",
            priority = 2, groups = {"UI"})
    public void checkCompanyDisplayingLimitsIp() {
        companyCardInfoSteps.checkDisplayingLimits(companyIndividual);
    }

    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Категория компании' (ИП)",
            priority = 2, groups = {"UI"})
    public void checkDisplayingCompanyCategoryIp() {
        companyCardInfoSteps.checkDisplayingCompanyCategory(companyIndividual);
    }
    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Реквизиты компании' (ИП)",
            priority = 2, groups = {"UI"})
    public void checkDisplayingCompanyRequisitesIp() {
        companyCardInfoSteps.checkDisplayingCompanyRequisites(companyIndividual);
    }

    @Features({@Feature("ИП")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Реквизиты компании' (ИП)",
            priority = 2, groups = {"UI"})
    public void checkDisplayingCompanyParametersIp() {
        companyCardInfoSteps.checkDisplayingCompanyParameters(companyIndividual);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока банковских реквизиторв (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkCompanyDisplayingBankForeign() {
        companyCardInfoSteps.checkDisplayingBankRequisites(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Главный бухгалтер' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkCompanyDisplayingMainAccountantForeign() {
        companyCardInfoSteps.checkDisplayingMainAccountant(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Руководитель' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkCompanyDisplayingDirectorForeign() {
        companyCardInfoSteps.checkDisplayingDirector(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Представитель' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkCompanyDisplayingPredstavitelForeign() {
        companyCardInfoSteps.checkDisplayingPredstavitel(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Подписант' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkCompanyDisplayingPodpisantForeign() {
        companyCardInfoSteps.checkDisplayingPodpisant(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Лимиты' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkCompanyDisplayingLimitsForeign() {
        companyCardInfoSteps.checkDisplayingLimits(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Категория компании' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkDisplayingCompanyCategoryForeign() {
        companyCardInfoSteps.checkDisplayingCompanyCategory(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Реквизиты компании' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkDisplayingCompanyRequisitesForeign() {
        companyCardInfoSteps.checkDisplayingCompanyRequisites(companyForeign);
    }

    @Features({@Feature("Иностранная")})
    @TmsLink("NAMEMIX-T598")
    @Test(description = "WebADMСайта/Компании/Карточка компании/Контакты - отображение блока 'Реквизиты компании' (Иностранная)",
            priority = 3, groups = {"UI"})
    public void checkDisplayingCompanyParametersForeign() {
        companyCardInfoSteps.checkDisplayingCompanyParameters(companyForeign);
    }
}
