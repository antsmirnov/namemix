package tests.companyCard.info.foreign;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.BaseTest;
import steps.elements.InputDropDownList;
import testData.enums.UserRole;
import testData.models.BankRequisites;
import testData.models.Company;
import testData.models.Phone;
import utils.StringGenerator;

import static pageObject.pages.CompanyCardInfoPage.onCompanyCategoryBlock;
import static testData.enums.CompanyForm.FOREIGN;
import static utils.Common.randomInt;
import static utils.StringFunctions.randomFio;

/*
Карточка компании - информация - Иностранная организация

(только кейсы на редактирование, добавление отрасли, валидация в других файлах)
*/

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
public class CcInfoForeignEditTests extends BaseTest {

    static public Company companyBeforeEdit, companyWithEditedRequisites;
    static public BankRequisites newBankRequisites;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1479-T1503 Edit";
        companyBeforeEdit = new Company(FOREIGN)
                .withShortName("AUTO Tests Foreign Company " + testCode)
                .withOfficialName("Изначальное имя" + testCode)
                .withContactorFio(randomFio())
                .withContactorPhone(new Phone("+10045678900"))
                .withContactorEmail("first@email.com")
                .withAddressActual("414017, Астраханская обл, г Астрахань, Начальный пер")
                .withAddressRegistered("420075, Респ Татарстан, г Казань, ул Начальная")
                .withPhoneOfCompanyOrIp(new Phone("+10045678901"))
                .withEmailOfCompanyOrIp("first.company@email.ru")
                .withEin("00-0000000");
        companyWithEditedRequisites = new Company(FOREIGN)
                .withShortName("AUTO Tests Foreign Company " + testCode)
                .withOfficialName("Изменённое имя" + testCode)
                .withContactorFio("Отредактированное Контактное Лицо")
                .withContactorPhone(new Phone("+90045678903"))
                .withContactorEmail("changed@email.com")
                .withAddressActual("243160, Брянская обл, Красногорский р-н, поселок Новая Москва")
                .withAddressRegistered("169945, Респ Коми, г Воркута, деревня Елец")
                .withPhoneOfCompanyOrIp(new Phone("+90045678904"))
                .withEmailOfCompanyOrIp("changed.company@email.ru")
                .withEin("99-9999999");

        newBankRequisites = new BankRequisites(FOREIGN)
                .withBank("Edited name")
                .withBankAddress("123456, New Address, New St. 1")
                .withSwift(new StringGenerator().withLatin(false).withDigits(true).generate(randomInt(8, 11)))
                .withAccountNumber(new StringGenerator().withLatin(false).withDigits(true).generate(randomInt(8, 34)))
                .withAchRoutingNumber(new StringGenerator().withLatin(false).withDigits(true).generate(9));

        recreateTempCompany(companyBeforeEdit);
    }

    /* Редактирование */

    @TmsLink("NAMEMIX-T1500")
    @Feature("NAMEMIX-T1500. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Реквизиты компании- Редактирование")
    @Test(description = "Реквизиты компании - Редактирование", groups = {"Checklist", "UI"})
    public void companyRequisitesEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfRequisitesBlock(companyWithEditedRequisites);
    }

    @TmsLink("NAMEMIX-T1481")
    @Feature("NAMEMIX-T1481. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Банковские реквизиты- редактирование")
    @Test(description = "Банковские реквизиты - редактирование", groups = {"Checklist", "UI"})
    public void companyBankRequisitesEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfBankRequisitesBlock(newBankRequisites);
    }

    @TmsLink("NAMEMIX-T1503")
    @Feature("NAMEMIX-T1503. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Руководитель компании - редактирование")
    @Test(description = "Руководитель компании - редактирование", groups = {"Checklist", "UI"})
    public void companyDirectorEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfDirectorBlock("Изменённое Имя С-де-фи-сом", new Phone("+13388888888"));
    }

    @TmsLink("NAMEMIX-T1484")
    @Feature("NAMEMIX-T1484. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Главный бухгалтер- редактирование")
    @Test(description = "Главный бухгалтер - редактирование", groups = {"Checklist", "UI"})
    public void companyMainAccountantEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfMainAccountantBlock("Изменённое Имя С-де-фи-сом", new Phone("+23388888888"));
    }

    @TmsLink("NAMEMIX-T1497")
    @Feature("NAMEMIX-T1497. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Представитель - редактирование")
    @Test(description = "Представитель - редактирование", groups = {"Checklist", "UI"})
    public void companyPredstavitelEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfPredstavitelBlock("Изменённое Имя С-де-фи-сом", "Изменённый текст");
    }

    @TmsLink("NAMEMIX-T1494")
    @Feature("NAMEMIX-T1494. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Подписант- редактирование")
    @Test(description = "Подписант - редактирование", groups = {"Checklist", "UI"})
    public void companyPodpisantEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfPodpisantBlock("Должность после", "Расшифровка после");
    }

    /* По идее это должно быть в другом классе с тестами на валидацию, но я решил не разбивать один тест по классам */
    @TmsLink("NAMEMIX-T1485")
    @Feature("NAMEMIX-T1485. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Категория компании - добавление отрасли")
    @Test(description = "Step 1-2. Категория компании - добавление отрасли - проверка отображения", groups = {"Checklist", "UI"})
    public void companyCategoryDisplaying() {
        /*
           Уходим со страницы компани, чтобы openCompanyInfoPageIfNeeds заново открыла ее, пройдясь по UI,
           чтобы соответствовало сценарию
         */
        Selenide.open("/client");
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit, false);
        companyCardInfoSteps.checkDisplayingCompanyCategory(companyBeforeEdit);
    }

    @DataProvider(name = "companyCategories")
    public Object[] companyCategories() {
        return new String[]{
                "Аренда",
                "Виртуальная помощь",
                "Врачебная практика",
                "Грузоперевозки и логистика",
                "Дизайн",
                "Издательские услуги",
                "Исследования",
                "Консультационные услуги",
                "Красота и здоровье",
                "Курьерские услуги",
                "Маркетинг, реклама PR",
                "Общественное питание",
                "Охранные услуги",
                "Погрузочно-разгрузочные работы",
                "Посреднические услуги",
                "Проектирование",
                "Производство и изготовление",
                "Разработка ПО",
                "Реализация металлолома и прочих отходов",
                "Ремонт авто",
                "Репетиторы и обучение",
                "Реставрация",
                "Сельскохозяйственные услуги",
                "Строительство и ремонт",
                "Торговля",
                "Тренерские услуги",
                "Уборка и помощь по хозяйству",
                "Услуги для животных",
                "Услуги по страхованию",
                "Фото-видео услуги",
                "Юридические услуги"
        };
    }

    /* По идее это должно быть в другом классе с тестами на валидацию, но я решил не разбивать один тест по классам */
    @TmsLink("NAMEMIX-T1485")
    @Feature("NAMEMIX-T1485. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Категория компании - добавление отрасли")
    @Test(description = "Step 3. Добавление отрасли - проверка списка отраслей", groups = {"Checklist", "UI"},
            dataProvider = "companyCategories")
    public void companyCategoryList(String category) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.startEditCategoryBlock(false);
        InputDropDownList.checkOptionExists(onCompanyCategoryBlock.inputDdlCategory, category);
    }

    @TmsLink("NAMEMIX-T1485")
    @Feature("NAMEMIX-T1485. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Категория компании - добавление отрасли")
    @Test(description = "Подписант - редактирование", groups = {"Checklist", "UI"})
    public void companyCategoryEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfCategoryBlock("Строительство и ремонт", "13.37");
    }

}
