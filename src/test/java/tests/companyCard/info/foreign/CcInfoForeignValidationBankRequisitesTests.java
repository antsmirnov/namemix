package tests.companyCard.info.foreign;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.DataProviders;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static pageObject.pages.CompanyCardInfoPage.onBankRequisitesBlock;
import static testData.enums.CompanyForm.FOREIGN;

//@Features({@Feature("WebADMСайта"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1479. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Банковские реквизиты - валидация")
@TmsLink("NAMEMIX-T1479")
public class CcInfoForeignValidationBankRequisitesTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1479";
        companyValidation = new Company(FOREIGN)
                .withShortName("AUTO Tests Foreign Company " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    @Test(description = "Реквизиты банка - Валидация Step 1. Пустые поля", groups = {"Checklist", "UI"})
    public void bankRequisitesClearFieldsAndPressSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        companyCardInfoSteps.validationCompanyBankRequisitesMandatoryInputsCheck(FOREIGN);
    }

    @Test(description = "Реквизиты банка - Валидация Step 2. Название банка", groups = {"Checklist", "UI"})
    public void bankRequisitesBankNameValidation() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        companyCardInfoSteps.validationMaxLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputBank,
                onBankRequisitesBlock.promptBank,
                100
        );
    }

    @Test(description = "Реквизиты банка - Валидация Step 3. Адрес банка", groups = {"Checklist", "UI"})
    public void bankRequisitesBankAddressValidation() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        companyCardInfoSteps.validationAddressMaxLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputAddress,
                onBankRequisitesBlock.promptAddress,
                100
        );
    }

    @Test(description = "Реквизиты банка - Валидация Step 4. SWIFT (только из цифр и лат. букв)",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "nonDigitsNonLatinSmallSet")
    public void bankRequisitesBankSwiftAllowedSymbols(String value) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt(value, "Только латинские буквы и цифры");
    }

    @Test(description = "Реквизиты банка - Валидация Step 4. SWIFT (мин., макс. длина)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankSwiftMinMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        companyCardInfoSteps.validationMinLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputSwift,
                onBankRequisitesBlock.promptSwift,
                8, false, new StringGenerator().withLatin(false).withDigits(true));
        companyCardInfoSteps.validationMaxLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputSwift,
                onBankRequisitesBlock.promptSwift,
                11, false, new StringGenerator().withLatin(false).withDigits(true));
    }

    @Test(description = "Реквизиты банка - Step 5. Номер счета (Account number) (только цифры)",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "nonDigitsNonSpacesSmallSet")
    public void bankRequisitesBankAccountNumberInvalid(String input) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyBankRequisitesAccountNumberPrompt(input, "Только цифры");
    }

    @Test(description = "Реквизиты банка - Валидация Step 5. Номер счета (Account number) (мин., макс. длина)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesAccountNumberAccountNumberMinMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        companyCardInfoSteps.validationMinLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputAccountNumber,
                onBankRequisitesBlock.promptAccountNumber,
                8, false, new StringGenerator().withLatin(false).withDigits(true));
        companyCardInfoSteps.validationMaxLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputAccountNumber,
                onBankRequisitesBlock.promptAccountNumber,
                34, false, new StringGenerator().withLatin(false).withDigits(true));
    }

    @Test(description = "Реквизиты банка - Step 6. Банковский код (ACH Routing number)  (только цифры)",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "nonDigitsNonSpacesSmallSet")
    public void bankRequisitesBankRoutingNumberInvalid(String input) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyBankRequisitesRoutingNumberPrompt(input, "Только цифры");
    }

    @Test(description = "Реквизиты банка - Step 6. Банковский код (ACH Routing number)  (только цифры)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankRoutingNumberOnly9() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(true);
        companyCardInfoSteps.validationCompanyBankRequisitesRoutingNumberPrompt("12345678", "Количество символов - 9");
        companyCardInfoSteps.validationCompanyBankRequisitesRoutingNumberPrompt("1234567890", "Количество символов - 9");
        companyCardInfoSteps.isInputAllowedBankRequisitesRoutingNumber("123456789");
    }

}
