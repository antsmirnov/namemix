package tests.companyCard.info.foreign;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static pageObject.pages.CompanyCardInfoPage.onPodpisantBlock;
import static testData.enums.CompanyForm.FOREIGN;
import static utils.InputMethods.clearWithCheck;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1492. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Подписант - валидация")
@TmsLink("NAMEMIX-T1492")
public class CcInfoForeignValidationPodpisantTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1492";
        companyValidation = new Company(FOREIGN)
                .withShortName("AUTO Tests Foreign Company " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    @Test(description = "Step 1. Проверка обязательности полей",
            groups = {"Checklist", "UI"})
    public void podpisantMandatoryFields() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditPodpisantBlock(false);
        clearWithCheck(onPodpisantBlock.inputRole);
        clearWithCheck(onPodpisantBlock.inputTranscript);
        companyCardInfoSteps.clickSaveForBlock(onPodpisantBlock.container);
        companyCardInfoSteps.checkPromptMandatoryField(onPodpisantBlock.promptRole);
        companyCardInfoSteps.checkPromptMandatoryField(onPodpisantBlock.promptTranscript);
    }

    @Test(description = "Step 2. Должность. Максимальная длина строки 150",
            groups = {"Checklist", "UI"})
    public void podpisantRoleMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditPodpisantBlock();
        companyCardInfoSteps.validationMaxLength(
                onPodpisantBlock.container,
                onPodpisantBlock.inputRole,
                onPodpisantBlock.promptRole,
                150,
                true,
                new StringGenerator().withLatin(true).withCyrillic(true).withExtra("-")
        );
    }

    @Test(description = "Step 3. Расшифровка. Максимальная длина строки 150",
            groups = {"Checklist", "UI"})
    public void podpisantTranscriptionMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditPodpisantBlock();
        companyCardInfoSteps.validationMaxLength(
                onPodpisantBlock.container,
                onPodpisantBlock.inputTranscript,
                onPodpisantBlock.promptTranscript,
                150,
                true,
                new StringGenerator().withLatin(true).withCyrillic(true).withExtra("-")
        );
    }

}
