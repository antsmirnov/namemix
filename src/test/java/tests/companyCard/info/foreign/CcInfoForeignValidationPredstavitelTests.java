package tests.companyCard.info.foreign;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static pageObject.pages.CompanyCardInfoPage.onPredstavitelBlock;
import static testData.enums.CompanyForm.FOREIGN;
import static utils.InputMethods.clearWithCheck;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1495. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Представитель - валидация")
@TmsLink("NAMEMIX-T1495")
public class CcInfoForeignValidationPredstavitelTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1495";
        companyValidation = new Company(FOREIGN)
                .withShortName("AUTO Tests Foreign Company " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    @Test(description = "Step 1. Проверка обязательности полей",
            groups = {"Checklist", "UI"})
    public void predstavitelMandatoryFields() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditPredstavitelBlock(false);
        clearWithCheck(onPredstavitelBlock.inputFio);
        clearWithCheck(onPredstavitelBlock.inputBased);
        companyCardInfoSteps.clickSaveForBlock(onPredstavitelBlock.container);
        companyCardInfoSteps.checkPromptMandatoryField(onPredstavitelBlock.promptFio);
        companyCardInfoSteps.checkPromptMandatoryField(onPredstavitelBlock.promptBased);
    }

    @Test(description = "Step 2. В лице. Максимальная длина строки 150",
            groups = {"Checklist", "UI"})
    public void predstavitelFioMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditPredstavitelBlock();
        companyCardInfoSteps.validationMaxLength(
                onPredstavitelBlock.container,
                onPredstavitelBlock.inputFio,
                onPredstavitelBlock.promptFio,
                150,
                true,
                new StringGenerator().withLatin(true).withCyrillic(true).withExtra("-")
        );
    }

    @Test(description = "Step 3. На основании. Максимальная длина строки 500",
            groups = {"Checklist", "UI"})
    public void predstavitelBasedOnMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditPredstavitelBlock();
        companyCardInfoSteps.validationMaxLength(
                onPredstavitelBlock.container,
                onPredstavitelBlock.inputBased,
                onPredstavitelBlock.promptBased,
                500,
                true,
                new StringGenerator().withLatin(true).withCyrillic(true).withExtra("-")
        );
    }

}
