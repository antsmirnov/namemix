package tests.companyCard.info.foreign;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import steps.elements.InputEin;
import steps.elements.InputPhone;
import testData.DataProviders;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static com.codeborne.selenide.Condition.exactTextCaseSensitive;
import static com.codeborne.selenide.Selenide.$;
import static pageObject.pages.CompanyCardInfoPage.onRequisitesBlock;
import static testData.Values.*;
import static testData.enums.CompanyForm.FOREIGN;
import static utils.StringFunctions.randomEmail;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1498. Админ Наймикса/Компании/Карточка Иностранной Компании/Информация/Реквизиты компании - Валидация")
@TmsLink("NAMEMIX-T1498")
public class CcInfoForeignValidationRequisitesTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1498";
        companyValidation = new Company(FOREIGN)
                .withShortName("AUTO Tests Foreign Company " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    /* ================ Реквизиты компании ================ */

    @Test(description = "Реквизиты компании - Валидация Step 1. Пустые поля", groups = {"Checklist", "UI"})
    public void requisitesClearFieldsAndPressSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationCompanyRequisitesBlankInputsCheck(FOREIGN);
    }

    @Test(description = "Реквизиты компании - Валидация Step 2.1. Официальное название компании (разреш. символы)",
            groups = {"Checklist", "UI"})
    public void requisitesOfficialNameChars() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.isInputAllowedRequisitesOfficialName(latinAlphabetLC + " " + latinAlphabetUC);
        companyCardInfoSteps.isInputAllowedRequisitesOfficialName(cyrillicAlphabetLC + " " + cyrillicAlphabetUC);
        companyCardInfoSteps.isInputAllowedRequisitesOfficialName(digits);
        companyCardInfoSteps.isInputAllowedRequisitesOfficialName(keyboardSpecialSymbolsFiltered);
    }

    @Test(description = "Реквизиты компании - Валидация Step 2.1. Официальное название компании (разреш. символы)",
            groups = {"Checklist", "UI"})
    public void requisitesOfficialNameLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationMinLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputOfficialName,
                onRequisitesBlock.promptOfficialName,
                5
        );
        companyCardInfoSteps.validationMaxLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputOfficialName,
                onRequisitesBlock.promptOfficialName,
                250
        );
    }

    @Test(description = "Реквизиты компании - Валидация Step 3. Форма регистрации бизнеса", groups = {"Checklist", "UI"})
    public void requisitesBusinessRegForm() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationCompanyRequisitesFormOfRegDisabled();
    }

    @Test(description = "Реквизиты компании - Валидация Step 4.1 ФИО контактного лица. Проверка ввода разрешенных символов",
            groups = {"Checklist", "UI"})
    public void requisitesFioContactorAllowedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.isInputAllowedRequisitesContactorFio(
                latinAlphabetUC + " - " + latinAlphabetLC);
        companyCardInfoSteps.isInputAllowedRequisitesContactorFio(
                cyrillicAlphabetUC + " - " + cyrillicAlphabetLC);
    }
    @Test(description = "Реквизиты компании - Валидация Step 4.2 ФИО контактного лица. Макс. длина строки 100 символов",
            groups = {"Checklist", "UI"})
    public void requisitesFioContactorMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationMaxLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputFioContactPerson,
                onRequisitesBlock.promptFioContactPerson,
                100,
                false,
                new StringGenerator());
    }
    @Test(description = "Реквизиты компании - Валидация Step 4.3 ФИО контактного лица. Запрещен ввод других символов",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "charsNotAllowedInNonRussianFioShortSet")
    public void requisitesFioContactorRestrictedSymbols(String c) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesFioContactorPrompt(c, "Только латиница, кириллица и знаки -");
    }

    @Test(description = "Реквизиты компании - Валидация Step 5. Номер телефона контактного лица",
            groups = {"Checklist", "UI"})
    public void requisitesContactorPhoneRestrictedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        InputPhone.inputPhoneValidate(onRequisitesBlock.inputPhoneContactPerson, true);
    }

    @Test(description = "Реквизиты компании - Валидация Step 6.5 Email контактного лица (валидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsValid")
    public void requisitesContactorEmail(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesContactorEmail(email);
    }
    @Test(description = "Реквизиты компании - Валидация Step 6.5 Email контактного лица (невалидный)'",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsInvalid")
    public void requisitesContactorEmailInvalid(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesEmailContactorPrompt(email, "Введен некорректный email");
    }
    @Test(description = "Реквизиты компании - Валидация Step 6.5 Email контактного лица (проверка длины)",
            groups = {"Checklist", "UI"})
    public void requisitesContactorEmailLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesContactorEmail(randomEmail(320));
        companyCardInfoSteps.validationCompanyRequisitesEmailContactorPrompt(randomEmail(321), "Максимальная длина - 320 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 7. Фактический адрес",
            groups = {"Checklist", "UI"})
    public void requisitesActualAddress() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationAddressMinLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputActualAddress,
                onRequisitesBlock.promptActualAddress,
                5);
        companyCardInfoSteps.validationAddressMaxLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputActualAddress,
                onRequisitesBlock.promptActualAddress,
                250);
    }
    @Test(description = "Реквизиты компании - Валидация Step 8. Адрес регистрации",
            groups = {"Checklist", "UI"})
    public void requisitesRegisteredAddress() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationAddressMinLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputRegisteredAddress,
                onRequisitesBlock.promptRegisteredAddress,
                5);
        companyCardInfoSteps.validationAddressMaxLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputRegisteredAddress,
                onRequisitesBlock.promptRegisteredAddress,
                250);
    }

    @Test(description = "Реквизиты компании - Валидация Step 9. Номер телефона компании (только цифры)",
            groups = {"Checklist", "UI"})
    public void requisitesPhoneCompanyRestrictedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        InputPhone.inputPhoneValidate(onRequisitesBlock.inputPhone, true);
    }

    @Test(description = "Реквизиты компании - Валидация Step 10.1 Email компании (валидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsValid")
    public void requisitesCompanyEmail(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesCompanyEmail(email);
    }
    @Test(description = "Реквизиты компании - Валидация Step 10.2 Email компании (невалидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsInvalid")
    public void requisitesCompanyEmailInvalid(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesCompanyEmailPrompt(email, "Введен некорректный email");
    }
    @Test(description = "Реквизиты компании - Валидация Step 10.3 Email компании (проверка длины)",
            groups = {"Checklist", "UI"})
    public void requisitesCompanyEmailLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesCompanyEmail(randomEmail(320));
        companyCardInfoSteps.validationCompanyRequisitesCompanyEmailPrompt(randomEmail(321), "Максимальная длина - 320 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 11.1 EIN - невозможность ввода НЕ цифр",
            groups = {"Checklist", "UI"})
    public void requisitesEinNotAllowedChars() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        InputEin.inputWithCheck(onRequisitesBlock.inputEin, nonDigitsBigSet, "");
    }
    @Test(description = "Реквизиты компании - Валидация Step 11.2 EIN - Количество введенных знаков равно 9",
            groups = {"Checklist", "UI"})
    public void requisitesEinOnlyNineDigits() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);

        /* меньше 9 */
        InputEin.inputWithCheck(onRequisitesBlock.inputEin, "12345678");
        companyCardInfoSteps.clickSaveForBlock(onRequisitesBlock.container);
        $(onRequisitesBlock.promptEin).shouldHave(exactTextCaseSensitive("EIN состоит из 9 цифр"));

        /* больше 9 */
        InputEin.inputWithCheck(onRequisitesBlock.inputEin, "123456789012345", "12-3456789");
    }

    @Test(description = "Реквизиты компании - Валидация Step 11.3 EIN - Маска XX-XXXXXXX",
            groups = {"Checklist", "UI"})
    public void requisitesEinMask() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        InputEin.inputWithCheck(onRequisitesBlock.inputEin, "123456789", "12-3456789");
    }
}
