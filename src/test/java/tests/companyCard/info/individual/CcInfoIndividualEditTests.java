package tests.companyCard.info.individual;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.BankRequisites;
import testData.models.Company;
import testData.models.Phone;

import static testData.enums.CompanyForm.INDIVIDUAL;
import static utils.Common.randomInt;

/*
Карточка компании - информация - ИП

NAMEMIX-T1514 NAMEMIX-T1517 NAMEMIX-T1527 NAMEMIX-T1530 NAMEMIX-T1533
(валидация в других файлах)
*/

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
public class CcInfoIndividualEditTests extends BaseTest {

    static public Company companyBeforeEdit, companyWithEditedRequisites;
    static public BankRequisites oldBankRequisites, newBankRequisites;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1514-T533 Edit";
        companyBeforeEdit = new Company(INDIVIDUAL)
                .withFioIp("ФИО до редактирования")
                .withContactorFio("ФИО КЛ до редактирования")
                .withContactorPhone(new Phone("+70000000001"))
                .withContactorEmail("first@email.ru")
                .withAddressRegistered("414017, Астраханская обл, г Астрахань, Начальный пер")
                .withPhoneOfCompanyOrIp(new Phone("+70000000501"))
                .withEmailOfCompanyOrIp("first.individual@email.com")
                .withOgrnIp("301669422710317")
                .withInn("549643653690");
        companyWithEditedRequisites = new Company(INDIVIDUAL)
                .withFioIp("ФИО после редактирования")
                .withContactorFio("ФИО КЛ после редактирования")
                .withContactorPhone(new Phone("+70000000002"))
                .withContactorEmail("second@email.ru")
                .withAddressRegistered("366311, Чеченская Респ, Шалинский р-н, село Мескер-Юрт, ул Финальная")
                .withPhoneOfCompanyOrIp(new Phone("+70000000502"))
                .withEmailOfCompanyOrIp("second.individual@email.com")
                .withOgrnIp("303868665701208")
                .withInn("608756961813");

        oldBankRequisites = new BankRequisites()
                .withBank("Имя банка до редактирования " + randomInt(1, 99))
                .withBik("044525411")
                .withCheckAccount("40703810801080000017")
                .withCorrAccount("30101810145250000411");
        newBankRequisites = new BankRequisites()
                .withBank("Новое имя")
                .withBankAddress("Новый адрес")
                .withBik("044525225")
                .withCheckAccount("40702810638050013199")
                .withCorrAccount("30101810400000000225");

        recreateTempCompany(companyBeforeEdit);
    }

    /* Редактирование */

    @TmsLink("NAMEMIX-T1533")
    @Feature("NAMEMIX-T1533. Админ Наймикса/Компании/Карточка компании ИП/Информация/Реквизиты компании- Редактирование")
    @Test(description = "Реквизиты компании - Редактирование", groups = {"Checklist", "UI"})
    public void companyRequisitesEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfRequisitesBlock(companyWithEditedRequisites);
    }

    @TmsLink("NAMEMIX-T1514")
    @Feature("NAMEMIX-T1514. Админ Наймикса/Компании/Карточка компании ИП/Информация/Банковские реквизиты- редактирование")
    @Test(description = "Банковские реквизиты - редактирование", groups = {"Checklist", "UI"})
    public void companyBankRequisitesEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfBankRequisitesBlock(newBankRequisites);
    }

    @TmsLink("NAMEMIX-T1517")
    @Feature("NAMEMIX-T1517. Админ Наймикса/Компании/Карточка компании ИП/Информация/Главный бухгалтер- редактирование")
    @Test(description = "Главный бухгалтер - редактирование", groups = {"Checklist", "UI"})
    public void companyMainAccountantEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfMainAccountantBlock("Изменённое Имя С-де-фи-сом", new Phone("+73388888888"));
    }

    @TmsLink("NAMEMIX-T1530")
    @Feature("NAMEMIX-T1530. Админ Наймикса/Компании/Карточка компании ИП/Информация/Представитель - редактирование")
    @Test(description = "Представитель - редактирование", groups = {"Checklist", "UI"})
    public void companyPredstavitelEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfPredstavitelBlock("Изменённое Имя С-де-фи-сом", "Изменённый текст");
    }

    @TmsLink("NAMEMIX-T1527")
    @Feature("NAMEMIX-T1527. Админ Наймикса/Компании/Карточка компании ИП/Информация/Подписант- редактирование")
    @Test(description = "Подписант - редактирование", groups = {"Checklist", "UI"})
    public void companyPodpisantEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfPodpisantBlock("Должность после", "Расшифровка после");
    }
}