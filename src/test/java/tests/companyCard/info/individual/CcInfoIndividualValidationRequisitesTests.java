package tests.companyCard.info.individual;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import steps.elements.InputPhone;
import testData.DataProviders;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static pageObject.pages.CompanyCardInfoPage.onRequisitesBlock;
import static steps.elements.InputPhone.clearAndEnterPhoneWithExpectedResultCheck;
import static testData.Values.*;
import static testData.enums.CompanyForm.INDIVIDUAL;
import static utils.StringFunctions.*;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1531. Админ Наймикса/Компании/Карточка компании ИП/Информация/Реквизиты компании - Валидация")
@TmsLink("NAMEMIX-T1531")
public class CcInfoIndividualValidationRequisitesTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1531";
        companyValidation = new Company(INDIVIDUAL)
                .withFioIp("AUTO Валидация " + testCode);

        recreateTempCompany(companyValidation);
    }

    /* ================ Реквизиты компании ================ */

    @Test(description = "Реквизиты компании - Валидация Step 1. Пустые поля", groups = {"Checklist", "UI"})
    public void requisitesClearFieldsAndPressSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationCompanyRequisitesBlankInputsCheck(INDIVIDUAL);
    }
    @Test(description = "Реквизиты компании - Валидация Step 2 ФИО ИП. Проверка ввода разрешенных символов",
            groups = {"Checklist", "UI"})
    public void requisitesFioIpAllowedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.isInputAllowedFioIp(latinAlphabetLC + " " + latinAlphabetUC);
        companyCardInfoSteps.isInputAllowedFioIp(cyrillicAlphabetLC + " " + cyrillicAlphabetUC);
        companyCardInfoSteps.isInputAllowedFioIp(digits);
        companyCardInfoSteps.isInputAllowedFioIp("~`!@#№$%^&*()-_+={}|/:;\"',.");
        companyCardInfoSteps.isInputAllowedFioIp("5симв"); // минимальная длина
        companyCardInfoSteps.validationCompanyRequisitesFioIpPrompt("4сим", "Минимальная длина строки 5 символов");
        companyCardInfoSteps.validationMaxLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputFioIp,
                onRequisitesBlock.promptFioIp,
                250, false,
                new StringGenerator().withLatin(false).withCyrillic(true).withExtra("-"));
    }

    @Test(description = "Реквизиты компании - Валидация Step 3. Форма регистрации бизнеса", groups = {"Checklist", "UI"})
    public void requisitesBusinessRegForm() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationCompanyRequisitesFormOfRegDisabled();
    }
    @Test(description = "Реквизиты компании - Валидация Step 4.1 ФИО контактного лица. Проверка ввода разрешенных символов",
            groups = {"Checklist", "UI"})
    public void requisitesFioContactorAllowedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.isInputAllowedRequisitesContactorFio(
                cyrillicAlphabetUC + " - " + cyrillicAlphabetLC);
    }

    @Test(description = "Реквизиты компании - Валидация Step 4.2 ФИО контактного лица. Макс. длина строки 100 символов",
            groups = {"Checklist", "UI"})
    public void requisitesFioContactorMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationMaxLength(
                onRequisitesBlock.container,
                onRequisitesBlock.inputFioContactPerson,
                onRequisitesBlock.promptFioContactPerson,
                100,
                false,
                new StringGenerator().withLatin(false).withCyrillic(true).withExtra("-")
        );

    }
    @Test(description = "Реквизиты компании - Валидация Step 4.3 ФИО контактного лица. Запрещен ввод других символов",
            groups = {"Checklist", "UI"}, dataProvider = "charsNotAllowedInFioShortSet", dataProviderClass = DataProviders.class)
    public void requisitesFioContactorRestrictedSymbols(String c) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesFioContactorPrompt(c, "Только кириллица и знаки -");
    }
    @Test(description = "Реквизиты компании - Валидация Step 5.4 Номер телефона контактного лица(только цифры",
            groups = {"Checklist", "UI"})
    public void requisitesContactorPhoneRestrictedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        String emptyPhone = "+7 ___ ___ ____";
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, keyboardSpecialSymbols, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, cyrillicAlphabetLC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, cyrillicAlphabetUC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, latinAlphabetLC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, latinAlphabetUC, emptyPhone);
    }
    @Test(description = "Реквизиты компании - Валидация Step 5.4 Номер телефона контактного лица (только 10 символов)",
            groups = {"Checklist", "UI"})
    public void requisitesContactorPhone10DigitsOnly() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson,
                "12345678901234567890", "+7 123 456 7890");
    }

    @Test(description = "Реквизиты компании - Валидация Step 6.5 Email контактного лица (валидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsValid")
    public void requisitesContactorEmail(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesContactorEmail(email);
    }
    @Test(description = "Реквизиты компании - Валидация Step 6.5 Email контактного лица (невалидный)'",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsInvalid")
    public void requisitesContactorEmailInvalid(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesEmailContactorPrompt(email, "Введен некорректный email");
    }
    @Test(description = "Реквизиты компании - Валидация Step 6.5 Email контактного лица (проверка длины)",
            groups = {"Checklist", "UI"})
    public void requisitesContactorEmailLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesContactorEmail(randomEmail(320));
        companyCardInfoSteps.validationCompanyRequisitesEmailContactorPrompt(randomEmail(321), "Максимальная длина - 320 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 7 Адрес регистрации",
            groups = {"Checklist", "UI"})
    public void requisitesRegisteredAddress() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesRegisteredAddress(
                new StringGenerator().generateDescribed(250, "Ровно 250 символов"));
        companyCardInfoSteps.validationCompanyRequisitesRegisteredAddressPrompt(
                new StringGenerator().generateDescribed(251, "251 символ"),
                "Максимальная длина - 250 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 8.1 Номер телефона компании (только цифры)",
            groups = {"Checklist", "UI"})
    public void requisitesPhoneCompany() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        InputPhone.inputPhoneValidate(onRequisitesBlock.inputCompanyPhone);
    }
    @Test(description = "Реквизиты компании - Валидация Step 9.1 Email компании (валидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsValid")
    public void requisitesCompanyEmail(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesCompanyEmail(email);
    }
    @Test(description = "Реквизиты компании - Валидация Step 9.2 Email компании (невалидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsInvalid")
    public void requisitesCompanyEmailInvalid(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesCompanyEmailPrompt(email, "Введен некорректный email");
    }
    @Test(description = "Реквизиты компании - Валидация Step 9.1 Email компании (проверка длины)",
            groups = {"Checklist", "UI"})
    public void requisitesCompanyEmailLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesCompanyEmail(randomEmail(320));
        companyCardInfoSteps.validationCompanyRequisitesCompanyEmailPrompt(randomEmail(321), "Максимальная длина - 320 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 10.1 ОГРНИП - Проверка валидного значения",
            groups = {"Checklist", "UI"})
    public void requisitesOgrnIpValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesOgrn(randomOgrnIpValid());
    }
    @Test(description = "Реквизиты компании - Валидация Step 10.2 ОГРНИП - Проверка невалидных значений",
            groups = {"Checklist", "UI"})
    public void requisitesOgrnIpInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt(randomOgrnIpInvalid(), "ОГРНИП введен неверно");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt("12345678901234", "ОГРНИП может состоять только из 15 цифр");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt("1234567890123456", "ОГРНИП может состоять только из 15 цифр");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt("AZazАЯаяЁёxyz", "ОГРНИП может состоять только из 15 цифр");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt(keyboardSpecialSymbols, "ОГРНИП может состоять только из 15 цифр");
    }

    @Test(description = "Реквизиты компании - Валидация Step 11.1 ИНН - Проверка валидного значения",
            groups = {"Checklist", "UI"})
    public void requisitesInnLeValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesInnLe(randomInnPersonValid());
    }
    @Test(description = "Реквизиты компании - Валидация Step 11.2 ИНН - Проверка невалидных значений",
            groups = {"Checklist", "UI"})
    public void requisitesInnLeInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt(randomInnPerson(true, false), "Неверный формат ИНН"); // Невалидная 12-я цифра
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt(randomInnPerson(false, true), "Неверный формат ИНН"); // Невалидная 11-я цифра
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("123456789", "ИНН может состоять только из 12 цифр");
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("12345678901", "ИНН может состоять только из 12 цифр");
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("AZazАЯаяЁёxyz", "ИНН может состоять только из цифр");
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("keyboardSpecialSymbols", "ИНН может состоять только из цифр");
    }

}
