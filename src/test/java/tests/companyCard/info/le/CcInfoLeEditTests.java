package tests.companyCard.info.le;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.BankRequisites;
import testData.models.Company;
import testData.models.Phone;

import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.Common.randomInt;
import static utils.StringFunctions.randomFio;

/*
Карточка компании - информация - ЮЛ

NAMEMIX-T1342 NAMEMIX-T1343 NAMEMIX-T1344 NAMEMIX-T1345 NAMEMIX-T1478
(только кейсы на редактирование, валидация в другом файле)
*/

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
public class CcInfoLeEditTests extends BaseTest {

    static public Company companyBeforeEdit, companyWithEditedRequisites;
    static public BankRequisites oldBankRequisites, newBankRequisites;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1342-T1478 Edit";
        companyBeforeEdit = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testCode)
                .withOfficialName("Изначальное имя" + testCode)
                .withContactorFio(randomFio())
                .withContactorPhone(new Phone("+70045678900"))
                .withContactorEmail("first@email.com")
                .withAddressActual("414017, Астраханская обл, г Астрахань, Начальный пер")
                .withAddressRegistered("420075, Респ Татарстан, г Казань, ул Начальная")
                .withPhoneOfCompanyOrIp(new Phone("+70045678901"))
                .withEmailOfCompanyOrIp("first.company@email.ru")
                .withOgrn("5147519566519")
                .withKpp("111111111")
                .withInn("4714124418");
        companyWithEditedRequisites = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testCode)
                .withOfficialName("Изменённое имя" + testCode)
                .withContactorFio("Отредактированное Контактное Лицо")
                .withContactorPhone(new Phone("+70045678903"))
                .withContactorEmail("changed@email.com")
                .withAddressActual("243160, Брянская обл, Красногорский р-н, поселок Новая Москва")
                .withAddressRegistered("169945, Респ Коми, г Воркута, деревня Елец")
                .withPhoneOfCompanyOrIp(new Phone("+70045678904"))
                .withEmailOfCompanyOrIp("changed.company@email.ru")
                .withOgrn("1067032606860")
                .withKpp("222222222")
                .withInn("7186401093");

        oldBankRequisites = new BankRequisites()
                .withBank("Имя банка до редактирования " + randomInt(1, 99))
                .withBik("044525411")
                .withCheckAccount("40703810801080000017")
                .withCorrAccount("30101810145250000411");
        newBankRequisites = new BankRequisites()
                .withBank("Новое имя")
                .withBankAddress("Новый адрес")
                .withBik("044525225")
                .withCheckAccount("40702810638050013199")
                .withCorrAccount("30101810400000000225");

        recreateTempCompany(companyBeforeEdit);
    }

    /* Редактирование */

    @TmsLink("NAMEMIX-T1342")
    @Feature("NAMEMIX-T1342. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Реквизиты компании- Редактирование")
    @Test(description = "Реквизиты компании - Редактирование", groups = {"Checklist", "UI"})
    public void companyRequisitesEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfRequisitesBlock(companyWithEditedRequisites);
    }

    @TmsLink("NAMEMIX-T1343")
    @Feature("NAMEMIX-T1343. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Банковские реквизиты- редактирование")
    @Test(description = "Банковские реквизиты - редактирование", groups = {"Checklist", "UI"})
    public void companyBankRequisitesEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfBankRequisitesBlock(newBankRequisites);
    }

    @TmsLink("NAMEMIX-T1344")
    @Feature("NAMEMIX-T1344. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Руководитель компании - редактирование")
    @Test(description = "Руководитель компании - редактирование", groups = {"Checklist", "UI"})
    public void companyDirectorEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfDirectorBlock("Изменённое Имя С-де-фи-сом", new Phone("+73388888888"));
    }

    @TmsLink("NAMEMIX-T1345")
    @Feature("NAMEMIX-T1345. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Главный бухгалтер- редактирование")
    @Test(description = "Главный бухгалтер - редактирование", groups = {"Checklist", "UI"})
    public void companyMainAccountantEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfMainAccountantBlock("Изменённое Имя С-де-фи-сом", new Phone("+73388888888"));
    }

    @TmsLink("NAMEMIX-T1475")
    @Feature("NAMEMIX-T1475. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Представитель - редактирование")
    @Test(description = "Представитель - редактирование", groups = {"Checklist", "UI"})
    public void companyPredstavitelEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfPredstavitelBlock("Изменённое Имя С-де-фи-сом", "Изменённый текст");
    }

    @TmsLink("NAMEMIX-T1478")
    @Feature("NAMEMIX-T1478. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Подписант- редактирование")
    @Test(description = "Подписант - редактирование", groups = {"Checklist", "UI"})
    public void companyPodpisantEditSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyBeforeEdit);
        companyCardInfoSteps.checkEditOfPodpisantBlock("Должность после", "Расшифровка после");
    }

}
