package tests.companyCard.info.le;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.DataProviders;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static pageObject.pages.CompanyCardInfoPage.onBankRequisitesBlock;
import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.InputMethods.enterWithCheck;
import static utils.StringFunctions.*;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1347. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Банковские реквизиты - валидация")
@TmsLink("NAMEMIX-T1347")
public class CcInfoLeValidationBankRequisitesTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1347";
        companyValidation = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    @Test(description = "Реквизиты банка - Валидация Step 1. Пустые поля", groups = {"Checklist", "UI"})
    public void bankRequisitesClearFieldsAndPressSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        companyCardInfoSteps.validationCompanyBankRequisitesMandatoryInputsCheck(LEGAL_ENTITY);
    }

    @Test(description = "Реквизиты банка - Валидация Step 2. Название банка", groups = {"Checklist", "UI"})
    public void bankRequisitesBankNameValidation() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        companyCardInfoSteps.validationMaxLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputBank,
                onBankRequisitesBlock.promptBank,
                100
        );
    }

    @Test(description = "Реквизиты банка - Валидация Step 3. Адрес банка", groups = {"Checklist", "UI"})
    public void bankRequisitesBankAddressValidation() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        companyCardInfoSteps.validationAddressMaxLength(
                onBankRequisitesBlock.container,
                onBankRequisitesBlock.inputAddress,
                onBankRequisitesBlock.promptAddress,
                100
        );
    }

    @Test(description = "Реквизиты банка - Валидация Step 6. БИК (только из цифр)",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "nonDigitsNonSpacesSmallSet")
    public void bankRequisitesBankBikDigitsOnly(String value) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt(value, "БИК может состоять только из цифр");
    }

    @Test(description = "Реквизиты банка - Валидация Step 6. БИК (невалидный)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankBikInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt("04345678", "БИК может состоять только из 9 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt("0434567890", "БИК может состоять только из 9 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt("030000999", "БИК должен начинаться с 04");
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt("050000050", "БИК должен начинаться с 04");
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt("040000000", "Три крайних цифры должны быть в диапазоне от 050 до 999");
        companyCardInfoSteps.validationCompanyBankRequisitesBikPrompt("040000049", "Три крайних цифры должны быть в диапазоне от 050 до 999");
    }

    @Test(description = "Реквизиты банка - Валидация Step 6. БИК (валидный)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankBikValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(true);
        companyCardInfoSteps.isInputAllowedBankRequisitesBik("041234050");
        companyCardInfoSteps.isInputAllowedBankRequisitesBik("049999051");
        companyCardInfoSteps.isInputAllowedBankRequisitesBik("040000998");
        companyCardInfoSteps.isInputAllowedBankRequisitesBik("040000999");
    }

    @Test(description = "Реквизиты банка - Валидация Step 4. р/с (невалидный)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankCheckingAccountInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        String bik = randomBikValid();
        enterWithCheck(onBankRequisitesBlock.inputBik, bik);
        companyCardInfoSteps.validationCompanyBankRequisitesCheckingAccountPrompt(
                randomCheckingAccount(bik, false), "Неправильное контрольное число");
        companyCardInfoSteps.validationCompanyBankRequisitesCheckingAccountPrompt(
                randomCheckingAccount(bik, true).substring(0, 19),          // 20-1
                "Р/С может состоять только из 20 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesCheckingAccountPrompt(
                randomCheckingAccount(bik, true) + "1",                       // 20+1
                "Р/С может состоять только из 20 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesCheckingAccountPrompt(
                "1",                                                             // 1
                "Р/С может состоять только из 20 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesCheckingAccountPrompt(
                new StringGenerator().withLatinLowerCase(false).withDigits(true).generate(1000), // 1000
                "Р/С может состоять только из 20 цифр");
    }

    @Test(description = "Реквизиты банка - Валидация Step 4. р/с (только из цифр)",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "nonDigitsNonSpacesSmallSet")
    public void bankRequisitesBankCheckingAccountNonDigits(String value) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        String bik = randomBikValid();
        enterWithCheck(onBankRequisitesBlock.inputBik, bik);
        companyCardInfoSteps.validationCompanyBankRequisitesCheckingAccountPrompt(value, "Р/С может состоять только из цифр");
    }

    @Test(description = "Реквизиты банка - Валидация Step 4. р/с (валидный)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankCheckingAccountValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        String bik = randomBikValid();
        enterWithCheck(onBankRequisitesBlock.inputBik, bik);
        companyCardInfoSteps.isInputAllowedBankRequisitesCheckingAccount(randomCheckingAccount(bik, true));
    }

    @Test(description = "Реквизиты банка - Валидация Step 5. к/с (невалидный)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankCorrespondentAccountInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        String bik = randomBikValid();
        enterWithCheck(onBankRequisitesBlock.inputBik, bik);
        companyCardInfoSteps.validationCompanyBankRequisitesCorrespondentAccountPrompt(
                randomCorrespondentAccount(bik, false), "Неправильное контрольное число");
        companyCardInfoSteps.validationCompanyBankRequisitesCorrespondentAccountPrompt(
                randomCorrespondentAccount(bik, true).substring(0, 19),          // 20-1
                "К/С может состоять только из 20 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesCorrespondentAccountPrompt(
                randomCorrespondentAccount(bik, true) + "1",                       // 20+1
                "К/С может состоять только из 20 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesCorrespondentAccountPrompt(
                "1",                                                             // 1
                "К/С может состоять только из 20 цифр");
        companyCardInfoSteps.validationCompanyBankRequisitesCorrespondentAccountPrompt(
                new StringGenerator().withLatinLowerCase(false).withDigits(true).generate(1000), // 1000
                "К/С может состоять только из 20 цифр");
    }

    @Test(description = "Реквизиты банка - Валидация Step 5. к/с (только из цифр)",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "nonDigitsNonSpacesSmallSet")
    public void bankRequisitesBankCorrespondentAccountNonDigits(String value) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock(false);
        String bik = randomBikValid();
        enterWithCheck(onBankRequisitesBlock.inputBik, bik);
        companyCardInfoSteps.validationCompanyBankRequisitesCorrespondentAccountPrompt(value, "К/С может состоять только из цифр");
    }

    @Test(description = "Реквизиты банка - Валидация Step 5. к/с (валидный)",
            groups = {"Checklist", "UI"})
    public void bankRequisitesBankCorrespondentAccountValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditBankRequisitesBlock();
        String bik = randomBikValid();
        enterWithCheck(onBankRequisitesBlock.inputBik, bik);
        companyCardInfoSteps.isInputAllowedBankRequisitesCorrespondentAccount(randomCorrespondentAccount(bik, true));
    }

}
