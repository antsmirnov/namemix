package tests.companyCard.info.le;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.DataProviders;
import testData.enums.UserRole;
import testData.models.Company;
import testData.models.Phone;
import utils.StringGenerator;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;
import static pageObject.pages.CompanyCardInfoPage.onDirectorBlock;
import static service.CustomAssert.assertThat;
import static service.Matchers.notSpecified;
import static steps.elements.InputPhone.clearPhoneWithCheck;
import static steps.elements.InputPhone.inputPhoneValidate;
import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.InputMethods.clearWithCheck;
import static utils.StringFunctions.randomFio;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1348. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Руководитель компании - валидация")
@TmsLink("NAMEMIX-T1348")
public class CcInfoLeValidationDirectorTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1348";
        companyValidation = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    @Test(description = "Step 1. Проверка необязательности полей",
            groups = {"Checklist", "UI"})
    public void directorMandatoryFields() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditDirectorBlock();
        companyCardInfoSteps.inputDirectorBlock(randomFio(), new Phone("+70000000000"));
        companyCardInfoSteps.saveDirectorAndCheckNotification();

        companyCardInfoSteps.startEditDirectorBlock();
        clearWithCheck(onDirectorBlock.inputFio);
        clearPhoneWithCheck(onDirectorBlock.inputPhone);
        companyCardInfoSteps.saveDirectorAndCheckNotification();
        refresh();
        assertThat($(onDirectorBlock.textFio).getText(), notSpecified());
        assertThat($(onDirectorBlock.textPhone).getText(), notSpecified());
    }

    @Test(description = "Step 2. Фио руководителя. Максимальная длина строки 150",
            groups = {"Checklist", "UI"})
    public void directorFioMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditDirectorBlock();
        companyCardInfoSteps.validationMaxLength(
                onDirectorBlock.container,
                onDirectorBlock.inputFio,
                onDirectorBlock.promptFio,
                150,
                false,
                new StringGenerator().withLatin(false).withCyrillic(true).withExtra("-")
        );
    }

    @Test(description = "Step 2. Фио руководителя. Валидные символы",
            groups = {"Checklist", "UI"})
    public void directorFioAllowedChars() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditDirectorBlock();
        companyCardInfoSteps.isInputAllowedDirectorFio(randomFio(true));
    }

    @Test(description = "Step 2. Фио руководителя. Невалидные символы",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "charsNotAllowedInFioShortSet")
    public void directorFioNotAllowedChars(String text) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditDirectorBlock(false);
        companyCardInfoSteps.validationCompanyDirectorFioPrompt(text, "Только кириллица и знаки -");
    }

    @Test(description = "Step 3. Номер телефона руководителя. Валидация",
            groups = {"Checklist", "UI"})
    public void directorPhone() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditDirectorBlock(false);
        inputPhoneValidate(onDirectorBlock.inputPhone);
    }

}
