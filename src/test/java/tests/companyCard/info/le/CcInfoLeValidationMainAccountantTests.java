package tests.companyCard.info.le;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.DataProviders;
import testData.enums.UserRole;
import testData.models.Company;
import testData.models.Phone;
import utils.StringGenerator;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;
import static pageObject.pages.CompanyCardInfoPage.onMainAccountantBlock;
import static service.CustomAssert.assertThat;
import static service.Matchers.notSpecified;
import static steps.elements.InputPhone.clearPhoneWithCheck;
import static steps.elements.InputPhone.inputPhoneValidate;
import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.InputMethods.clearWithCheck;
import static utils.StringFunctions.randomFio;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1349. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Главный бухгалтер - валидация")
@TmsLink("NAMEMIX-T1349")
public class CcInfoLeValidationMainAccountantTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1349";
        companyValidation = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    @Test(description = "Step 1. Проверка необязательности полей",
            groups = {"Checklist", "UI"})
    public void mainAccountantMandatoryFields() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditMainAccountantBlock(false);
        companyCardInfoSteps.inputMainAccountantBlock(randomFio(), new Phone("+70000000000"));
        companyCardInfoSteps.saveMainAccountantAndCheckNotification();

        companyCardInfoSteps.startEditMainAccountantBlock();
        clearWithCheck(onMainAccountantBlock.inputFio);
        clearPhoneWithCheck(onMainAccountantBlock.inputPhone);
        companyCardInfoSteps.saveMainAccountantAndCheckNotification();
        refresh();
        assertThat($(onMainAccountantBlock.textFio).getText(), notSpecified());
        assertThat($(onMainAccountantBlock.textPhone).getText(), notSpecified());
    }

    @Test(description = "Step 2. ФИО главного бухгалтера. Максимальная длина строки 150",
            groups = {"Checklist", "UI"})
    public void mainAccountantFioMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditMainAccountantBlock();
        companyCardInfoSteps.validationMaxLength(
                onMainAccountantBlock.container,
                onMainAccountantBlock.inputFio,
                onMainAccountantBlock.promptFio,
                150,
                false,
                new StringGenerator().withLatin(false).withCyrillic(true).withExtra("-")
        );
    }

    @Test(description = "Step 2. ФИО главного бухгалтера. Валидные символы",
            groups = {"Checklist", "UI"})
    public void mainAccountantFioAllowedChars() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditMainAccountantBlock();
        companyCardInfoSteps.isInputAllowedMainAccountantFio(randomFio(true));
    }

    @Test(description = "Step 2. Фио главного бухгалтера. Невалидные символы",
            groups = {"Checklist", "UI"},
            dataProviderClass = DataProviders.class, dataProvider = "charsNotAllowedInFioShortSet")
    public void mainAccountantFioNotAllowedChars(String text) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditMainAccountantBlock(false);
        companyCardInfoSteps.validationCompanyMainAccountantFioPrompt(text, "Только кириллица и знаки -");
    }

    @Test(description = "Step 3. Номер телефона главного бухгалтера. Валидация",
            groups = {"Checklist", "UI"})
    public void mainAccountantPhone() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditMainAccountantBlock(false);
        inputPhoneValidate(onMainAccountantBlock.inputPhone);
    }

}
