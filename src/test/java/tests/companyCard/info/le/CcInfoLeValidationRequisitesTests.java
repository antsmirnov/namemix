package tests.companyCard.info.le;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.DataProviders;
import testData.enums.UserRole;
import testData.models.Company;
import utils.StringGenerator;

import static pageObject.pages.CompanyCardInfoPage.onRequisitesBlock;
import static steps.elements.InputPhone.clearAndEnterPhoneWithExpectedResultCheck;
import static testData.Values.*;
import static testData.enums.CompanyForm.LEGAL_ENTITY;
import static utils.StringFunctions.*;

//@Features({@Feature("WebADMСайта"), @Feature("Юр. лицо"), @Feature("Карточка компании - Инфо")})
@Feature("NAMEMIX-T1346. Админ Наймикса/Компании/Карточка компании ЮЛ/Информация/Реквизиты компании - Валидация")
@TmsLink("NAMEMIX-T1346")
public class CcInfoLeValidationRequisitesTests extends BaseTest {

    static public Company companyValidation;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1346";
        companyValidation = new Company(LEGAL_ENTITY)
                .withShortName("AUTO Tests LeCompany " + testCode + " validation")
                .withOfficialName("Компания для валидации полей" + testCode);

        recreateTempCompany(companyValidation);
    }

    /* ================ Реквизиты компании ================ */

    @Test(description = "Реквизиты компании - Валидация Step 1. Пустые поля", groups = {"Checklist", "UI"})
    public void requisitesClearFieldsAndPressSave() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationCompanyRequisitesBlankInputsCheck(LEGAL_ENTITY);
    }
    @Test(description = "Реквизиты компании - Валидация Step 2. Форма регистрации бизнеса", groups = {"Checklist", "UI"})
    public void requisitesBusinessRegForm() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.validationCompanyRequisitesFormOfRegDisabled();
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.1 ФИО контактного лица. Проверка ввода разрешенных символов",
            groups = {"Checklist", "UI"})
    public void requisitesFioContactorAllowedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.isInputAllowedRequisitesContactorFio(
                latinAlphabetUC + " - " + latinAlphabetLC);
        companyCardInfoSteps.isInputAllowedRequisitesContactorFio(
                cyrillicAlphabetUC + " - " + cyrillicAlphabetLC);
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.2 ФИО контактного лица. Макс. длина строки 100 символов",
            groups = {"Checklist", "UI"})
    public void requisitesFioContactorMaxLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        companyCardInfoSteps.isInputAllowedRequisitesContactorFio(new StringGenerator().generate(100));
        companyCardInfoSteps.validationCompanyRequisitesFioContactorPrompt(new StringGenerator().generate(101),
                "Максимальная длина - 100 символов");
    }
    @DataProvider(name = "unacceptableSymbols")
    public Object[] unacceptableSybbols() {
        return "~`!@#£€$¢¥§%°^&*()_+={}[]|\\/:;\"'<>,.0123456789".chars().mapToObj(c -> Character.toString((char) c)).toArray(String[]::new);
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.3 ФИО контактного лица. Запрещен ввод других символов",
            groups = {"Checklist", "UI"}, dataProvider = "unacceptableSymbols")
    public void requisitesFioContactorRestrictedSymbols(String c) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesFioContactorPrompt(c, "Только латиница, кириллица и знаки -");
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.4 Номер телефона контактного лица(только цифры",
            groups = {"Checklist", "UI"})
    public void requisitesContactorPhoneRestrictedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        String emptyPhone = "+7 ___ ___ ____";
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, keyboardSpecialSymbols, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, cyrillicAlphabetLC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, cyrillicAlphabetUC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, latinAlphabetLC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson, latinAlphabetUC, emptyPhone);
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.4 Номер телефона контактного лица (только 10 символов)",
            groups = {"Checklist", "UI"})
    public void requisitesContactorPhone10DigitsOnly() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputPhoneContactPerson,
                "12345678901234567890", "+7 123 456 7890");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.5 Email контактного лица (валидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsValid")
    public void requisitesContactorEmail(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesContactorEmail(email);
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.5 Email контактного лица (невалидный)'",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsInvalid")
    public void requisitesContactorEmailInvalid(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesEmailContactorPrompt(email, "Введен некорректный email");
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.5 Email контактного лица (проверка длины)",
            groups = {"Checklist", "UI"})
    public void requisitesContactorEmailLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesContactorEmail(randomEmail(320));
        companyCardInfoSteps.validationCompanyRequisitesEmailContactorPrompt(randomEmail(321), "Максимальная длина - 320 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.6 Фактический адрес",
            groups = {"Checklist", "UI"})
    public void requisitesActualAddress() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesActualAddress(
                new StringGenerator().generateDescribed(250, "Ровно 250 символов"));
        companyCardInfoSteps.validationCompanyRequisitesActualAddressPrompt(
                new StringGenerator().generateDescribed(251, "251 символ"),
                "Максимальная длина - 250 символов");
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.7 Адрес регистрации",
            groups = {"Checklist", "UI"})
    public void requisitesRegisteredAddress() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesRegisteredAddress(
                new StringGenerator().generateDescribed(250, "Ровно 250 символов"));
        companyCardInfoSteps.validationCompanyRequisitesRegisteredAddressPrompt(
                new StringGenerator().generateDescribed(251, "251 символ"),
                "Максимальная длина - 250 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.8 Номер телефона компании (только цифры)",
            groups = {"Checklist", "UI"})
    public void requisitesPhoneCompanyRestrictedSymbols() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock();
        String emptyPhone = "+7 ___ ___ ____";
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputCompanyPhone, keyboardSpecialSymbols, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputCompanyPhone, cyrillicAlphabetLC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputCompanyPhone, cyrillicAlphabetUC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputCompanyPhone, latinAlphabetLC, emptyPhone);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputCompanyPhone, latinAlphabetUC, emptyPhone);
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.8 Номер телефона компании (только 10 символов)",
            groups = {"Checklist", "UI"})
    public void requisitesPhoneCompany10DigitsOnly() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        clearAndEnterPhoneWithExpectedResultCheck(onRequisitesBlock.inputCompanyPhone,
                "12345678901234567890", "+7 123 456 7890");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.9 Email компании (валидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsValid")
    public void requisitesCompanyEmail(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesCompanyEmail(email);
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.9 Email компании (невалидный)",
            groups = {"Checklist", "UI"}, dataProviderClass = DataProviders.class, dataProvider = "emailsInvalid")
    public void requisitesCompanyEmailInvalid(String email) {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesCompanyEmailPrompt(email, "Введен некорректный email");
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.9 Email компании (проверка длины)",
            groups = {"Checklist", "UI"})
    public void requisitesCompanyEmailLength() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesCompanyEmail(randomEmail(320));
        companyCardInfoSteps.validationCompanyRequisitesCompanyEmailPrompt(randomEmail(321), "Максимальная длина - 320 символов");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.10 ОГРН - Проверка валидного значения",
            groups = {"Checklist", "UI"})
    public void requisitesOgrnValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesOgrn(randomOgrnValid());
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.10 ОГРН - Проверка невалидных значений",
            groups = {"Checklist", "UI"})
    public void requisitesOgrnInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt(randomOgrnInvalid(), "Неверный формат ОГРН");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt("123456789012", "ОГРН может состоять только из 13 цифр");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt("12345678901234", "ОГРН может состоять только из 13 цифр");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt("AZazАЯаяЁёxyz", "ОГРН может состоять только из цифр");
        companyCardInfoSteps.validationCompanyRequisitesOgrnPrompt(keyboardSpecialSymbols, "ОГРН может состоять только из цифр");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.11 ИНН - Проверка валидного значения",
            groups = {"Checklist", "UI"})
    public void requisitesInnLeValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesInnLe(randomInnLeValid());
    }
    @Test(description = "Реквизиты компании - Валидация Step 3.11 ИНН - Проверка невалидных значений",
            groups = {"Checklist", "UI"})
    public void requisitesInnLeInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt(randomInnLeInvalid(), "Неверный формат ИНН");
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("123456789", "ИНН может состоять только из 10 цифр");
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("12345678901", "ИНН может состоять только из 10 цифр");
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("AZazАЯаяЁёxyz", "ИНН может состоять только из цифр");
        companyCardInfoSteps.validationCompanyRequisitesInnLePrompt("keyboardSpecialSymbols", "ИНН может состоять только из цифр");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.12 КПП - Проверка валидного значения",
            groups = {"Checklist", "UI"})
    public void requisitesKppValid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.isInputAllowedRequisitesKpp("123456789");
        //companyCardInfoSteps.isInputAllowedRequisitesKpp("1234AZ789");
    }

    @Test(description = "Реквизиты компании - Валидация Step 3.12 КПП - Проверка невалидных значений",
            groups = {"Checklist", "UI"})
    public void requisitesKppInvalid() {
        companyCardInfoSteps.openCompanyInfoPageIfNeeds(companyValidation);
        companyCardInfoSteps.startEditRequisitesBlock(false);
        companyCardInfoSteps.validationCompanyRequisitesKppPrompt("12345678", "КПП может состоять только из 9 знаков");
        companyCardInfoSteps.validationCompanyRequisitesKppPrompt("1234567890", "КПП может состоять только из 9 знаков");
        companyCardInfoSteps.validationCompanyRequisitesKppPrompt("AZazАЯаяЁёxyz", "КПП может состоять только из 9 знаков");
        companyCardInfoSteps.validationCompanyRequisitesKppPrompt("keyboardSpecialSymbols", "КПП может состоять только из 9 знаков");

    }

}
