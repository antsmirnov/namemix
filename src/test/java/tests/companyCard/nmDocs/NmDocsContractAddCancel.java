package tests.companyCard.nmDocs;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;
import utils.fileCreator.FileCreator;
import utils.fileCreator.FileFormats;

import static testData.enums.CompanyForm.LEGAL_ENTITY;

@TmsLink("NAMEMIX-T1297")
@Feature("NAMEMIX-T1297. Админ Наймикса/Карточка компании/Агентский договор/ Добавить договор - отменить")
@Test(groups = {"Checklist"})
public class NmDocsContractAddCancel extends BaseTest {

    Company companyLe;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1297";
        // В тестах не указан тип компаний, тип компании для каждого теста выбран произвольно при написании тестов
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO tests LeCompany " + testCode)
                .withOfficialName("AUTO tests LeCompany " + testCode);

        recreateTempCompany(companyLe);
    }

    @Test(description = "Step 1. Нажать отменить")
    public void pressCancel() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG));
        companyCardNmDocsContractSteps.clickCancel();
        companyCardNmDocsContractSteps.checkUploadFormNotVisible();
    }

}
