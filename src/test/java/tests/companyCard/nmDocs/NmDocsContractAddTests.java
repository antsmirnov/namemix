package tests.companyCard.nmDocs;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;
import utils.fileCreator.FileCreator;
import utils.fileCreator.FileFormats;

import static testData.enums.CompanyForm.*;

@TmsLink("NAMEMIX-T1292")
@Feature("NAMEMIX-T1292. Админ Наймикса/Карточка компании/Агентский договор - Добавить договор")
@Test(groups = {"Checklist"})
public class NmDocsContractAddTests extends BaseTest {

    Company companyLe, companyFc, companyIe;
    String firstFilesName, secondFilesName;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1292";
        // В тестах не указан тип компаний, тип компании для каждого теста выбран произвольно при написании тестов
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO tests LeCompany " + testCode)
                .withOfficialName("AUTO tests LeCompany " + testCode);
        companyFc = new Company(FOREIGN)
                .withShortName("AUTO tests ForCompany " + testCode)
                .withOfficialName("AUTO tests ForCompany " + testCode);
        companyIe = new Company(INDIVIDUAL)
                .withFioIp("AUTO ИП " + testCode);
        recreateTempCompany(companyLe);
        recreateTempCompany(companyFc);
        recreateTempCompany(companyIe);
    }

    @Test(description = "Step 1.1. Отображение drag-n-drop")
    public void checkDisplaying1() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.checkDisplayingOfUploadFormDragNDrop();
    }

    @Test(description = "Step 1.2. Кнопка Сохранить 'задизейблена'")
    public void checkDisplaying2() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractSteps.checkDisplayingOfUploadFormBtnSaveDisabled();
    }

    @Test(description = "Step 1.3. Кнопка отмены сворачивает форму")
    public void checkDisplaying3() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.clickCancel();
        companyCardNmDocsContractSteps.checkUploadFormNotVisible();
    }

    @Test(description = "Step 2. Прикрепленные файлы отображаются на форме")
    public void attachFilesAndCheckDisplaying() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.JPG));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG));
    }

    @Test(description = "Step 3. Удалить один из прикрепленных файлов")
    public void attachFilesAndDeleteOne() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyIe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.JPG));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG));
        companyCardNmDocsContractSteps.deleteAttachedFileAndCheck();
    }

    @Test(description = "Step 4.1. Сохранение первой версии агентского договора", priority = 1)
    public void uploadFirstVersionOfContract() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractSteps.openUploadForm();
        firstFilesName = "first version";
        secondFilesName = "second version";
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF, firstFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.JPG, firstFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, firstFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, firstFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, firstFilesName));
        companyCardNmDocsContractSteps.clickSave();
        companyCardNmDocsContractSteps.checkSuccessUploadNotification();
        companyCardNmDocsContractUploadedSteps.checkActualFiles(new String[]{
                firstFilesName + ".pdf",
                firstFilesName + ".jpg",
                firstFilesName + ".odt",
                firstFilesName + ".docx",
                firstFilesName + ".png",
        });
    }

    @Test(description = "Step 4.2. Сохранение второй версии агентского договора (проверка истории)", priority = 2)
    public void uploadSecondVersionOfContract() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, secondFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF, secondFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.JPG, secondFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, secondFilesName));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, secondFilesName));
        companyCardNmDocsContractSteps.clickSave();
        companyCardNmDocsContractSteps.checkSuccessUploadNotification();
        companyCardNmDocsContractUploadedSteps.checkActualFiles(new String[]{
                secondFilesName + ".pdf",
                secondFilesName + ".jpg",
                secondFilesName + ".odt",
                secondFilesName + ".docx",
                secondFilesName + ".png",
        });
        companyCardNmDocsContractUploadedSteps.checkLastHistoryVerFiles(new String[]{
                firstFilesName + ".pdf",
                firstFilesName + ".jpg",
                firstFilesName + ".odt",
                firstFilesName + ".docx",
                firstFilesName + ".png",
        });
    }

}
