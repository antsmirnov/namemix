package tests.companyCard.nmDocs;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import testData.enums.UserRole;
import testData.models.Company;
import utils.fileCreator.FileCreator;
import utils.fileCreator.FileFormats;

import static com.codeborne.selenide.Condition.disabled;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;
import static pageObject.pages.CompanyCardNmDocsPage.companyCardNmDocsFrame;
import static testData.enums.CompanyForm.*;

@TmsLink("NAMEMIX-T1293")
@Feature("NAMEMIX-T1293. Админ Наймикса/Карточка компании/Агентский договор - Добавить договор валидация")
@Test(groups = {"Checklist"})
public class NmDocsContractAddValidationTests extends BaseTest {

    Company companyLe, companyFc, companyIe;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1293";
        // В тестах не указан тип компаний, тип компании для каждого теста выбран произвольно при написании тестов
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO tests LeCompany " + testCode)
                .withOfficialName("AUTO tests LeCompany " + testCode);
        companyFc = new Company(FOREIGN)
                .withShortName("AUTO tests ForCompany " + testCode)
                .withOfficialName("AUTO tests ForCompany " + testCode);
        companyIe = new Company(INDIVIDUAL)
                .withFioIp("AUTO ИП " + testCode);
        recreateTempCompany(companyLe);
        recreateTempCompany(companyFc);
        recreateTempCompany(companyIe);
    }

    @Test(description = "Step 1. Нажать сохранить не прикрепляя файлов")
    public void pressSaveWithoutFiles() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyIe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.clickSave();
        baseSteps.checkWarningNotification("Файлы не выбраны");
    }

    @Test(description = "Step 2.2.1 Проверить валидацию формата (позитивный)")
    public void uploadFormatValidation() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "file"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "file"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "file"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF, "file"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.JPG, "file"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "file"));
        companyCardNmDocsContractSteps.clickSave();
        companyCardNmDocsContractSteps.checkSuccessUploadNotification();
        companyCardNmDocsContractUploadedSteps.checkActualFiles(new String[]{
                "file.pdf", "file.jpg", "file.odt", "file.docx", "file.doc", "file.png"
        });
    }

    @Test(description = "Step 2.2.2 Проверить валидацию формата (негативный - txt)")
    public void uploadFormatValidationNegTxt() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.TXT, "file"));
        companyCardNmDocsContractSteps.checkFileNotAttached("file.txt");
        baseSteps.checkErrorNotification("Неверное расширение файла");
    }

    @Test(description = "Step 2.2.3 Проверить валидацию формата (негативный - без расширения)")
    public void uploadFormatValidationNegNoExt() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFile(1, "NoExtension"));
        companyCardNmDocsContractSteps.checkFileNotAttached("NoExtension");
        baseSteps.checkErrorNotification("Неверное расширение файла");
    }

    @Test(description = "Step 2.2.4 Проверить валидацию формата (негативный - gif)")
    public void uploadFormatValidationNegGif() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyIe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.GIF, "file"));
        companyCardNmDocsContractSteps.checkFileNotAttached("file.gif");
        baseSteps.checkErrorNotification("Неверное расширение файла");
    }

    @Test(description = "Step 3.1 Проверить валидацию на размер файла (2МБ, позитивный)")
    public void uploadBigFileValidation() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithDescribedSize(2 * 1024 * 1024, "pdf"));
        companyCardNmDocsContractSteps.clickSave();
        companyCardNmDocsContractSteps.checkSuccessUploadNotification();
    }

    @Test(description = "Step 3.2 Проверить валидацию на размер файла (2МБ+, негативный)")
    public void uploadBigFileValidationNeg1() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFileWithDescribedSize(3 * 1024 * 1024, "pdf"));
        companyCardNmDocsContractSteps.checkNoFilesAttached();
    }

    @Test(description = "Step 3.3 Проверить валидацию на размер файла (10МБ+, негативный)")
    public void uploadBigFileValidationNeg2() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFileWithDescribedSize(10 * 1024 * 1024 + 1, "pdf"));
        companyCardNmDocsContractSteps.checkNoFilesAttached();
        companyCardNmDocsContractSteps.checkErrorInDragNDrop("Превышен размер файла в 2 мб");
    }

    @Test(description = "Step 4 Проверить валидацию на уникальность")
    public void uploadUniqueNames() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "name"));
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.PNG, "name"));
        companyCardNmDocsContractSteps.checkErrorInDragNDrop("Не удалось загрузить файл name.png. Файл с таким именем уже существует");
        companyCardNmDocsContractSteps.checkAttachedCount(1);
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "name"));
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "name"));
        companyCardNmDocsContractSteps.checkErrorInDragNDrop("Не удалось загрузить файл name.docx. Файл с таким именем уже существует");
        companyCardNmDocsContractSteps.checkAttachedCount(2);
    }

    @Test(description = "Step 5. Более 10 файлов")
    public void uploadMoreThen10Files() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "1"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "2"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "3"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "4"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "5"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "6"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "7"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "8"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "9"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "10"));
        $(companyCardNmDocsFrame.btnSave).shouldBe(enabled);
        companyCardNmDocsContractSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.ODT, "11"));
        companyCardNmDocsContractSteps.checkErrorInDragNDrop("Версия агентского договора не может содержать более 10 файлов.");
        $(companyCardNmDocsFrame.btnSave).shouldBe(disabled);
    }

    // Step 6 (Drag-and-Drop) не получилось автоматизировать

    @Test(description = "Step 7-8. Нажать крестик рядом с одним из прикрепленных файлов, нажать сохранить")
    public void attachAndDelete() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyIe);
        companyCardNmDocsContractSteps.openUploadForm();
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "1"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF, "2"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "3"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "4"));
        companyCardNmDocsContractSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "5"));
        companyCardNmDocsContractSteps.deleteAttachedFileAndCheck();
        companyCardNmDocsContractSteps.clickSave();
        companyCardNmDocsContractSteps.checkSuccessUploadNotification();
        companyCardNmDocsContractSteps.checkAttachedCount(0);
        companyCardNmDocsContractUploadedSteps.checkActualFiles(new String[]{"2.pdf", "3.docx", "4.png", "5.odt"});
        // История здесь не проверяется, на это есть проверка в T1292
    }

}
