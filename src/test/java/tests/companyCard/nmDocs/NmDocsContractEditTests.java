package tests.companyCard.nmDocs;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import service.BaseTest;
import steps.elements.PopupQuestion;
import testData.enums.UserRole;
import testData.models.Company;
import utils.fileCreator.FileCreator;
import utils.fileCreator.FileFormats;

import static testData.enums.CompanyForm.LEGAL_ENTITY;

@Test(groups = {"Checklist"})
public class NmDocsContractEditTests extends BaseTest {

    Company companyLe;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1298-T1303";
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO tests LeCompany " + testCode)
                .withOfficialName("AUTO tests LeCompany " + testCode);
        recreateTempCompany(companyLe);

        companyCardNmDocsContractSteps.uploadContractWithSixFilesForCompany(companyLe);
    }

    @BeforeMethod(description = "Открыть страницу компании, нажать кнопку редактирования последней версии договора")
    public void openPageAndClickEdit() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyLe);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
    }

    @TmsLink("NAMEMIX-T1298")
    @Feature("NAMEMIX-T1298. Админ Наймикса/Карточка компании/Агентский договор/ - Редактировать")
    @Test(description = "Step 1. У Актуальной версии договора нажать Редактировать (отображение)", priority = -1)
    public void clickEditCheckDisplaying() {
        companyCardNmDocsContractUploadedSteps.checkAttachedCount(6);
        companyCardNmDocsContractUploadedSteps.checkThereIsDeleteBtnForEachUploadedFile();
        companyCardNmDocsContractUploadedSteps.checkTextOfDragNDrop();
        companyCardNmDocsContractUploadedSteps.checkExistsBtnSaveCancel();
    }

    @TmsLink("NAMEMIX-T1298")
    @Feature("NAMEMIX-T1298. Админ Наймикса/Карточка компании/Агентский договор/ - Редактировать")
    @Test(description = "Step 2-3 Удалить")
    public void clickDeleteForOneOfFiles() {
        companyCardNmDocsContractUploadedSteps.clickDeleteForFirstUploadedFile();
        PopupQuestion.isDisplayed();
        PopupQuestion.checkPopupText("Вы действительно хотите удалить файл 1.png?");
        PopupQuestion.clickYes();
        companyCardNmDocsContractUploadedSteps.checkAttachedCount(5);
        companyCardNmDocsContractUploadedSteps.checkFileDoesNotExistInEditMode("1.png");
    }

    @TmsLink("NAMEMIX-T1298")
    @Feature("NAMEMIX-T1298. Админ Наймикса/Карточка компании/Агентский договор/ - Редактировать")
    @Test(description = "Step 4 Добавить")
    public void addFile() {
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "7"));
    }

    @TmsLink("NAMEMIX-T1298")
    @Feature("NAMEMIX-T1298. Админ Наймикса/Карточка компании/Агентский договор/ - Редактировать")
    @Test(description = "Step 2-5 Удалить, добавить, сохранить")
    public void deleteAddAndSave() {
        companyCardNmDocsContractUploadedSteps.clickDeleteForFirstUploadedFile();
        PopupQuestion.clickYes();
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "7"));
        companyCardNmDocsContractUploadedSteps.clickSave();
        companyCardNmDocsContractUploadedSteps.checkSuccessNotificationFilesUpdated();
        companyCardNmDocsContractUploadedSteps.checkActualFiles(new String[]{
                "2.doc", "3.docx", "4.pdf", "5.jpg", "6.odt", "7.docx"
        });
    }

    @TmsLink("NAMEMIX-T1303")
    @Feature("NAMEMIX-T1303. Админ Наймикса/Карточка компании/Агентский договор/ Редактировать - отменить")
    @Test(description = "Step 2-5 Удалить, добавить, сохранить", priority = -1)
    public void editAndCancel() {
        companyCardNmDocsContractUploadedSteps.clickDeleteForFirstUploadedFile();
        PopupQuestion.clickYes();
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "7"));
        companyCardNmDocsContractUploadedSteps.clickCancel();
        companyCardNmDocsContractUploadedSteps.checkActualFiles(new String[]{
                "2.doc", "3.docx", "4.pdf", "5.jpg", "6.odt", "1.png"
        });
    }

}
