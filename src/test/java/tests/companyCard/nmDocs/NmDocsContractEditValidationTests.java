package tests.companyCard.nmDocs;

import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.BaseTest;
import steps.elements.PopupQuestion;
import testData.enums.UserRole;
import testData.models.Company;
import utils.fileCreator.FileCreator;
import utils.fileCreator.FileFormats;

import java.io.File;

import static com.codeborne.selenide.Condition.disabled;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;
import static pageObject.pages.CompanyCardNmDocsPage.companyCardNmDocsUploaded;
import static testData.enums.CompanyForm.*;

@TmsLink("NAMEMIX-T1304")
@Feature("NAMEMIX-T1304. Админ Наймикса/Карточка компании/Агентский договор/ - Редактировать валидация")
@Test(groups = {"Checklist"})
public class NmDocsContractEditValidationTests extends BaseTest {

    Company companyLe, companyFc, companyIe;

    @BeforeClass(alwaysRun = true, description = "Вход в систему, создание тестовых данных")
    public void beforeClass() {
        baseSteps.loginAs(UserRole.WEBADMIN_OF_SITE);
        String testCode = "T1304";
        companyLe = new Company(LEGAL_ENTITY)
                .withShortName("AUTO tests LeCompany " + testCode)
                .withOfficialName("AUTO tests LeCompany " + testCode);
        companyFc = new Company(FOREIGN)
                .withShortName("AUTO tests ForCompany " + testCode)
                .withOfficialName("AUTO tests ForCompany " + testCode);
        companyIe = new Company(INDIVIDUAL)
                .withFioIp("AUTO ИП " + testCode);
        recreateTempCompany(companyLe);
        recreateTempCompany(companyFc);
        recreateTempCompany(companyIe);

        companyCardNmDocsContractSteps.uploadContractWithSixFilesForCompany(companyLe);
        companyCardNmDocsContractSteps.uploadContractWithSixFilesForCompany(companyIe);
        companyCardNmDocsContractSteps.uploadContractWithSixFilesForCompany(companyFc);
    }

    @Test(description = "Step 1. Нажать крестик рядом с одним из файлов")
    public void clickEditCheckDisplaying() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyIe);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.clickDeleteForFirstUploadedFile();
        PopupQuestion.isDisplayed();
        PopupQuestion.checkPopupText("Вы действительно хотите удалить файл 1.png?");
        PopupQuestion.clickYes();
        companyCardNmDocsContractUploadedSteps.checkAttachedCount(5);
        companyCardNmDocsContractUploadedSteps.checkFileDoesNotExistInEditMode("1.png");
    }

    @Test(description = "Step 2. Удалить все файлы и сохранить")
    public void deleteAllAndTryToSave() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyIe);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.deleteAllFiles();
        companyCardNmDocsContractUploadedSteps.clickSave();
        baseSteps.checkWarningNotification("Файлы не выбраны");
    }

    @Test(description = "Step 3.1 Проверить валидацию формата (позитивный)")
    public void uploadFormatValidation() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyIe);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.deleteAllFiles();
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "name"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "name"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "name"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PDF, "name"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.JPG, "name"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "name"));
        companyCardNmDocsContractUploadedSteps.clickSave();
        companyCardNmDocsContractUploadedSteps.checkSuccessNotificationFilesUpdated();
        companyCardNmDocsContractUploadedSteps.checkActualFiles(new String[]{
                "name.pdf", "name.jpg", "name.odt", "name.docx", "name.doc", "name.png"
        });
    }

    @Test(description = "Step 3.2 Проверить валидацию формата (негативный - txt)")
    public void uploadFormatValidationNegTxt() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.TXT, "file"));
        companyCardNmDocsContractUploadedSteps.checkFileDoesNotExistInEditMode("file.txt");
        baseSteps.checkErrorNotification("Неверное расширение файла");
    }

    @Test(description = "Step 3.3 Проверить валидацию формата (негативный - без расширения)")
    public void uploadFormatValidationNegNoExt() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.attachFile(new FileCreator().createFile(1, "NoExtension"));
        companyCardNmDocsContractUploadedSteps.checkFileDoesNotExistInEditMode("NoExtension");
        baseSteps.checkErrorNotification("Неверное расширение файла");
    }

    @Test(description = "Step 3.4 Проверить валидацию формата (негативный - gif)")
    public void uploadFormatValidationNegGif() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.GIF, "file"));
        companyCardNmDocsContractUploadedSteps.checkFileDoesNotExistInEditMode("file.gif");
        baseSteps.checkErrorNotification("Неверное расширение файла");
    }

    @Test(description = "Step 4.1 Проверить валидацию на размер файла (2МБ, позитивный)")
    public void uploadBigFileValidation() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithDescribedSize(2 * 1024 * 1024, "pdf"));
        companyCardNmDocsContractUploadedSteps.clickSave();
        companyCardNmDocsContractUploadedSteps.checkSuccessNotificationFilesUpdated();
    }

    @Test(description = "Step 4.2 Проверить валидацию на размер файла (2МБ+, негативный)")
    public void uploadBigFileValidationNeg1() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        File file = new FileCreator().createFileWithDescribedSize(3 * 1024 * 1024, "pdf");
        companyCardNmDocsContractUploadedSteps.attachFile(file);
        companyCardNmDocsContractUploadedSteps.checkFileDoesNotExistInEditMode(file.getName());
    }

    @Test(description = "Step 4.3 Проверить валидацию на размер файла (10МБ+, негативный)")
    public void uploadBigFileValidationNeg2() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        File file = new FileCreator().createFileWithDescribedSize(10 * 1024 * 1024 + 1, "pdf");
        companyCardNmDocsContractUploadedSteps.attachFile(file);
        companyCardNmDocsContractUploadedSteps.checkFileDoesNotExistInEditMode(file.getName());
        companyCardNmDocsContractUploadedSteps.checkErrorInDragNDrop("Превышен размер файла в 2 мб");
    }

    @Test(description = "Step 5 Проверить валидацию на уникальность")
    public void uploadUniqueNames() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.deleteAllFiles();
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "name"));
        companyCardNmDocsContractUploadedSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.PNG, "name"));
        companyCardNmDocsContractUploadedSteps.checkErrorInDragNDrop("Не удалось загрузить файл name.png. Файл с таким именем уже существует");
        companyCardNmDocsContractUploadedSteps.checkAttachedCount(1);
    }

    @Test(description = "Step 6. Более 10 файлов", priority = 1)
    public void uploadMoreThen10Files() {
        companyCardNmDocsContractSteps.openCompanyNmDocs(companyFc);
        companyCardNmDocsContractUploadedSteps.clickBtnActualVersionEdit();
        companyCardNmDocsContractUploadedSteps.deleteAllFiles();
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "1"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "2"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "3"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "4"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "5"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "6"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOC, "7"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.DOCX, "8"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.PNG, "9"));
        companyCardNmDocsContractUploadedSteps.attachFileAndCheckAttached(new FileCreator().createFileWithValidContent(FileFormats.ODT, "10"));
        $(companyCardNmDocsUploaded.btnSave).shouldBe(enabled);
        companyCardNmDocsContractUploadedSteps.attachFile(new FileCreator().createFileWithValidContent(FileFormats.ODT, "11"));
        companyCardNmDocsContractUploadedSteps.checkErrorInDragNDrop("Версия агентского договора не может содержать более 10 файлов.");
        $(companyCardNmDocsUploaded.btnSave).scrollIntoView(false);
        $(companyCardNmDocsUploaded.btnSave).shouldBe(disabled);
    }

    // Step 7 (Drag-and-Drop) не получилось автоматизировать

    // Step 8 Проверяется в T1298
}
